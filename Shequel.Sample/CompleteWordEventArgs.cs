using System;
using System.Diagnostics;

namespace Shequel.Sample
{
    [DebuggerDisplay("{Position}")]
    public class CompleteWordEventArgs : EventArgs
    {
        public int Position { get; set; }

        public CompleteWordEventArgs(int position)
        {
            Position = position;
        }
    }
}