namespace Shequel.Sample
{
    public class Message
    {
        public Span Span { get; set; }

        public string Text { get; set; }

        public Message(Span span, string text)
        {
            Span = span;
            Text = text;
        }
    }
}