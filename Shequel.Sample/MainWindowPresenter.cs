using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Nemerle.Collections;
using Nemerle.Core;
using Nitra;
using Nitra.ProjectSystem;

namespace Shequel.Sample
{
    public class MainWindowPresenter
    {
        private readonly TimeSpan parseQueueBufferSpan =
            TimeSpan.FromMilliseconds(500);

        private readonly IScheduler parseScheduler =
            new EventLoopScheduler();

        private readonly IScheduler loadScheduler = 
            new EventLoopScheduler();

        private readonly Subject<InputChangedEventArgs> reparseSignal =
            new Subject<InputChangedEventArgs>();

        private readonly ShequelQueryFile file;
        private readonly MainWindow view;

        public MainWindowPresenter(MainWindow view)
        {
            this.file = new ShequelQueryFile("current text", string.Empty);

            file.RootScope.DefaultSchema = "dbo";

            this.view = view;

            view.Input
                .Throttle(parseQueueBufferSpan)
                .DistinctUntilChanged()
                .Merge(reparseSignal)
                .ObserveOn(parseScheduler)
                .Select(ParseInput)
                .ObserveOn(DispatcherScheduler.Current)
                .Subscribe(PaintInput);

            view.CompleteWord
                .ObserveOn(parseScheduler)
                .Select(GetCompletionList)
                .ObserveOn(DispatcherScheduler.Current)
                .Subscribe(ShowCompletionPopup);

            view.DefineObjects
                .Do(DefineObjectsStart)
                .ObserveOn(loadScheduler)
                .Select(LoadObjects)
                .ObserveOn(parseScheduler)
                .Do(DefineObjects)
                .ObserveOn(DispatcherScheduler.Current)
                .Subscribe(DefineObjectsDone, OnDefineObjectsError);
        }

        private IReadOnlyCollection<DatabaseObject> LoadObjects(DefineDatabaseObjectsEventArgs arg)
        {
            var builder = new SqlConnectionStringBuilder
            {
                IntegratedSecurity = true,
                InitialCatalog = arg.Database,
                DataSource = "localhost"
            };

            var getObjectsScript = System.IO.File.ReadAllText("GetObjects.sql");
            var getTypesScript = System.IO.File.ReadAllText("GetTypes.sql");

            var objects = new List<DatabaseObject>();
            using (var connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                using (var readObjectsCommand = connection.CreateCommand())
                {
                    readObjectsCommand.CommandText = getObjectsScript;
                    readObjectsCommand.CommandType = CommandType.Text;

                    using (var reader = readObjectsCommand.ExecuteReader())
                    {
                        var objs =
                            from DataRow row in GetDataTable(reader).Rows
                            group row by
                            new
                            {
                                Database = (string) row["database_name"],
                                Schema = (string) row["schema_name"],
                                Object = (string) row["object_name"],
                                Type = (string)row["object_type"],
                            }
                            into g
                            let cols = g.SelectMany(GetColumn)
                            select new DatabaseObject(g.Key.Object, g.Key.Schema, g.Key.Database, string.Empty, GetObjectType(g.Key.Type), cols);

                        objects.AddRange(objs);
                    }

                    using (var readTypesCommand = connection.CreateCommand())
                    {
                        readTypesCommand.CommandText = getTypesScript;
                        readTypesCommand.CommandType = CommandType.Text;

                        using (var reader = readTypesCommand.ExecuteReader())
                        {
                            var types = from DataRow row in GetDataTable(reader).Rows
                                let name = (string) row["type_name"]
                                let schema = (string) row["schema_name"]
                                let database = (string) row["database_name"]
                                let server = (string) row["server_name"]
                                let type = ObjectType.UserDefinedType
                                select new DatabaseObject(name, schema, database, server, type, Enumerable.Empty<string>());

                            objects.AddRange(types);
                        }
                    }
                }

                return objects;
            }
        }

        private IEnumerable<string> GetColumn(DataRow row)
        {
            if (row["column_name"] == DBNull.Value)
                yield break;

            yield return (string) row["column_name"];
        }

        private DataTable GetDataTable(IDataReader reader)
        {
            var table = new DataTable();
            table.Load(reader);
            return table;
        }

        private ObjectType GetObjectType(string decription)
        {
            var value = decription.Trim();
            if (value.Equals("fn", StringComparison.OrdinalIgnoreCase))
                return ObjectType.ScalarFunction;

            if (value.Equals("tf", StringComparison.OrdinalIgnoreCase))
                return ObjectType.TableFunction;

            if (value.Equals("if", StringComparison.OrdinalIgnoreCase))
                return ObjectType.TableFunction;
            
            if (value.Equals("p", StringComparison.OrdinalIgnoreCase))
                return ObjectType.Procedure;
            
            if (value.Equals("u", StringComparison.OrdinalIgnoreCase))
                return ObjectType.Table;

            if (value.Equals("v", StringComparison.OrdinalIgnoreCase))
                return ObjectType.View;

            return ObjectType.Table;
        }

        private void DefineObjects(IReadOnlyCollection<DatabaseObject> objs)
        {
            file.RootScope.Reset();
            foreach (var obj in objs)
            {
                switch (obj.Type)
                {
                    case ObjectType.Table:
                        Build
                            .ExternalTable()
                            .WithName(obj.Object, obj.Schema, obj.Database, obj.Server)
                            .AddColumns(obj.Data)
                            .Define(file.RootScope);
                        break;
                    case ObjectType.TableFunction:
                        Build
                            .ExternalTableFunction()
                            .WithName(obj.Object, obj.Schema, obj.Database, obj.Server)
                            .AddColumns(obj.Data)
                            .Define(file.RootScope);
                        break;
                    case ObjectType.View:
                        Build
                            .ExternalView()
                            .WithName(obj.Object, obj.Schema, obj.Database, obj.Server)
                            .AddColumns(obj.Data)
                            .Define(file.RootScope);
                        break;
                    case ObjectType.ScalarFunction:
                        Build
                            .ExternalScalarFunction()
                            .WithName(obj.Object, obj.Schema, obj.Database, obj.Server)
                            .Define(file.RootScope);
                        break;
                    case ObjectType.Procedure:
                        Build
                            .ExternalProcedure()
                            .WithName(obj.Object, obj.Schema, obj.Database, obj.Server)
                            .Define(file.RootScope);
                        break;
                    case ObjectType.UserDefinedType:
                        Build
                            .ExternalUserDefinedType()
                            .WithName(obj.Object, obj.Schema)
                            .Define(file.RootScope);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            reparseSignal.OnNext(new InputChangedEventArgs(file.Text, true));
        }

        private void DefineObjectsStart(DefineDatabaseObjectsEventArgs args)
        {
            view.Defining = true;
        }

        private void DefineObjectsDone(IReadOnlyCollection<DatabaseObject> objs)
        {
            view.Defining = false;
        }

        private void OnDefineObjectsError(Exception ex)
        {
            MessageBox.Show(view, 
                $"Unable to load database objects due to following error:\r\n\r\n{ex.Message}", 
                "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            view.Defining = false;
        }

        private InputParseResult ParseInput(InputChangedEventArgs args)
        {
            file.Text = args.Input;
            file.Parse();
            file.UpdateParseTree();
            file.UpdateAst();

            if (args.EvaluateProperties)
            {
               file.EvalProperties();
            }
            
            return new InputParseResult(args, file.ParseResult);
        }

        private void PaintInput(InputParseResult result)
        {
            var spans = EnumerateSpans(result.Result);
            var messages = EnumerateMessages(result.Result);
            var messageSpans = messages.Select(m => m.Span).ToArray();
            var messagesText = messages.Select(m => m.Text).ToArray();

            var statistics = string.Concat(
                "parse time ", file.Statistics.Parse.Elapsed.Milliseconds, "ms, ",
                "parse tree time ", file.Statistics.ParseTree.Elapsed.Milliseconds, "ms, ",
                "ast time ", file.Statistics.Ast.Elapsed.Milliseconds, "ms, ",
                "eval properties time ", file.Statistics.Typing.Total.Milliseconds, "ms, ");

            view.PaintInput(spans, messageSpans);
            view.SetMessages(messagesText);
            view.SetStatistics(statistics);
        }

        private CompletionResult GetCompletionList(CompleteWordEventArgs e)
        {
            NSpan span;
            var completions = file.CompleteWord(e.Position, out span);
            var infos = completions
                .Cast<CompletionInfo>()
                .OrderBy(i => i.Kind)
                .ThenBy(i => i.Text)
                .Select(i => i.Text);
            return new CompletionResult(infos, new Span(span.StartPos, span.EndPos, ColorFromArgb(0)));
        }

        private void ShowCompletionPopup(CompletionResult result)
        {
            view.ShowPopup(result.Values, result.ReplacementSpan);
        }

        private Span[] EnumerateSpans(IParseResult result)
        {
            var spans = new HashSet<SpanInfo>();
            result.GetSpans(0, result.SourceSnapshot.Text.Length - 1, spans);

            var highlight = from span in spans
                let start = span.Span.StartPos
                let end = span.Span.EndPos
                let color = ColorFromArgb(span.SpanClass.Style.ForegroundColor)
                select new Span(start, end, color);

            return highlight.ToArray();
        }

        private Message[] EnumerateMessages(IParseResult result)
        {
            var originalFile = (ShequelQueryFile)result.SourceSnapshot.File;

            var messages = originalFile.ParseMessages.ToArray()
                .Concat(originalFile.AstMessages.ToArray())
                .Concat(originalFile.TypingMessages.ToArray())
                .Concat(originalFile.EvalMessages)
                .OrderBy(m => m.Location.StartPos)
                .ThenBy(m => m.Location.EndPos)
                .ToArray();
            
            return messages.SelectMany(EnumerateMessages).ToArray();
        }

        private IEnumerable<Message> EnumerateMessages(CompilerMessage compilerMessage)
        {
            var location = compilerMessage.Location;

            yield return new Message(
                new Span(location.StartPos, location.EndPos, Color.FromRgb(255, 0, 0)), 
                string.Format("({0}, {1}) {2}", location.StartPos, location.EndPos, compilerMessage.Text));

            foreach (var nestedMessage in compilerMessage.NestedMessages)
            {
                foreach (var message in EnumerateMessages(nestedMessage))
                {
                    yield return message;
                }
            }
        }

        private Color ColorFromArgb(int argb)
        {
            unchecked
            {
                return Color.FromArgb((byte)(argb >> 24), (byte)(argb >> 16), (byte)(argb >> 8), (byte)argb);
            }
        }

        private class CompletionResult
        {
            public string[] Values { get; }

            public Span ReplacementSpan { get; }

            public CompletionResult(IEnumerable<string> values, Span replacementSpan)
            {
                Values = values.ToArray();
                ReplacementSpan = replacementSpan;
            }
        }

        [DebuggerDisplay("{Database}.{Schema}.{Object} ({Type})")]
        private class DatabaseObject
        {
            public DatabaseObject(string obj, string schema, string database, string server, ObjectType type, IEnumerable<string> data)
            {
                Object = obj;
                Schema = schema;
                Database = database;
                Server = server;
                Type = type;
                Data = data.ToList();
            }

            public string Object { get; }

            public string Schema { get; }

            public string Database { get; }

            public string Server { get; }

            public ObjectType Type { get; }

            public IReadOnlyCollection<string> Data { get; }
        }

        private enum ObjectType
        {
            Table,
            TableFunction,
            View,
            ScalarFunction,
            Procedure,
            UserDefinedType
        }
    }
}