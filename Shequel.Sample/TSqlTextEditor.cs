﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Rendering;

namespace Shequel.Sample
{
    public class TSqlTextEditor : TextEditor
    {
        public TSqlTextEditor()
        {
            Spans = new Span[0];
            MessageSpans = new Span[0];

            TextArea.TextView.LineTransformers.Add(
                new TSqlColorizer(this));

            TextArea.TextView.BackgroundRenderers.Add(
                new TSqlMessageRenderer(this));
        }

        public Span[] Spans { get; set; }

        public Span[] MessageSpans { get; set; }

        private class TSqlMessageRenderer : IBackgroundRenderer
        {
            private readonly TSqlTextEditor editor;

            public TSqlMessageRenderer(TSqlTextEditor editor)
            {
                this.editor = editor;
            }

            public void Draw(TextView textView, DrawingContext drawingContext)
            {
                var brush = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                brush.Freeze();

                var pen = new Pen(brush, 1);
                pen.DashStyle = DashStyles.Dot;
                pen.Freeze();

                foreach (var line in editor.Document.Lines)
                {
                    foreach (var span in editor.MessageSpans)
                    {
                        Tuple<int, int> offset;
                        if (TryGetOffset(line, span, out offset))
                        {
                            var start = offset.Item1;
                            var end = offset.Item2;
                            var length = end - start;

                            var segment = new TextSegment
                            {
                                StartOffset = start,
                                EndOffset = end,
                                Length = length
                            };

                            foreach (var rectangle in BackgroundGeometryBuilder.GetRectsForSegment(textView, segment))
                            {
                                var startPoint = rectangle.BottomLeft;
                                var endPoint = length > 0
                                    ? rectangle.BottomRight
                                    : new Point(rectangle.BottomRight.X + 10, rectangle.BottomRight.Y);

                                drawingContext.DrawLine(pen, startPoint, endPoint);
                            }
                        }
                    }
                }
            }

            public KnownLayer Layer
            {
                get { return KnownLayer.Selection; }
            }
        }

        private class TSqlColorizer : DocumentColorizingTransformer
        {
            private readonly TSqlTextEditor editor;

            public TSqlColorizer(TSqlTextEditor editor)
            {
                this.editor = editor;
            }

            protected override void ColorizeLine(DocumentLine line)
            {
                foreach (var span in editor.Spans)
                {
                    Tuple<int, int> offset;
                    if (TryGetOffset(line, span, out offset))
                    {
                        ChangeLinePart(offset.Item1, offset.Item2, e => HighLightElement(e, span.Color));
                    }
                }
            }

            private void HighLightElement(VisualLineElement element, Color color)
            {
                var brush = new SolidColorBrush(color);
                brush.Freeze();

                element.TextRunProperties.SetForegroundBrush(brush);
            }
        }

        private static bool TryGetOffset(DocumentLine line, Span span, out Tuple<int, int> result)
        {
            result = null;

            var start = line.Offset;
            var end = line.Offset + line.Length;
            if (start > span.End || end < span.Start)
                return false;

            var startOffset = Math.Max(line.Offset, span.Start);
            var endOffset = Math.Min(line.EndOffset, span.End);

            result = Tuple.Create(startOffset, endOffset);

            return true;
        }
    }

    [DebuggerDisplay("({Start}, {End}) {Color}")]
    public class Span
    {
        public Span(int start, int end, Color color)
        {
            Start = start;
            End = end;
            Color = color;
        }

        public int Start { get; }

        public int End { get; }

        public Color Color { get; }
    }
}
