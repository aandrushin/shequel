﻿using Nitra;

namespace Shequel.Sample
{
    public class InputParseResult
    {
        public InputChangedEventArgs EventArgs { get; set; }

        public IParseResult Result { get; set; }

        public InputParseResult(InputChangedEventArgs e, IParseResult result)
        {
            EventArgs = e;
            Result = result;
        }
    }
}