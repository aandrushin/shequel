﻿using System;
using System.Diagnostics;

namespace Shequel.Sample
{
    [DebuggerDisplay("{Input}")]
    public class InputChangedEventArgs : EventArgs
    {
        public string Input { get; private set; }

        public bool EvaluateProperties { get; set; }

        public InputChangedEventArgs(string input, bool evaluateProperties)
        {
            Input = input;
            EvaluateProperties = evaluateProperties;
        }
    }
}