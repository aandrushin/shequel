﻿select
	[o].[name] as [object_name],
	[o].[type] as [object_type],
	[s].[name] as [schema_name],
	[c].[name] as [column_name],
	db_name() as [database_name],
	@@servername as [server_name]
from [sys].[objects] [o] inner join 
	 [sys].[schemas] [s] on [o].[schema_id] = [s].[schema_id] left join
	 [sys].[all_columns] [c] on [o].[object_id] = [c].[object_id]
where [type] in ('FN', 'IF', 'TF', 'P', 'U', 'V')