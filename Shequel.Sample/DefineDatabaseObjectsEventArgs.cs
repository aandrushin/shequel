using System;
using System.Diagnostics;

namespace Shequel.Sample
{
    [DebuggerDisplay("{Database}")]
    public class DefineDatabaseObjectsEventArgs : EventArgs
    {
        public string Database { get; set; }

        public DefineDatabaseObjectsEventArgs(string database)
        {
            Database = database;
        }
    }
}