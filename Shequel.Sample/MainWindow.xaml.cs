﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;

namespace Shequel.Sample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowPresenter presenter;

        public MainWindow()
        {
            InitializeComponent();
            
            Input = Observable
                .FromEventPattern(
                    h => editor.TextChanged += h,
                    h => editor.TextChanged -= h)
                .Select(o => new InputChangedEventArgs(editor.Text, true));

            CompleteWord = Observable
                .FromEventPattern<KeyEventHandler, KeyEventArgs>(
                    h => editor.KeyDown += h,
                    h => editor.KeyDown -= h)
                .Where(CompleteWordEventPredicate)
                .Select(o => new CompleteWordEventArgs(editor.CaretOffset));

            DefineObjects = Observable
                .FromEventPattern<RoutedEventHandler, RoutedEventArgs>(
                    h => defineButton.Click += h,
                    h => defineButton.Click -= h)
                .Where(e => !string.IsNullOrWhiteSpace(databaseTextBox.Text))
                .Select(e => new DefineDatabaseObjectsEventArgs(databaseTextBox.Text));

            presenter = new MainWindowPresenter(this);
        }

        public bool Defining
        {
            get
            {
                return string.Equals(defineButton.Content, "Load");
            }
            set
            {
                if (value)
                {
                    defineButton.Content = "Loading...";
                    defineButton.IsEnabled = false;
                }
                else
                {
                    defineButton.Content = "Load";
                    defineButton.IsEnabled = true;
                }
            }
        }

        public IObservable<InputChangedEventArgs> Input { get; private set; }

        public IObservable<CompleteWordEventArgs> CompleteWord { get; private set; }

        public IObservable<DefineDatabaseObjectsEventArgs> DefineObjects { get; private set; }

        public void PaintInput(Span[] spans, Span[] messageSpans)
        {
            editor.Spans = spans;
            editor.MessageSpans = messageSpans;
            editor.TextArea.TextView.Redraw(DispatcherPriority.Input);
        }

        public void SetMessages(string[] messages)
        {
            messagesTextBox.Text = string.Join(Environment.NewLine, messages);
        }

        public void SetStatistics(string text)
        {
            statisticsBlock.Text = text;
        }

        public void ShowPopup(string[] values, Span replacementSpan)
        {
            if (values.Length > 0)
            {
                var window = new CompletionWindow(editor.TextArea);

                for (var index = 0; index < values.Length; index++)
                {
                    window.CompletionList.CompletionData.Add(
                       new TextCompletionData(values[index], replacementSpan, index));
                }

                window.Show();
            }
        }

        private bool CompleteWordEventPredicate(EventPattern<KeyEventArgs> arg)
        {
            var e = arg.EventArgs;
            if ((e.Key == Key.Space && e.KeyboardDevice.Modifiers == ModifierKeys.Control) ||
                (e.Key == Key.Escape))
            {
                e.Handled = true;
                return true;
            }
            return false;
        }

        private class TextCompletionData : ICompletionData
        {
            public TextCompletionData(string value, Span span, int priority)
            {
                Span = span;
                Image = null;
                Text = value;
                Content = value;
                Description = value;
                Priority = priority;
            }

            public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
            {
                var start = completionSegment.Offset < Span.Start
                    ? completionSegment.Offset
                    : Span.Start;
                var end = completionSegment.EndOffset > Span.End
                    ? completionSegment.EndOffset
                    : Span.End;

                var segment = new TextSegment
                {
                    StartOffset = start,
                    EndOffset = end,
                    Length = end - start
                };

                textArea.Document.BeginUpdate();
                textArea.Document.Replace(segment, string.Empty);
                textArea.Document.Insert(Span.Start, Text);
                textArea.Document.EndUpdate();
            }

            public Span Span { get; set; }

            public ImageSource Image { get; }

            public string Text { get; }

            public object Content { get; }

            public object Description { get; }

            public double Priority { get; }
        }
    }
}
