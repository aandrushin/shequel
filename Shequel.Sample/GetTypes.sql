﻿select
	[t].[name] as [type_name],
	[s].[name] as [schema_name],
	db_name() as [database_name],
	@@servername as [server_name]
from [sys].[types] t inner join 
	 [sys].[schemas] [s] on [t].[schema_id] = [s].[schema_id]
where 
	 [t].[is_user_defined] = 1