﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Completion
{
    [TestFixture]
    public class CompletionTest
    {
        private ShequelQueryFile file = new ShequelQueryFile("test.sql");

        [SetUp]
        public void Setup()
        {
            file.RootScope.Reset();
        }

        [Test]
        public void Completion_from_external_object()
        {
            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .Define(file.RootScope);

            Build
                .ExternalTable()
                .WithName("a", "d1", "c", "d")
                .Define(file.RootScope);

            NSpan span;
            var result = Complete("select * from |", out span);

            Assert.That(span.IsEmpty);
            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new[]
            {
                "a"
            }));
            Assert.That(result.All(r => r.Kind == CompletionObjectKind.Table));
        }

        [Test]
        public void Completion_from_parameters()
        {
            Build
                .Declare()
                .WithName("@a")
                .WithType(Build
                    .TableType()
                    .AddColumn("id", Build.SimpleType()
                    .WithName("int")))
                .Define(file.RootScope);

            Build
                .Declare()
                .WithName("@b")
                .WithType(Build
                    .SimpleType()
                    .WithName("int"))
                .Define(file.RootScope);

            NSpan span;
            var result = Complete("select * from |", out span);
            
            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "@a"
            }));
            Assert.That(result.All(r => r.Kind == CompletionObjectKind.Parameter));
        }

        [Test]
        public void Completion_from_join_predicate()
        {
            Build
                .Declare()
                .WithName("@a")
                .WithType(Build
                    .TableType()
                    .AddColumn("id", Build.SimpleType()
                    .WithName("int")))
                .Define(file.RootScope);

            Build
                .Declare()
                .WithName("@b")
                .WithType(Build
                    .SimpleType()
                    .WithName("int"))
                .Define(file.RootScope);

            NSpan span;
            var result = Complete("select * from @a a inner join @a b on |", out span);

            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "a", "b", "id", "@b"
            }));
            Assert.That(result.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Parameter,
                CompletionObjectKind.Parameter,
                CompletionObjectKind.Column,
                CompletionObjectKind.Parameter,
            }));
        }

        [Test]
        public void Completion_projection_external_symbol()
        {
            Build
                 .ExternalTable()
                 .WithName("a", "dbo", "c", "d")
                 .AddColumn("id1")
                 .Define(file.RootScope);

            Build
                 .ExternalTable()
                 .WithName("b", "b1", "c", "d")
                 .AddColumn("id2")
                 .Define(file.RootScope);

            NSpan span;
            var result = Complete("select | from a, b1.b", out span);

            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "b", "a", "id1", "id2"
            }));
            Assert.That(result.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Table,
                CompletionObjectKind.Table,
                CompletionObjectKind.Column,
                CompletionObjectKind.Column,
            }));
        }

        [Test]
        public void Completion_projection_procedure_udt()
        {
            Build
                 .ExternalProcedure()
                 .WithName("a", "dbo", "c", "d")
                 .Define(file.RootScope);

            Build
                 .ExternalUserDefinedType()
                 .WithName("c", "dbo")
                 .Define(file.RootScope);

            Build
                 .ExternalTable()
                 .WithName("b", "dbo", "c", "d")
                 .AddColumn("id2")
                 .Define(file.RootScope);

            NSpan span;
            var result = Complete("select | from b", out span);

            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "b", "id2"
            }));
            Assert.That(result.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Table,
                CompletionObjectKind.Column,
            }));
        }

        [Test]
        public void Completion_relation_procedure_udt()
        {
            Build
                 .ExternalProcedure()
                 .WithName("a", "dbo", "c", "d")
                 .Define(file.RootScope);

            Build
                 .ExternalUserDefinedType()
                 .WithName("c", "dbo")
                 .Define(file.RootScope);

            Build
                 .ExternalTable()
                 .WithName("b", "dbo", "c", "d")
                 .AddColumn("id2")
                 .Define(file.RootScope);

            NSpan span;
            var result = Complete("select * from |", out span);

            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "b"
            }));
            Assert.That(result.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Table,
            }));
        }

        [Test]
        public void Completion_projection_parameter()
        {
            Build
                .Declare()
                .WithName("@a")
                .WithType(Build
                    .TableType()
                    .AddColumn("id", Build.SimpleType()
                    .WithName("int")))
                .Define(file.RootScope);

            Build
                .Declare()
                .WithName("@b")
                .WithType(Build
                    .SimpleType()
                    .WithName("int"))
                .Define(file.RootScope);

            NSpan span;
            var result = Complete("select | from @a a", out span);

            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "a", "@b", "id"
            }));
            Assert.That(result.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Parameter,
                CompletionObjectKind.Parameter,
                CompletionObjectKind.Column,
            }));
        }

        [Test]
        public void Completion_parameter()
        {
            Build
                .Declare()
                .WithName("@a1")
                .WithType(Build
                    .SimpleType()
                    .WithName("int"))
                .Define(file.RootScope);

            Build
                .Declare()
                .WithName("@b1")
                .WithType(Build
                    .SimpleType()
                    .WithName("int"))
                .Define(file.RootScope);

            NSpan span;
            var result = Complete("select @a| from @a a", out span);

            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "@a1"
            }));
            Assert.That(result.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Parameter,
            }));
        }

        [Test]
        public void Completion_object_identifier()
        {
            Build
                 .ExternalTable()
                 .WithName("a1", "dbo", "c", "d")
                 .AddColumn("id1")
                 .Define(file.RootScope);

            Build
                 .ExternalTable()
                 .WithName("b1", "b1", "c", "d")
                 .AddColumn("id2")
                 .Define(file.RootScope);
            
            NSpan span1;
            var result1 = Complete("select * from a|", out span1);

            Assert.That(result1.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "a1"
            }));
            Assert.That(result1.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Table,
            }));
            
            NSpan span2;
            var result2 = Complete("select * from dbo.a|", out span2);
            
            Assert.That(result2.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "a1"
            }));
            Assert.That(result2.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Table,
            }));
            
            NSpan span3;
            var result3 = Complete("select * from c.dbo.a|", out span3);

            Assert.That(result3.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "a1"
            }));
            Assert.That(result3.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Table,
            }));
            

            NSpan span4;
            var result4 = Complete("select * from d.c.dbo.a|", out span4);

            Assert.That(result4.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "a1"
            }));
            Assert.That(result4.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Table,
            }));
        }

        [Test]
        public void Completion_column_identifier()
        {
            Build
                 .ExternalTable()
                 .WithName("a", "dbo", "c", "d")
                 .AddColumn("id1")
                 .Define(file.RootScope);

            Build
                 .ExternalTable()
                 .WithName("b", "b1", "c", "d")
                 .AddColumn("id2")
                 .Define(file.RootScope);
            
            NSpan span1;
            var result1 = Complete("select id| from a", out span1);

            Assert.That(result1.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "id1"
            }));
            Assert.That(result1.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Column,
            }));
            
            NSpan span2;
            var result2 = Complete("select a.id| from a", out span2);

            Assert.That(result2.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "id1"
            }));
            Assert.That(result2.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Column,
            }));
            
            NSpan span3;
            var result3 = Complete("select c.dbo.a.id| from a", out span3);

            Assert.That(result3.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "id1"
            }));
            Assert.That(result3.Select(r => r.Kind), Is.EquivalentTo(new []
            {
                CompletionObjectKind.Column,
            }));
        }

        private CompletionInfo[] Complete(string text, out NSpan span)
        {
            var index = text.IndexOf("|", StringComparison.InvariantCulture);
            file.Text = text.Replace("|", string.Empty);
            file.Parse();
            file.UpdateParseTree();
            file.UpdateAst();
            file.EvalProperties();
            return file.CompleteWord(index, out span).Cast<CompletionInfo>().ToArray();
        }
    }
}
