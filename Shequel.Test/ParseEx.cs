﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading;
using Nemerle.Collections;
using Nitra;
using Nitra.Declarations;
using Nitra.ProjectSystem;
using Shequel.Internal;

namespace Shequel.Test
{
    internal static class ParseEx
    {
        public static ParseResult ParseTree(this StartRuleDescriptor parser, string text)
        {
            var list = new CompilerMessageList();
            var session = new ParseSession(parser);
            session.CompilerMessages = list;
            session.RecoveryTimeout = TimeSpan.FromSeconds(1);
            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(5));
            session.CancellationToken = cts.Token;
            session.ParseToEndOfString = true;
            return (ParseResult)session.Parse(text);
        }

        public static T Parse<T>(this StartRuleDescriptor parser, string text)
            where T : IAst
        {
            var result = ParseTree(parser, text);
            var list = (CompilerMessageList) result.CompilerMessages;
            if (list.Count > 0)
                foreach (var message in list)
                    Console.WriteLine($"{message.Number} ({message.Location.StartPos}, {message.Location.EndPos}) {message.Type} {message.Text}");

            if (!result.IsSuccess)
                throw new ParsingFailureException("Parsing failure", result);

            var tree = result.CreateParseTree();
            return (T) tree.GetAstUntyped();
        }

        public static T Eval<T>(this StartRuleDescriptor parser, string text, BindingScope scope = null)
            where T : IAst
        {
            var ast = Parse<T>(parser, text);
            var context = new DependentPropertyEvalContext();
            var host = new ColumnEvalHost(ast, scope ?? new BindingScope(), new ParseOptions());
            host.EvalProperties(context, stage: 0);
            host.EvalProperties(context, stage: 1);
            host.EvalProperties(context, stage: 2);
            return ast;
        }

        public static T EvalSymbol<T>(this StartRuleDescriptor parser, string text)
            where T : Declaration, IAst
        {
            var ast = Parse<T>(parser, text);
            var symbol = ast.CreateSymbol();
            ast.Symbol = symbol;
            symbol.AddDeclaration(ast);
            var context = new DependentPropertyEvalContext();
            var host = new TestEvalHost(ast, new BindingScope(), new ParseOptions());
            host.EvalProperties(context, stage: 0);
            host.EvalProperties(context, stage: 1);
            host.EvalProperties(context, stage: 2);
            return ast;
        }

        public static T EvalExternalSymbol<T>(this ExternalObject declaration)
            where T : ExternalObjectSymbol
        {
            var scope = new BindingScope();
            scope.Define<T>(declaration);
            var context = new DependentPropertyEvalContext();
            scope.EvalProperties(context);
            return (T) declaration.Symbol;
        }

        public static IEnumerable<ColumnDeclarationSymbol> EvalSymbolColumns<T>(this StartRuleDescriptor parser, string text, BindingScope scope = null)
            where T : Declaration
        {
            var ast = Parse<T>(parser, text);
            scope = scope ?? new BindingScope();
            ast.ContainingTable = scope;
            var symbol = ast.CreateSymbol();
            ast.Symbol = symbol;
            symbol.AddDeclaration(ast);
            var context = new DependentPropertyEvalContext();
            var host = new ColumnEvalHost(ast, scope, new ParseOptions());
            host.EvalProperties(context, stage: 0);
            host.EvalProperties(context, stage: 1);
            host.EvalProperties(context, stage: 2);
            var columns = new LightList<ColumnDeclarationSymbol>();
            ast.ContainingTable.FindMany(t => true, ref columns);
            return columns.ToArray();
        }

        public static IEnumerable<CompilerMessage> EvalMessages(this StartRuleDescriptor parser, string text, BindingScope scope = null)
        {
            var ast = parser.Parse<IAst>(text);
            var data = new FileEvalPropertiesData(string.Empty, string.Empty, -1, 0, ast,
                new StatisticsTask.Container(string.Empty));
            var context = new DependentPropertyEvalContext();
            context.Files = ImmutableArray.Create(data);
            var options = new ParseOptions();
            var host = new ColumnEvalHost(ast, scope ?? new BindingScope(), options);
            host.EvalProperties(context, stage: 0);
            host.EvalProperties(context, stage: 1);
            host.EvalProperties(context, stage: 2);
            var messages = data.GetCompilerMessage();
            var visitor = new ReferenceVisitor(messages, options.CaseComparer);
            visitor.Visit(ast);
            return messages.GetMessages();
        }
    }

    public class TestEvalHost : EvalPropertiesHost
    {
        protected readonly IAst ast;
        protected readonly BindingScope scope;
        protected readonly ParseOptions options;

        public TestEvalHost(IAst ast, BindingScope scope, ParseOptions options)
        {
            this.ast = ast;
            this.scope = scope;
            this.options = options;
        }

        protected override void ExecutePass(DependentPropertyEvalContext context, string passName)
        {
            if (context.CancellationToken.IsCancellationRequested)
                return;

            var statistics = new StatisticsTask.Single(passName);
            EvalProperties(context, ast, statistics);
            var declaration = ast as Declaration;
            if (declaration != null)
                EvalProperties(context, declaration.Symbol, statistics);
        }

        protected override void BeforeStage(DependentPropertyEvalContext context, string passName)
        {
            if (context.Stage == 0)
            {
                var bindable = ast as BindableAst;
                if (bindable != null)
                    bindable.Scope = scope;

                var scoped = ast as ScopedAst;
                if (scoped != null)
                    scoped.ContainingTable = scope;

                var visitor = new InitializeOptionsVisitor(options);
                visitor.Visit(ast);
                
                scope.EvalProperties(context);
            }
        }
    }

    public class ColumnEvalHost : TestEvalHost
    {
        public ColumnEvalHost(IAst ast, BindingScope scope, ParseOptions options) 
            : base(ast, scope, options)
        {
        }

        protected override void BeforeStage(DependentPropertyEvalContext context, string passName)
        {
            base.BeforeStage(context, passName);
            
            if (context.Stage == 2)
            {
                var visitor = new DefineColumnVisitor(context, options);
                visitor.Visit(ast);
            }
        }
    }
}
