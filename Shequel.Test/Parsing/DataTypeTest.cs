﻿using System;
using System.Linq;
using FluentAssertions;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class DataTypeTests
    {
        private readonly ExtensibleRuleDescriptor parser = TSql.DataType;

        [Test]
        public void DataType_any()
        {
            Assert.That(parser.Parse<DataType>("BINARY(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("CHAR(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("DATETIMEOFFSET(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("DECIMAL(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("NCHAR(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("NUMERIC(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("NVARCHAR(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("TIME(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("VARBINARY(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("VARCHAR(1)"), Is.InstanceOf<DataType.Simple>());
            Assert.That(parser.Parse<DataType>("XML"), Is.InstanceOf<DataType.Xml>());
            Assert.That(parser.Parse<DataType>("TABLE(b int)"), Is.InstanceOf<DataType.Table>());
            Assert.That(parser.Parse<DataType>("dbo.a"), Is.InstanceOf<DataType.UserDefined>());
        }

        [Test]
        public void DataType_Simple()
        {
            var simpleDataType = new[] {"BIGINT", "BIT", "DATE", "DATETIME2", "DATETIME", "FLOAT",
                "GEOGRAPHY", "GEOMETRY", "HIERARCHYID", "IMAGE", "INT", "MONEY", "NTEXT", "REAL",
                "SMALLDATETIME", "SMALLINT", "SMALLMONEY", "SQL_VARIANT", "TEXT", "TIMESTAMP",
                "TINYINT", "UNIQUEIDENTIFIER" };

            foreach (var dataType in simpleDataType)
            {
                var result = parser.Parse<DataType.Simple>(dataType);

                Assert.That(
                    string.Equals(result.Name.Value, dataType, StringComparison.OrdinalIgnoreCase),
                    string.Format("Unable to parse type {0}", dataType));
            }
        }

        [Test]
        public void DataType_Simple_binary()
        {
            Assert.That(parser.Parse<DataType.Simple>("BINARY").Name.Value, Is.EqualTo("binary"));
            Assert.That(parser.Parse<DataType.Simple>("BINARY").Length.HasValue, Is.False);
            parser.Parse<DataType.Simple>("BINARY(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());
        }

        [Test]
        public void DataType_Simple_char()
        {
            Assert.That(parser.Parse<DataType.Simple>("CHAR").Name.Value, Is.EqualTo("char"));
            Assert.That(parser.Parse<DataType.Simple>("CHAR").Length.HasValue, Is.False);
            parser.Parse<DataType.Simple>("CHAR(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());

        }

        [Test]
        public void DataType_Simple_datetimeoffset()
        {
            Assert.That(parser.Parse<DataType.Simple>("DATETIMEOFFSET").Name.Value, Is.EqualTo("datetimeoffset"));
            Assert.That(parser.Parse<DataType.Simple>("DATETIMEOFFSET").Length.HasValue, Is.False);
            parser.Parse<DataType.Simple>("DATETIMEOFFSET(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());
        }

        [Test]
        public void DataType_Simple_decimal()
        {
            Assert.That(parser.Parse<DataType.Simple>("DECIMAL").Name.Value, Is.EqualTo("decimal"));
            parser.Parse<DataType.Simple>("DECIMAL(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());
            parser.Parse<DataType.Simple>("DECIMAL(1, 2)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1, Scale = 2 }, x => x.ExcludingMissingMembers());
        }

        [Test]
        public void DataType_Simple_numeric()
        {
            Assert.That(parser.Parse<DataType.Simple>("NUMERIC").Name.Value, Is.EqualTo("numeric"));
            parser.Parse<DataType.Simple>("NUMERIC(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());
            parser.Parse<DataType.Simple>("NUMERIC(1, 2)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1, Scale = 2 }, x => x.ExcludingMissingMembers());
        }

        [Test]
        public void DataType_Simple_nchar()
        {
            Assert.That(parser.Parse<DataType.Simple>("NCHAR").Name.Value, Is.EqualTo("nchar"));
            parser.Parse<DataType.Simple>("NCHAR(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());
        }

        [Test]
        public void DataType_Simple_time()
        {
            Assert.That(parser.Parse<DataType.Simple>("TIME").Name.Value, Is.EqualTo("time"));
            parser.Parse<DataType.Simple>("TIME(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());
        }

        [Test]
        public void DataType_Simple_nvarchar()
        {
            Assert.That(parser.Parse<DataType.Simple>("NVARCHAR").Name.Value, Is.EqualTo("nvarchar"));
            Assert.That(parser.Parse<DataType.Simple>("NVARCHAR").Length.HasValue, Is.False);
            parser.Parse<DataType.Simple>("NVARCHAR(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());
            Assert.That(parser.Parse<DataType.Simple>("NVARCHAR(MAX)").Length.Value, Is.InstanceOf<DataTypeLength.Max>());
        }

        [Test]
        public void DataType_Simple_varchar()
        {
            Assert.That(parser.Parse<DataType.Simple>("VARCHAR").Name.Value, Is.EqualTo("varchar"));

           parser.Parse<DataType.Simple>("VARCHAR(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());

            Assert.That(parser.Parse<DataType.Simple>("VARCHAR(MAX)").Length.Value, Is.InstanceOf<DataTypeLength.Max>());
        }

        [Test]
        public void DataType_Simple_varbinary()
        {
            Assert.That(parser.Parse<DataType.Simple>("VARBINARY").Name.Value, Is.EqualTo("varbinary"));

            parser.Parse<DataType.Simple>("VARBINARY(1)").Length.Value
                .ShouldBeEquivalentTo(new { Length = 1 }, x => x.ExcludingMissingMembers());

            Assert.That(parser.Parse<DataType.Simple>("VARBINARY(MAX)").Length.Value, Is.InstanceOf<DataTypeLength.Max>());
        }

        [Test]
        public void DataType_Xml()
        {
            Assert.That(parser.Parse<DataType.Xml>("xml").XmlSchema.HasValue, Is.False);
            Assert.That(parser.Parse<DataType.Xml>("xml").Kind.HasValue, Is.False);
        }

        [Test]
        public void DataType_Xml_schema()
        {
            var result = parser.Parse<DataType.Xml>("xml(a.b)");

            Assert.That(result.Schema.Value, Is.EqualTo("a"));
            Assert.That(result.XmlSchema.Value, Is.EqualTo("b"));
        }

        [Test]
        public void DataType_Xml_missing_schema()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<DataType.Xml>("xml()"));
        }

        [Test]
        public void DataType_Xml_kind()
        {
            Assert.That(parser.Parse<DataType.Xml>("xml(DOCUMENT a.b)").Kind.Value,
                Is.EqualTo(XmlContentKind.Document));

            Assert.That(parser.Parse<DataType.Xml>("xml(CONTENT a.b)").Kind.Value,
                Is.EqualTo(XmlContentKind.Content));
        }

        [Test]
        public void DataType_UserDefined()
        {
            Assert.That(parser.Parse<DataType.UserDefined>("POINT").Type.Value, Is.EqualTo("POINT"));

            var udt = parser.Parse<DataType.UserDefined>("dbo.POINT");
            Assert.That(udt.Type.Value, Is.EqualTo("POINT"));
            Assert.That(udt.Schema.Value, Is.EqualTo("dbo"));
        }

        [Test]
        public void DataType_UserDefined_long_name()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<DataType.UserDefined>("database.dbo.POINT"));
        }
        
        [Test]
        public void DataType_Table()
        {
            var result = parser.Parse<DataType.Table>("table(a varchar(max), b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            Assert.That(result.Items.ElementAt(0), Is.InstanceOf<TableDataTypeColumnItem>());

            var column = (TableDataTypeColumnItem)result.Items.ElementAt(0);

            Assert.That(column.Column.Value, Is.EqualTo("a"));
            Assert.That(column.Collation.HasValue, Is.False);
            Assert.That(column.Constraints, Is.Empty);
            Assert.That(column.DataType, Is.InstanceOf<DataType.Simple>());
            Assert.That(column.RowGuidColumn.HasValue, Is.False);
            Assert.That(column.Default.HasValue, Is.False);
        }

        [Test]
        public void DataType_Table_primary_key()
        {
            var result = parser.Parse<DataType.Table>("table(a int, b int, primary key (a, b))");

            Assert.That(result.Items.Count(), Is.EqualTo(3));

            Assert.That(result.Items.ElementAt(2), Is.InstanceOf<TableDataTypeConstraintItem>());

            var constraint = (TableDataTypeConstraintItem.PrimaryKey)result.Items.ElementAt(2);

            Assert.That(constraint.Columns.Count(), Is.EqualTo(2));
            Assert.That(constraint.Columns.First().Name.Value, Is.EqualTo("a"));
        }

        [Test]
        public void DataType_Table_unique()
        {
            var result = parser.Parse<DataType.Table>("table(a int, b int, unique (a, b))");

            Assert.That(result.Items.Count(), Is.EqualTo(3));

            Assert.That(result.Items.ElementAt(2), Is.InstanceOf<TableDataTypeConstraintItem>());

            var constraint = (TableDataTypeConstraintItem.Unique)result.Items.ElementAt(2);

            Assert.That(constraint.Columns.Count(), Is.EqualTo(2));
            Assert.That(constraint.Columns.First().Name.Value, Is.EqualTo("a"));
            Assert.That(constraint.Columns.Last().Name.Value, Is.EqualTo("b"));
        }

        [Test]
        public void DataType_Table_check()
        {
            var result = parser.Parse<DataType.Table>("table(a int, check (a < 2))");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            Assert.That(result.Items.ElementAt(1), Is.InstanceOf<TableDataTypeConstraintItem>());

            var constraint = (TableDataTypeConstraintItem.Check)result.Items.ElementAt(1);

            Assert.That(constraint.Predicate, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void DataType_Table_check_column()
        {
            var result = parser.Parse<DataType.Table>("table(a int check (a < 2), b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();
            var constraint = (ColumnConstraint.Check)column.Constraints.First();

            Assert.That(constraint.Name.Value, Is.EqualTo("check"));
            Assert.That(constraint.Predicate, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void DataType_Table_default()
        {
            var result = parser.Parse<DataType.Table>("table(a int default 1, b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();

            Assert.That(column.Default.Value, Is.InstanceOf<DefaultValue.Value>());

            var value = (DefaultValue.Value) column.Default.Value;

            Assert.That(value.Value, Is.InstanceOf<Constant.Integer>());
        }

        [Test]
        public void DataType_Table_default_expression()
        {
            var result = parser.Parse<DataType.Table>("table(a int default 1 + 1, b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();

            Assert.That(column.Default.Value, Is.InstanceOf<DefaultValue.Value>());

            var value = (DefaultValue.Value)column.Default.Value;

            Assert.That(value.Value, Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void DataType_Table_default_function()
        {
            var result = parser.Parse<DataType.Table>("table(a uniqueidentifier default abs(1), b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();

            Assert.That(column.Default.Value, Is.InstanceOf<DefaultValue.Value>());

            var value = (DefaultValue.Value)column.Default.Value;

            Assert.That(value.Value, Is.InstanceOf<Function.System>());
        }

        [Test]
        public void DataType_Table_default_expression_2()
        {
            var result = parser.Parse<DataType.Table>("table(a varchar(10) default abs(1) + 'a', b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();

            Assert.That(column.Default.Value, Is.InstanceOf<DefaultValue.Value>());

            var value = (DefaultValue.Value)column.Default.Value;

            Assert.That(value.Value, Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void DataType_Table_column_primary_key()
        {
            var result = parser.Parse<DataType.Table>("table(a varchar(10) primary key, b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();
            var constraint = (ColumnConstraint.Simple)column.Constraints.First();

            Assert.That(constraint.Name.Value, Is.EqualTo("primary key"));
        }

        [Test]
        public void DataType_Table_column_unique()
        {
            var result = parser.Parse<DataType.Table>("table(a varchar(10) unique, b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();
            var constraint = (ColumnConstraint.Simple)column.Constraints.First();

            Assert.That(constraint.Name.Value, Is.EqualTo("unique"));
        }

        [Test]
        public void DataType_Table_column_null()
        {
            var result = parser.Parse<DataType.Table>("table(a varchar(10) null, b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();
            var constraint = (ColumnConstraint.Simple)column.Constraints.First();

            Assert.That(constraint.Name.Value, Is.EqualTo("null"));
        }

        [Test]
        public void DataType_Table_column_not_null()
        {
            var result = parser.Parse<DataType.Table>("table(a varchar(10) not null, b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();
            var constraint = (ColumnConstraint.Simple)column.Constraints.First();

            Assert.That(constraint.Name.Value, Is.EqualTo("not null"));
        }

        [Test]
        public void DataType_Table_column_identity()
        {
            var result = parser.Parse<DataType.Table>("table(a int identity, b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.ElementAt(0);

            Assert.That(column.Default.Value, Is.InstanceOf<DefaultValue.Identity>());
        }

        [Test]
        public void DataType_Table_column_identity_parameters()
        {
            var result = parser.Parse<DataType.Table>("table(a int identity(1, 2), b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.ElementAt(0);
            var identity = (DefaultValue.Identity)column.Default.Value;

            Assert.That(identity.Seed.Value, Is.EqualTo(1));
            Assert.That(identity.Increment.Value, Is.EqualTo(2));
        }

        [Test]
        public void DataType_Table_column_identity_missing_parameters()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<DataType.Table>("table(a int identity(1), b int)"));
        }
        
        [Test]
        public void DataType_Table_rowguidcol()
        {
            var result = parser.Parse<DataType.Table>("table(a uniqueidentifier rowguidcol, b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();

            Assert.That(column.RowGuidColumn.Value, Is.True);
        }

        [Test]
        public void DataType_Table_column_collation()
        {
            var result = parser.Parse<DataType.Table>("table(a varchar(10) collate abc, b int)");

            Assert.That(result.Items.Count(), Is.EqualTo(2));

            var column = (TableDataTypeColumnItem)result.Items.First();

            Assert.That(column.Collation.Value.Name.Value, Is.EqualTo("abc"));
        }

        [Test]
        public void DataType_Table_collation_missing()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<DataType.Table>("table(a varchar(10) collate, b int)"));
        }
    }
}