﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class HavingTest
    {
        private StartRuleDescriptor parser = TSql.Having;

        [Test]
        public void Should_parse_having()
        {
            var result = parser.Parse<Predicate>("HAVING a > @a");

            Assert.That(result, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void Should_not_parse_incomplete_having_clause()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate>("HAVING"));
        }
    }
}