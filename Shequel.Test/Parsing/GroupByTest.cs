﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class GroupByTest
    {
        private StartRuleDescriptor parser = TSql.GroupBy;

        [Test]
        public void GroupBy()
        {
            var result = parser.Parse<GroupBy>("GROUP BY ColumnA + ColumnB, ColumnB");

            Assert.That(result.Items.Count(), Is.EqualTo(2));
            Assert.That(result.Items.First(), Is.InstanceOf<GroupByItem.Simple>());

            var second = (GroupByItem.Simple)result.Items.ElementAt(1);

            Assert.That(second.Expression, Is.InstanceOf<ColumnIdentifier>());
        }

        [Test]
        public void Should_parse_cube_item()
        {
            var result = parser.Parse<GroupBy>("GROUP BY CUBE ((a, b), (c, d))");

            Assert.That(result.Items.Count(), Is.EqualTo(1));
            Assert.That(result.Items.First(), Is.InstanceOf<GroupByItem.Cube>());

            var item = (GroupByItem.Cube)result.Items.First();

            Assert.That(item.Rows.Count(), Is.EqualTo(2));

            var row = item.Rows.First();

            Assert.That(row.Expressions.Count(), Is.EqualTo(2));

            var expression = row.Expressions.First();

            Assert.That(expression, Is.InstanceOf<ColumnIdentifier>());
        }

        [Test]
        public void Should_parse_rollup_item()
        {
            var result = parser.Parse<GroupBy>("GROUP BY ROLLUP ((a, b), (c, d))");

            Assert.That(result.Items.Count(), Is.EqualTo(1));
            Assert.That(result.Items.First(), Is.InstanceOf<GroupByItem.Rollup>());

            var item = (GroupByItem.Rollup)result.Items.First();

            Assert.That(item.Rows.Count(), Is.EqualTo(2));

            var row = item.Rows.First();

            Assert.That(row.Expressions.Count(), Is.EqualTo(2));

            var expression = row.Expressions.First();

            Assert.That(expression, Is.InstanceOf<ColumnIdentifier>());
        }

        [Test]
        public void Should_parse_grouping_sets_item()
        {
            var result = parser.Parse<GroupBy>("GROUP BY GROUPING SETS ((a, b), (DATEPART(yyyy, a), DATEPART(mm, b)))");

            Assert.That(result.Items.Count(), Is.EqualTo(1));
            Assert.That(result.Items.First(), Is.InstanceOf<GroupByItem.GroupingSets>());

            var item = (GroupByItem.GroupingSets)result.Items.First();

            Assert.That(item.Sets.Count(), Is.EqualTo(2));

            var set = item.Sets.First();

            Assert.That(set.Rows.Count(), Is.EqualTo(2));

            var row = set.Rows.First();

            Assert.That(row, Is.InstanceOf<GroupByItem.Simple>());
        }

        [Test]
        public void Should_parse_group_by_non_iso_cube()
        {
            var result = parser.Parse<GroupBy>("GROUP BY a with cube");

            Assert.That(result, Is.InstanceOf<GroupBy.CompatibleSyntax>());
            Assert.That(result.Items.Count(), Is.EqualTo(1));
            Assert.That(result.Items.First(), Is.InstanceOf<GroupByItem.Simple>());

            var item = (GroupByItem.Simple)result.Items.First();

            Assert.That(item.Expression, Is.InstanceOf<ColumnIdentifier>());

            var groupBy = (GroupBy.CompatibleSyntax)result;

            Assert.That(groupBy.Kind.Value, Is.EqualTo(GroupByKind.Cube));
        }

        [Test]
        public void Should_parse_group_by_non_iso_rollup()
        {
            var result = parser.Parse<GroupBy>("GROUP BY a with rollup");

            Assert.That(result, Is.InstanceOf<GroupBy.CompatibleSyntax>());
            Assert.That(result.Items.Count(), Is.EqualTo(1));
            Assert.That(result.Items.First(), Is.InstanceOf<GroupByItem.Simple>());

            var item = (GroupByItem.Simple)result.Items.First();

            Assert.That(item.Expression, Is.InstanceOf<ColumnIdentifier>());

            var groupBy = (GroupBy.CompatibleSyntax)result;

            Assert.That(groupBy.Kind.Value, Is.EqualTo(GroupByKind.Rollup));
        }

        [Test]
        public void Should_parse_group_by_non_iso_all()
        {
            var result = parser.Parse<GroupBy>("GROUP BY all a with rollup");

            Assert.That(result, Is.InstanceOf<GroupBy.CompatibleSyntax>());
            Assert.That(result.Items.Count(), Is.EqualTo(1));
            Assert.That(result.Items.First(), Is.InstanceOf<GroupByItem.Simple>());

            var item = (GroupByItem.Simple)result.Items.First();

            Assert.That(item.Expression, Is.InstanceOf<ColumnIdentifier>());

            var groupBy = (GroupBy.CompatibleSyntax)result;

            Assert.That(groupBy.All.Value, Is.True);
        }
    }
}