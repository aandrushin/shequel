﻿using System.Linq;
using System.Linq.Expressions;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class PredicateTest
    {
        public StartRuleDescriptor parser = TSql.Predicate;

        [Test]
        public void Predicate_Comparison()
        {
            var result = parser.Parse<Predicate.Comparison>("1 < 2 + 1");

            Assert.That(result.Operator.Value, Is.EqualTo("<"));
            Assert.That(result.Left, Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Right, Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void Predicate_Comparison_operators()
        {
            var operators = new[] { "=", ">", "<", "< =", "<=", "> =", ">=", "< >", "<>", "! =", "!=", "! >", "!>", "! <", "!<" };

            foreach (var @operator in operators)
            {
                var result = parser.Parse<Predicate.Comparison>(string.Format("a {0} b", @operator));

                var fixedName = string.Join(string.Empty, @operator.Split(' '));
                
                Assert.That(result.Operator.Value, Is.EqualTo(fixedName));
            }
        }

        [Test]
        public void Predicate_Unary()
        {
            var result = parser.Parse<Predicate.Unary>("NOT 1 = 2");

            Assert.That(result.Operator.Value, Is.EqualTo("not"));
            Assert.That(result.Predicate, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void Predicate_Unary_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Unary>("NOT"));
        }

        [Test]
        public void Predicate_Binary()
        {
            var result = parser.Parse<Predicate.Binary>("1 > 2 AND a = b");

            Assert.That(result.Operator.Value, Is.EqualTo("and"));
            Assert.That(result.Left, Is.InstanceOf<Predicate.Comparison>());
            Assert.That(result.Right, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void Predicate_Contains()
        {
            var result = parser.Parse<Predicate.Contains>("CONTAINS(column_name, 'NEAR ((Monday, Tuesday, Wednesday), MAX, TRUE)') ");

            Assert.That(result.Parameters.First().Column.Value, Is.EqualTo("column_name"));
            Assert.That(((ContainsConditionExpression)result.Condition).Criteria.Value, Is.EqualTo("'NEAR ((Monday, Tuesday, Wednesday), MAX, TRUE)'"));
        }

        [Test]
        public void Predicate_Contains_multiple_parameters()
        {
            var result = parser.Parse<Predicate.Contains>("CONTAINS((Name, Color), 'Red')");

            Assert.That(result.Parameters.Count(), Is.EqualTo(2));
        }

        [Test]
        public void Predicate_Contains_parameter()
        {
            var result = parser.Parse<Predicate.Contains>("CONTAINS(column_name, @a) ");

            Assert.That(result.Condition, Is.InstanceOf<Parameter>());
        }

        [Test]
        public void Predicate_Contains_property()
        {
            Assert.Inconclusive("CONTAINS(PROPERTY(Document,'Title'), 'Maintenance OR Repair')");
        }

        [Test]
        public void Predicate_Contains_language()
        {
            var result = parser.Parse<Predicate.Contains>("CONTAINS((a, b), 'c', LANGUAGE 1)");

            Assert.That(result.Language.HasValue);
            Assert.That(((Constant.Integer)result.Language.Value).ParsedValue.Value, Is.EqualTo(1));

        }

        [Test]
        public void Predicate_Contains_language_hex()
        {
            var result = parser.Parse<Predicate.Contains>("CONTAINS((a, b), 'c', LANGUAGE 0x0f)");

            Assert.That(result.Language.HasValue);
            Assert.That(((Constant.Binary)result.Language.Value).ParsedValue.Value, Is.EqualTo("0x0f"));
        }

        [Test]
        public void Predicate_Contains_language_string()
        {
            var result = parser.Parse<Predicate.Contains>("CONTAINS((a, b), 'c', LANGUAGE 'language_term')");

            Assert.That(result.Language.HasValue);
            Assert.That(((Constant.String)result.Language.Value).ParsedValue.Value, Is.EqualTo("'language_term'"));
        }

        [Test]
        public void Predicate_Contains_language_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Contains>("CONTAINS()"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Contains>("CONTAINS(a)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Contains>("CONTAINS(a, )"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Contains>("CONTAINS((a, ))"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Contains>("CONTAINS((a, ), )"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Contains>("CONTAINS(a, 'a', )"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Contains>("CONTAINS(a, 'a', LANGUAGE )"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Contains>("CONTAINS(PROPERTY(a, ), 'a')"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Contains>("CONTAINS(PROPERTY(a), 'a')"));
        }

        [Test]
        public void Predicate_Freetext()
        {
            var result = parser.Parse<Predicate.Freetext>("FREETEXT(a, 'b')");

            Assert.That(result.Parameters.First().Column.Value, Is.EqualTo("a"));
            Assert.That(((Constant.String)result.Condition).ParsedValue.Value, Is.EqualTo("'b'"));
        }

        [Test]
        public void Predicate_Freetext_parameter()
        {
            var result = parser.Parse<Predicate.Freetext>("FREETEXT(a, @b)");

            Assert.That(result.Condition, Is.InstanceOf<Parameter>());
        }

        [Test]
        public void Predicate_Freetext_expression()
        {
            var result = parser.Parse<Predicate.Freetext>("FREETEXT(a, 'a' + 'b') ");

            Assert.That(result.Condition, Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void Predicate_Freetext_parameters()
        {
            var result = parser.Parse<Predicate.Freetext>("FREETEXT((a, b), 'c')");

            Assert.That(result.Parameters.Count(), Is.EqualTo(2));
        }

        [Test]
        public void Predicate_Freetext_language()
        {
            var result = parser.Parse<Predicate.Freetext>("FREETEXT((a, b), 'c', LANGUAGE 1)");

            Assert.That(result.Language.HasValue);
            Assert.That(((Constant.Integer)result.Language.Value).ParsedValue.Value, Is.EqualTo(1));
        }

        [Test]
        public void Predicate_Freetext_language_hex()
        {
            var result = parser.Parse<Predicate.Freetext>("FREETEXT((a, b), 'c', LANGUAGE 0x0f)");

            Assert.That(result.Language.HasValue);
            Assert.That(((Constant.Binary)result.Language.Value).ParsedValue.Value, Is.EqualTo("0x0f"));
        }

        [Test]
        public void Predicate_Freetext_language_string()
        {
            var result = parser.Parse<Predicate.Freetext>("FREETEXT((a, b), 'c', LANGUAGE 'language_term')");

            Assert.That(result.Language.HasValue);
            Assert.That(((Constant.String)result.Language.Value).ParsedValue.Value, Is.EqualTo("'language_term'"));
        }

        [Test]
        public void Predicate_Freetext_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Freetext>("FREETEXT()"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Freetext>("FREETEXT(a)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Freetext>("FREETEXT(a, )"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Freetext>("FREETEXT((a, ))"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Freetext>("FREETEXT((a, ), )"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Freetext>("FREETEXT(a, 'a', )"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Freetext>("FREETEXT(a, 'a', LANGUAGE )"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Freetext>("FREETEXT(PROPERTY(a, ), 'a')"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Freetext>("FREETEXT(PROPERTY(a), 'a')"));
        }

        [Test]
        public void Predicate_Between()
        {
            var result = parser.Parse<Predicate.Between>("a between 1 + 2 and 2");

            Assert.That(result.Not.HasValue, Is.False);
            Assert.That(result.Expression, Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Start, Is.InstanceOf<Expression.Binary>());
            Assert.That(result.End, Is.InstanceOf<Constant.Integer>());
        }

        [Test]
        public void Predicate_Between_not()
        {
            var result = parser.Parse<Predicate.Between>("1 not between a and c");

            Assert.That(result.Not.HasValue, Is.True);
            Assert.That(result.Expression, Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Start, Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.End, Is.InstanceOf<ColumnIdentifier>());
        }

        [Test]
        public void Predicate_Between_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse <Predicate.Between>("between a and b"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Between>("1 between a and "));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Between>("1 between and b"));
        }

        [Test]
        public void Predicate_InValues()
        {
            var result = parser.Parse<Predicate.InValues>("1 in (1, 2 + c)");

            Assert.That(result.Expression, Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Values.Count(), Is.EqualTo(2));
            Assert.That(result.Values.ElementAt(0), Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Values.ElementAt(1), Is.InstanceOf<Expression.Binary>());
            Assert.That(result.Not.HasValue, Is.False);
        }

        [Test]
        public void Predicate_InValues_not()
        {
            var result = parser.Parse<Predicate.InValues>("a + 1 not in (1, 2)");

            Assert.That(result.Expression, Is.InstanceOf<Expression.Binary>());
            Assert.That(result.Values.Count(), Is.EqualTo(2));
            Assert.That(result.Values.ElementAt(0), Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Values.ElementAt(1), Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Not.Value, Is.True);
        }

        [Test]
        public void Predicate_InValues_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse <Predicate.InValues>("a + 1 not in ()"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.InValues>("in (1, 2)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.InValues>("a + 1 in"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.InValues>("in"));
        }
        
        [Test]
        public void Predicate_Like()
        {
            var result = parser.Parse<Predicate.Like>("a like '%a' + '%'");

            Assert.That(result.Expression, Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Pattern, Is.InstanceOf<Expression.Binary>());
            Assert.That(result.Not.HasValue, Is.False);
            Assert.That(result.EscapeChar.HasValue, Is.False);
        }

        [Test]
        public void Predicate_Like_not()
        {
            var result = parser.Parse<Predicate.Like>("'a' + 'b' not like 'a%'");

            Assert.That(result.Expression, Is.InstanceOf<Expression.Binary>());
            Assert.That(result.Pattern, Is.InstanceOf<Constant.String>());
            Assert.That(result.Not.Value, Is.True);
            Assert.That(result.EscapeChar.HasValue, Is.False);
        }

        [Test]
        public void Predicate_Like_escape_char()
        {
            var result = parser.Parse<Predicate.Like>("'a' + 'b' not like 'a%' escape 'b'");

            Assert.That(result.Expression, Is.InstanceOf<Expression.Binary>());
            Assert.That(result.Pattern, Is.InstanceOf<Constant.String>());
            Assert.That(result.Not.Value, Is.True);
            Assert.That(result.EscapeChar.Value, Is.EqualTo("'b'"));
        }

        [Test]
        public void Predicate_Like_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse <Predicate.Like>("like '%a'"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Like>("a like"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Like>("a like @a escape"));
        }

        [Test]
        public void Predicate_IsNull()
        {
            var result = parser.Parse<Predicate.IsNull>("a is null");

            Assert.That(result.Expression, Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Not.HasValue, Is.False);
        }

        [Test]
        public void Predicate_IsNull_not()
        {
            var result = parser.Parse<Predicate.IsNull>("@a is not null");

            Assert.That(result.Expression, Is.InstanceOf<Parameter>());
            Assert.That(result.Not.Value, Is.True);
        }

        [Test]
        public void Predicate_IsNull_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.IsNull>("is null"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.IsNull>("a is "));
        }

        [Test]
        public void Predicate_Sub()
        {
            var result = parser.Parse<Predicate.Sub>("(NOT a = 1)");

            Assert.That(result.Inner, Is.InstanceOf<Predicate.Unary>());
        }

        [Test]
        public void Predicate_Sub_empty()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate.Sub>("()"));
        }

        [Test]
        public void Predicate_Some()
        {
            var result = parser.Parse<Predicate.QueryComparison>("1 > some (select 1)");

            Assert.That(result.Expression, Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Operator.Value, Is.EqualTo(">"));
            Assert.That(result.SubQuery.Projection.Count(), Is.EqualTo(1));
            Assert.That(result.Mode.Value, Is.EqualTo(QueryComparisonMode.Some));
        }

        [Test]
        public void Predicate_Any()
        {
            var result = parser.Parse<Predicate.QueryComparison>("1 > any (select 1)");

            Assert.That(result.Expression, Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Operator.Value, Is.EqualTo(">"));
            Assert.That(result.SubQuery.Projection.Count(), Is.EqualTo(1));
            Assert.That(result.Mode.Value, Is.EqualTo(QueryComparisonMode.Any));
        }

        [Test]
        public void Predicate_All()
        {
            var result = parser.Parse<Predicate.QueryComparison>("1 = all (select 1)");

            Assert.That(result.Expression, Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Operator.Value, Is.EqualTo("="));
            Assert.That(result.SubQuery.Projection.Count(), Is.EqualTo(1));
            Assert.That(result.Mode.Value, Is.EqualTo(QueryComparisonMode.All));
        }
    }
}