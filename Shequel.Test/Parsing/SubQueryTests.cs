﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class SubQueryTests
    {
        private StartRuleDescriptor parser = TSql.SubQuery;

        [Test]
        public void SubQuery()
        {
            var result = parser.Parse<SubQuery>("(select a)");

            Assert.That(result.Projection.Count(), Is.EqualTo(1));
        }

        [Test]
        public void SubQuery_constant()
        {
            var result = parser.Parse<SubQuery>("(select 1)");

            Assert.That(result.Projection.Count(), Is.EqualTo(1));
        }

        [Test]
        public void SubQuery_all()
        {
            var result = parser.Parse<SubQuery>("(select all a)");

            Assert.That(result.Distinct.HasValue, Is.True);
            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void SubQuery_distinct()
        {
            var result = parser.Parse<SubQuery>("(select distinct a)");

            Assert.That(result.Distinct.HasValue, Is.True);
            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void SubQuery_top()
        {
            var result = parser.Parse<SubQuery>("(select top 1 a)");

            Assert.That(result.Top.HasValue);
            Assert.That(result.Top.Value.Expression, Is.InstanceOf<Constant.Integer>());
        }

        [Test]
        public void SubQuery_from()
        {
            var result = parser.Parse<SubQuery>("(select a from b)");

            Assert.That(result.From.Any);
            Assert.That(result.From.First(), Is.InstanceOf<RelationDeclaration.ObjectRelation>());
        }

        [Test]
        public void SubQuery_where()
        {
            var result = parser.Parse<SubQuery>("(select a from b where b.a < 1)");

            Assert.That(result.Where.HasValue);
            Assert.That(result.Where.Value, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void SubQuery_group_by()
        {
            var result = parser.Parse<SubQuery>("(select a from b group by c)");

            Assert.That(result.GroupBy.HasValue);
            Assert.That(result.GroupBy.Value.Items.Count(), Is.EqualTo(1));
        }

        [Test]
        public void SubQuery_having()
        {
            var result = parser.Parse<SubQuery>("(select a from b group by c having b.a < 2)");

            Assert.That(result.Having.HasValue);
            Assert.That(result.Having.Value, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void SubQuery_order_by()
        {
            var result = parser.Parse<SubQuery>("(select a from b order by a)");

            Assert.That(result.OrderBy.HasValue);
            Assert.That(result.OrderBy.Value.Items.Count(), Is.EqualTo(1));
        }

        [Test]
        public void SubQuery_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("()"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("(select)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("(select top)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("(select from a)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("(select b from)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("(select b where)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("(select b order by)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("(select b group by)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("(select b having)"));
        }
    }
}