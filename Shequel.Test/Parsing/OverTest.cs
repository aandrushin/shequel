﻿using System.Linq;
using System.Linq.Expressions;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class OverTest
    {
        private StartRuleDescriptor parser = TSql.Over;

        [Test]
        public void Over()
        {
            var result = parser.Parse<Over>("over(order by a collate b desc, b asc, c)");

            Assert.That(result.OrderBy.Value.Items.Count(), Is.EqualTo(3));

            var member = result.OrderBy.Value.Items.First();
            Assert.That(member.Expression, Is.InstanceOf<ColumnIdentifier>());
            Assert.That(member.Collation.Value.Name.Value, Is.EqualTo("b"));
            Assert.That(member.Sort.Value, Is.EqualTo(SortMode.Desc));
        }

        [Test]
        public void Over_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Over>("over(order by )"));
        }

        [Test]
        public void Over_patition_by()
        {
            var result = parser.Parse<Over>("over(partition by a, b + 1, ' ')");

            Assert.That(result.PartitionBy.Value.Expressions.Count(), Is.EqualTo(3));

            var expressions = result.PartitionBy.Value.Expressions;
            Assert.That(expressions.ElementAt(0), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(expressions.ElementAt(1), Is.InstanceOf<Expression.Binary>());
            Assert.That(expressions.ElementAt(2), Is.InstanceOf<Constant.String>());
        }

        [Test]
        public void Over_patition_by_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Over>("over(partition by )"));
        }

        [Test]
        public void Over_rows()
        {
            var result = parser.Parse<Over>("over(rows unbounded preceding)");

            Assert.That(result.Window.Value.Mode.Value == OverWindowMode.Rows);
        }

        [Test]
        public void Over_range()
        {
            var result = parser.Parse<Over>("over(range unbounded preceding)");

            Assert.That(result.Window.Value.Mode.Value == OverWindowMode.Range);
        }

        [Test]
        public void Over_Window_between()
        {
            var result = parser.Parse<Over>("over(rows between current row and 1 following)");

            var frame = (OverWindowItem.Between) result.Window.Value.Item;

            Assert.That(frame.Start, Is.InstanceOf<OverWindowItem.CurrentRow>());

            var end = (OverWindowItem.Following)frame.End;

            Assert.That(end.RowsCount.Value, Is.EqualTo(1));
        }

        [Test]
        public void Over_Window_between_1()
        {
            var result = parser.Parse<Over>("over(rows between 1 preceding and current row)");

            var frame = (OverWindowItem.Between)result.Window.Value.Item;
            var start = (OverWindowItem.Preceding)frame.Start;

            Assert.That(start.RowsCount.Value, Is.EqualTo(1));
            Assert.That(frame.End, Is.InstanceOf<OverWindowItem.CurrentRow>());
        }

        [Test]
        public void Over_Window_onbounded_preceding()
        {
            var result = parser.Parse<Over>("over(rows unbounded preceding)");

            var frame = (OverWindowItem.Unbounded)result.Window.Value.Item;

            Assert.That(frame.Direction.Value, Is.EqualTo(WindowFrameDirection.Preceding));

        }

        [Test]
        public void Over_Window_onbounded_following()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Over>("over(rows unbounded following)"));
        }

        [Test]
        public void Over_Window_partition_by_order_by()
        {
            var result = parser.Parse<Over>("over(partition by a, b + 1, ' ' order by a desc, c asc, d rows unbounded preceding)");

            Assert.That(result.PartitionBy.HasValue);
            Assert.That(result.OrderBy.HasValue);
            Assert.That(result.Window.HasValue);
        }

        [Test]
        public void Over_1()
        {
            var result = parser.Parse<Over>("over()");

            Assert.That(result.OrderBy.HasValue, Is.False);
            Assert.That(result.PartitionBy.HasValue, Is.False);
            Assert.That(result.Window.HasValue, Is.False);
        }
    }
}