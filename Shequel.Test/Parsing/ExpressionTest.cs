﻿using System.Collections.Generic;
using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class ExpressionTest
    {
        private readonly ExtensibleRuleDescriptor parser = TSql.Expression;

        [Test]
        public void Expression_constants()
        {
            var expressions = parser
                .Parse<Expression>("N'1' + '1' + 1.0 + 0x1 + $1 + 1e0 + NULL")
                .Flatten()
                .ToArray();

            Assert.That(expressions.ElementAt(0), Is.InstanceOf<Constant.Null>());
            Assert.That(expressions.ElementAt(1), Is.InstanceOf<Constant.Real>());
            Assert.That(expressions.ElementAt(2), Is.InstanceOf<Constant.Money>());
            Assert.That(expressions.ElementAt(3), Is.InstanceOf<Constant.Binary>());
            Assert.That(expressions.ElementAt(4), Is.InstanceOf<Constant.Decimal>());
            Assert.That(expressions.ElementAt(5), Is.InstanceOf<Constant.String>());
            Assert.That(expressions.ElementAt(6), Is.InstanceOf<Constant.String>());
        }

        [Test]
        public void Expression_binary_operators()
        {
            var operators = parser
                .Parse<Expression>("1 + 1 - 1 & 1 | 1 * 1 / 1 % 1")
                .FlattenOperators()
                .ToArray();

            Assert.That(operators.ElementAt(0), Is.EqualTo("%"));
            Assert.That(operators.ElementAt(1), Is.EqualTo("/"));
            Assert.That(operators.ElementAt(2), Is.EqualTo("*"));
            Assert.That(operators.ElementAt(3), Is.EqualTo("|"));
            Assert.That(operators.ElementAt(4), Is.EqualTo("&"));
            Assert.That(operators.ElementAt(5), Is.EqualTo("-"));
            Assert.That(operators.ElementAt(6), Is.EqualTo("+"));
        }

        [Test]
        public void Expression_unary_operators()
        {
            var negate1 = parser.Parse<Expression.Unary>("~1");
            var negate2 = parser.Parse<Expression.Unary>("- -1");

            Assert.That(negate1.Operator.Value, Is.EqualTo("~"));
            Assert.That(negate2.Operator.Value, Is.EqualTo("-"));
        }

        [Test]
        public void Expression_Sub()
        {
            var expression = parser.Parse<Expression.Binary>("1 + (1 + 2)");

            Assert.That(expression.Right, Is.InstanceOf<Expression.Sub>());

            var sub = (Expression.Sub)expression.Right;

            Assert.That(sub.Inner, Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void Expression_column_reference()
        {
            var expression1 = parser.Parse<Expression.Binary>("1 + a");

            Assert.That(expression1.Right, Is.InstanceOf<ColumnIdentifier>());

            var expression2 = parser.Parse<Expression.Binary>("a.b + a");

            Assert.That(expression2.Left, Is.InstanceOf<ColumnIdentifier>());
            Assert.That(expression2.Right, Is.InstanceOf<ColumnIdentifier>());
        }

        [Test]
        public void Expression_parameter()
        {
            var expression1 = parser.Parse<Expression.Binary>("1 + @a");

            Assert.That(expression1.Right, Is.InstanceOf<Parameter>());

            var expression2 = parser.Parse<Expression.Binary>("a.b + @@a"); 

            Assert.That(expression2.Right, Is.InstanceOf<Parameter>());
        }

        [Test]
        public void Expression_Function()
        {
            var expression1 = parser.Parse<Expression.Binary>("1 + a()");

            Assert.That(expression1.Right, Is.InstanceOf<Function.UserDefined>());

            Assert.That(parser.Parse<Expression.Binary>("dbo.b() * 1").Left, Is.InstanceOf<Function.UserDefined>());
            Assert.That(parser.Parse<Expression.Binary>("abs(1) + 'a'").Left, Is.InstanceOf<Function.System>());
        }

        [Test]
        public void Expression_Function_with_parameters()
        {
            var result = parser.Parse<Function.UserDefined>("b.a(1, 2, 3)");

            Assert.That(result.Parameters.Count(), Is.EqualTo(3));
        }

        [Test]
        public void Expression_SimpleCase()
        {
            var result = parser.Parse<Expression.SimpleCase>("case @letter when 'a' then 'b' + 'f' when 'c' + 'e' then 'd' end");

            Assert.That(result.Expression, Is.InstanceOf<Parameter>());

            var first = result.Items.First();

            Assert.That(first.When, Is.InstanceOf<Constant.String>());
            Assert.That(first.Then, Is.InstanceOf<Expression.Binary>());

            var second = result.Items.Last();

            Assert.That(second.When, Is.InstanceOf<Expression.Binary>());
            Assert.That(second.Then, Is.InstanceOf<Constant.String>());

            Assert.That(result.Else.HasValue, Is.False);
        }


        [Test]
        public void Expression_SimpleCase_else()
        {
            var result = parser.Parse<Expression.SimpleCase>("case @letter when 'a' then 'b' + 'c' else 'd' end");

            Assert.That(result.Expression, Is.InstanceOf<Parameter>());

            var first = result.Items.First();

            Assert.That(first.When, Is.InstanceOf<Constant.String>());
            Assert.That(first.Then, Is.InstanceOf<Expression.Binary>());

            Assert.That(result.Else.HasValue, Is.True);
            Assert.That(result.Else.Value, Is.InstanceOf<Constant.String>());
        }

        [Test]
        public void Expression_SearchCase()
        {
            var result = parser.Parse<Expression.SearchCase>("case when 1 < 2 then 'b' + 'f' when 'c' like 'c%' then 'd' end");

            var first = result.Items.First();

            Assert.That(first.When, Is.InstanceOf<Predicate.Comparison>());
            Assert.That(first.Then, Is.InstanceOf<Expression.Binary>());

            var second = result.Items.Last();

            Assert.That(second.When, Is.InstanceOf<Predicate.Like>());
            Assert.That(second.Then, Is.InstanceOf<Constant.String>());

            Assert.That(result.Else.HasValue, Is.False);
        }

        [Test]
        public void Expression_SearchCase_else()
        {
            var result = parser.Parse<Expression.SearchCase> ("case when 1 < 2 then 'b' + 'c' else 'd' end");

            var first = result.Items.First();

            Assert.That(first.When, Is.InstanceOf<Predicate.Comparison>());
            Assert.That(first.Then, Is.InstanceOf<Expression.Binary>());

            Assert.That(result.Else.HasValue, Is.True);
            Assert.That(result.Else.Value, Is.InstanceOf<Constant.String>());
        }

        [Test]
        public void Expression_SimpleCase_failure()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Expression.SimpleCase>("case when 'a' then 'b' end"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Expression.SimpleCase>("case 1 when then 'b' end"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Expression.SimpleCase>("case 1 when 1 then end"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Expression.SimpleCase>("case 1 when 1 then 2 else end"));
        }

        [Test]
        public void Expression_SearchCase_failure()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Expression.SearchCase>("case when 'a' then 'b' end"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Expression.SearchCase>("case when then 'b' end"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Expression.SearchCase>("case when 1 < 2 then end"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Expression.SearchCase>("case when 1 > 2 then 2 else end"));
        }

        [Test]
        public void Expression_Sub_subquery()
        {
            Assert.That(parser.Parse<Expression>("(select a)"), Is.InstanceOf<SubQuery>());
        }
    }

    public static class ExpressionEx
    {
        public static IEnumerable<Expression> Flatten(this Expression expression)
        {
            var unary = expression as Expression.Unary;
            if (unary != null)
            {
                yield return unary.Expression;
            }
            else
            {
                var binary = expression as Expression.Binary;
                if (binary != null)
                {
                    foreach (var right in binary.Right.Flatten())
                    {
                        yield return right;
                    }

                    foreach (var left in binary.Left.Flatten())
                    {
                        yield return left;
                    }
                }
                else
                {
                    yield return expression;
                }
            }
        }

        public static IEnumerable<string> FlattenOperators(this Expression expression)
        {
            var unary = expression as Expression.Unary;
            if (unary != null)
            {
                yield return unary.Operator.Value;
            }
            else
            {
                var binary = expression as Expression.Binary;
                if (binary != null)
                {
                    foreach (var right in binary.Right.FlattenOperators())
                    {
                        yield return right;
                    }

                    yield return binary.Operator.Value;

                    foreach (var left in binary.Left.FlattenOperators())
                    {
                        yield return left;
                    }
                }
            }
        }
    }
}
