﻿using System.Linq;
using System.Linq.Expressions;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class AggregateFunctionTest
    {
        private StartRuleDescriptor parser = TSql.WindowFunction;

        [Test]
        public void WindowFunction_wildcard_distinct()
        {
            Assert.Inconclusive("count(distinct *)");
        }

        #region Avg
        [Test]
        public void Window_avg()
        {
            var result = parser.Parse<Function.Window>("AVG(a)");

            Assert.That(result.Name.Value, Is.EqualTo("avg"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);
            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_avg_all()
        {
            var result = parser.Parse<Function.Window>("avg(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_avg_distinct()
        {
            var result = parser.Parse<Function.Window>("avg(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_avg_over()
        {
            var result = parser.Parse<Function.Window>("avg(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_avg_over_partition_by()
        {
            var result = parser.Parse<Function.Window>("avg(distinct a) over (partition by b order by c)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
            Assert.That(result.Over.Value.PartitionBy.HasValue);
            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }
        #endregion

        #region Checksum_Agg
        [Test]
        public void Window_checksum_agg()
        {
            var result = parser.Parse<Function.Window>("CHECKSUM_AGG(1)");

            Assert.That(result.Name.Value, Is.EqualTo("checksum_agg"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Over.HasValue, Is.False);
            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_checksum_agg__all()
        {
            var result = parser.Parse<Function.Window>("checksum_agg(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_checksum_agg_distinct()
        {
            var result = parser.Parse<Function.Window>("checksum_agg(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }
        #endregion

        #region Count
        [Test]
        public void Window_count()
        {
            var result = parser.Parse<Function.Window>("count(a)");

            Assert.That(result.Name.Value, Is.EqualTo("count"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_count_all()
        {
            var result = parser.Parse<Function.Window>("count(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_count_distinct()
        {
            var result = parser.Parse<Function.Window>("count(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_count_wildcard()
        {
            var result = parser.Parse<Function.Window>("count(*)");

            Assert.That(result.Parameters.First(), Is.InstanceOf<WildcardExpression>());
        }

        [Test]
        public void Window_count_over()
        {
            var result = parser.Parse<Function.Window>("count(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_count_partition_by()
        {
            var result = parser.Parse<Function.Window>("count(a) over (partition by b)");

            Assert.That(result.Over.Value.PartitionBy.HasValue);
        }

        [Test]
        public void Window_count_rows()
        {
            var result = parser.Parse<Function.Window>("count(a) over (rows between current row and 1 following)");

            Assert.That(result.Over.Value.Window.HasValue);
        }
        #endregion

        #region Count_Big
        [Test]
        public void Window_count_big()
        {
            var result = parser.Parse<Function.Window>("count_big(a)");

            Assert.That(result.Name.Value, Is.EqualTo("count_big"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);
            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_count_big_all()
        {
            var result = parser.Parse<Function.Window>("count_big(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_count_big_distinct()
        {
            var result = parser.Parse<Function.Window>("count_big(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_count_big_wildcard()
        {
            var result = parser.Parse<Function.Window>("count_big(*)");

            Assert.That(result.Parameters.First(), Is.InstanceOf<WildcardExpression>());
        }

        [Test]
        public void Window_count_big_over()
        {
            var result = parser.Parse<Function.Window>("count_big(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_count_big_partition_by()
        {
            var result = parser.Parse<Function.Window>("count_big(a) over (partition by b)");

            Assert.That(result.Over.Value.PartitionBy.HasValue);
        }
        #endregion

        #region Grouping 
        [Test]
        public void Window_grouping_column()
        {
            var result = parser.Parse<Function.Window>("GROUPING(a)");

            Assert.That(result.Name.Value, Is.EqualTo("grouping"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);
        }

        [Test]
        public void Window_grouping_expression()
        {
            var result = parser.Parse<Function.Window>("grouping(a + 1)");

            Assert.That(result.Name.Value, Is.EqualTo("grouping"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<Expression.Binary>());
            Assert.That(result.Over.HasValue, Is.False);
        }
        #endregion 

        #region Grouping_Id
        [Test]
        public void Window_grouping_id()
        {
            var result = parser.Parse<Function.Window>("GROUPING_ID(a, a + 1)");

            Assert.That(result.Name.Value, Is.EqualTo("grouping_id"));
            Assert.That(result.Parameters.Count(), Is.EqualTo(2));
        }
        #endregion

        #region Max
        [Test]
        public void Window_max()
        {
            var result = parser.Parse<Function.Window>("MAX(a)");

            Assert.That(result.Name.Value, Is.EqualTo("max"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_max_all()
        {
            var result = parser.Parse<Function.Window>("max(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_max_distinct()
        {
            var result = parser.Parse<Function.Window>("max(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_max_over()
        {
            var result = parser.Parse<Function.Window>("max(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_max_partition_by()
        {
            var result = parser.Parse<Function.Window>("max(distinct a) over (partition by b order by c)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
            Assert.That(result.Over.Value.PartitionBy.HasValue);
        }
        #endregion

        #region Min
        [Test]
        public void Window_min()
        {
            var result = parser.Parse<Function.Window>("MIN(a)");

            Assert.That(result.Name.Value, Is.EqualTo("min"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_min_all()
        {
            var result = parser.Parse<Function.Window>("min(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_min_distinct()
        {
            var result = parser.Parse<Function.Window>("min(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_min_over()
        {
            var result = parser.Parse<Function.Window>("min(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_min_partition_by()
        {
            var result = parser.Parse<Function.Window>("min(distinct a) over (partition by b order by c)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
            Assert.That(result.Over.Value.PartitionBy.HasValue);
        }
        #endregion

        #region Sum
        [Test]
        public void Window_sum()
        {
            var result = parser.Parse<Function.Window>("SUM(a)");

            Assert.That(result.Name.Value, Is.EqualTo("sum"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_sum_all()
        {
            var result = parser.Parse<Function.Window>("sum(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_sum_distinct()
        {
            var result = parser.Parse<Function.Window>("sum(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_sum_over()
        {
            var result = parser.Parse<Function.Window>("sum(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_sum_partition_by()
        {
            var result = parser.Parse<Function.Window>("sum(distinct a) over (partition by b order by c)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
            Assert.That(result.Over.Value.PartitionBy.HasValue);
        }
        #endregion

        #region StDev
        [Test]
        public void Window_stdev()
        {
            var result = parser.Parse<Function.Window>("STDEV(a)");

            Assert.That(result.Name.Value, Is.EqualTo("stdev"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_stdev_all()
        {
            var result = parser.Parse<Function.Window>("stdev(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_stdev_distinct()
        {
            var result = parser.Parse<Function.Window>("stdev(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_stdev_over()
        {
            var result = parser.Parse<Function.Window>("stdev(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_stdev_partition_by()
        {
            var result = parser.Parse<Function.Window>("stdev(distinct a) over (partition by b order by c)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
            Assert.That(result.Over.Value.PartitionBy.HasValue);
        }
        #endregion

        #region StDevP
        [Test]
        public void Window_stdevp()
        {
            var result = parser.Parse<Function.Window>("STDEVP(a)");

            Assert.That(result.Name.Value, Is.EqualTo("stdevp"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_stdevp_all()
        {
            var result = parser.Parse<Function.Window>("stdevp(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_stdevp_distinct()
        {
            var result = parser.Parse<Function.Window>("stdevp(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_stdevp_over()
        {
            var result = parser.Parse<Function.Window>("stdevp(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_stdevp_partition_by()
        {
            var result = parser.Parse<Function.Window>("stdevp(distinct a) over (partition by b order by c)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
            Assert.That(result.Over.Value.PartitionBy.HasValue);
        }
        #endregion

        #region Var
        [Test]
        public void Window_var()
        {
            var result = parser.Parse<Function.Window>("VAR(a)");

            Assert.That(result.Name.Value, Is.EqualTo("var"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_var_all()
        {
            var result = parser.Parse<Function.Window>("var(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_var_distinct()
        {
            var result = parser.Parse<Function.Window>("var(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_var_over()
        {
            var result = parser.Parse<Function.Window>("var(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_var_partition_by()
        {
            var result = parser.Parse<Function.Window>("var(distinct a) over (partition by b order by c)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
            Assert.That(result.Over.Value.PartitionBy.HasValue);
        }
        #endregion

        #region VarP
        [Test]
        public void Window_varp()
        {
            var result = parser.Parse<Function.Window>("VARP(a)");

            Assert.That(result.Name.Value, Is.EqualTo("varp"));
            Assert.That(result.Parameters.First(), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Over.HasValue, Is.False);

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_varp_all()
        {
            var result = parser.Parse<Function.Window>("varp(all a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Window_varp_distinct()
        {
            var result = parser.Parse<Function.Window>("varp(distinct a)");

            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Window_varp_over()
        {
            var result = parser.Parse<Function.Window>("varp(a) over (order by b)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
        }

        [Test]
        public void Window_varp_partition_by()
        {
            var result = parser.Parse<Function.Window>("varp(distinct a) over (partition by b order by c)");

            Assert.That(result.Over.Value.OrderBy.HasValue);
            Assert.That(result.Over.Value.PartitionBy.HasValue);
        }
        #endregion
    }
}