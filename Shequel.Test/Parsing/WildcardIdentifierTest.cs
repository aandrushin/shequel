﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class WildcardIdentifierTest
    {
        private StartRuleDescriptor parser = TSql.WildcardIdentifier;

        [Test]
        public void WildcardIdentifier_Simple()
        {
            var identifier = parser.Parse<ColumnIdentifier>("*");

            Assert.That(identifier.Column.Value, Is.EqualTo("*"));
            Assert.That(identifier.Table.Value, Is.EqualTo(string.Empty));
            Assert.That(identifier.Schema.Value, Is.EqualTo(string.Empty));
            Assert.That(identifier.Database.Value, Is.EqualTo(string.Empty));
        }

        [Test]
        public void WildcardIdentifier_TwoPart()
        {
            var identifier = parser.Parse<ColumnIdentifier>("a.*");

            Assert.That(identifier.Column.Value, Is.EqualTo("*"));
            Assert.That(identifier.Table.Value, Is.EqualTo("a"));
            Assert.That(identifier.Schema.Value, Is.EqualTo(string.Empty));
            Assert.That(identifier.Database.Value, Is.EqualTo(string.Empty));
        }

        [Test]
        public void WildcardIdentifier_ThreePart()
        {
            var identifier = parser.Parse<ColumnIdentifier>("b.a.*");

            Assert.That(identifier.Column.Value, Is.EqualTo("*"));
            Assert.That(identifier.Table.Value, Is.EqualTo("a"));
            Assert.That(identifier.Schema.Value, Is.EqualTo("b"));
            Assert.That(identifier.Database.Value, Is.EqualTo(string.Empty));
        }

        [Test]
        public void WildcardIdentifier_FourPart()
        {
            var identifier = parser.Parse<ColumnIdentifier>("c.b.a.*");

            Assert.That(identifier.Column.Value, Is.EqualTo("*"));
            Assert.That(identifier.Table.Value, Is.EqualTo("a"));
            Assert.That(identifier.Schema.Value, Is.EqualTo("b"));
            Assert.That(identifier.Database.Value, Is.EqualTo("c"));
        }

        [Test]
        public void WildcardIdentifier_FourPart_too_long_identifier()
        {
            Assert.Throws<ParsingFailureException>(
                () => parser.Parse<ObjectIdentifier>("[e].[d].[c].[b].[a]"));
        }
    }
}