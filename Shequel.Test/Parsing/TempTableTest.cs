﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class TempTableTest
    {
        private StartRuleDescriptor parser = TSql.TempTable;

        [Test]
        public void TempTable_Local()
        {
            var result = parser.Parse<TempTable>("#a");

            Assert.That(result.Name.Value, Is.EqualTo("#a"));
            Assert.That(result.Kind.Value, Is.EqualTo(TempTableKind.Local));
        }

        [Test]
        public void TempTable_Global()
        {
            var result = parser.Parse<TempTable>("##a");

            Assert.That(result.Name.Value, Is.EqualTo("##a"));
            Assert.That(result.Kind.Value, Is.EqualTo(TempTableKind.Global));
        }
    }
}