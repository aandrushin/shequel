﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class ColumnIdentifierTest
    {
        private StartRuleDescriptor parser = TSql.ColumnIdentifier;


        [Test]
        public void ColumnIdentifier_Simple()
        {
            var identifier = parser.Parse<ColumnIdentifier>("a");

            Assert.That(identifier.Column.Value, Is.EqualTo("a"));
            Assert.That(identifier.Table.Value, Is.EqualTo(string.Empty));
            Assert.That(identifier.Schema.Value, Is.EqualTo(string.Empty));
            Assert.That(identifier.Database.Value, Is.EqualTo(string.Empty));
        }

        [Test]
        public void ColumnIdentifier_TwoPart()
        {
            var identifier = parser.Parse<ColumnIdentifier>("b.a");

            Assert.That(identifier.Column.Value, Is.EqualTo("a"));
            Assert.That(identifier.Table.Value, Is.EqualTo("b"));
            Assert.That(identifier.Schema.Value, Is.EqualTo(string.Empty));
            Assert.That(identifier.Database.Value, Is.EqualTo(string.Empty));
        }

        [Test]
        public void ColumnIdentifier_ThreePart()
        {
            var identifier = parser.Parse<ColumnIdentifier>("c.b.a");

            Assert.That(identifier.Column.Value, Is.EqualTo("a"));
            Assert.That(identifier.Table.Value, Is.EqualTo("b"));
            Assert.That(identifier.Schema.Value, Is.EqualTo("c"));
            Assert.That(identifier.Database.Value, Is.EqualTo(string.Empty));
        }

        [Test]
        public void ColumnIdentifier_FourPart()
        {
            var identifier = parser.Parse<ColumnIdentifier>("d.c.b.a");

            Assert.That(identifier.Column.Value, Is.EqualTo("a"));
            Assert.That(identifier.Table.Value, Is.EqualTo("b"));
            Assert.That(identifier.Schema.Value, Is.EqualTo("c"));
            Assert.That(identifier.Database.Value, Is.EqualTo("d"));
        }

        [Test]
        public void ColumnIdentifier_FourPart_no_schema()
        {
            Assert.Inconclusive("d..b.a");
        }

        [Test]
        public void ColumnIdentifierr_too_long_identifier()
        {
            Assert.Throws<ParsingFailureException>(
                () => parser.Parse<ObjectIdentifier>("[e].[d].[c].[b].[a]"));
        }
    }
}