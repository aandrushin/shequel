﻿using System;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class TopTest
    {
        private StartRuleDescriptor parser = TSql.Top;

        [Test]
        public void Top()
        {
            var result = parser.Parse<Top>("TOP 1");

            var value = (Constant.Integer)result.Expression;

            Assert.That(value.ParsedValue.Value, Is.EqualTo(1d));
            Assert.That(result.WithTies.HasValue, Is.False);
            Assert.That(result.Percent.HasValue, Is.False);
        }

        [Test]
        public void Top_with_ties()
        {
            var result = parser.Parse<Top>("TOP 2 WITH TIES");

            var value = (Constant.Integer)result.Expression;

            Assert.That(value.ParsedValue.Value, Is.EqualTo(2));
            Assert.That(result.WithTies.Value, Is.True);
            Assert.That(result.Percent.HasValue, Is.False);
        }

        [Test]
        public void Top_brackets()
        {
            var result = parser.Parse<Top>("TOP (1)");

            var sub = (Expression.Sub)result.Expression;
            var value = (Constant.Integer)sub.Inner;

            Assert.That(value.ParsedValue.Value, Is.EqualTo(1d));
            Assert.That(result.WithTies.HasValue, Is.False);
            Assert.That(result.Percent.HasValue, Is.False);
        }

        [Test]
        public void Top_percent_dec()
        {
            var result = parser.Parse<Top>("TOP 1.1 PERCENT");

            var value = (Constant.Decimal)result.Expression;

            Assert.That(value.ParsedValue.Value, Is.EqualTo(1.1d));
            Assert.That(result.WithTies.HasValue, Is.False);
            Assert.That(result.Percent.Value, Is.True);
        }

        [Test]
        public void Top_percent_int()
        {
            var result = parser.Parse<Top>("TOP 3 PERCENT");

            var value = (Constant.Integer)result.Expression;

            Assert.That(value.ParsedValue.Value, Is.EqualTo(3d));
            Assert.That(result.WithTies.HasValue, Is.False);
            Assert.That(result.Percent.Value, Is.True);
        }

        [Test]
        public void Top_percent_sub()
        {
            var result = parser.Parse<Top>("TOP (1.1) PERCENT");

            var sub = (Expression.Sub)result.Expression;
            var value = (Constant.Decimal)sub.Inner;

            Assert.That(value.ParsedValue.Value, Is.EqualTo(1.1d));
            Assert.That(result.WithTies.HasValue, Is.False);
            Assert.That(result.Percent.Value, Is.True);
        }

        [Test]
        public void Top_percent_with_ties()
        {
            var result = parser.Parse<Top>("TOP 1.1 PERCENT WITH TIES");

            var value = (Constant.Decimal)result.Expression;

            Assert.That(value.ParsedValue.Value, Is.EqualTo(1.1d));
            Assert.That(result.WithTies.Value, Is.True);
            Assert.That(result.Percent.Value, Is.True);
        }

        [Test]
        public void Top_expression()
        {
            var result = parser.Parse<Top>("TOP (1 + LEN(CURRENT_USER))");

            Assert.That(result.Expression, Is.InstanceOf<Expression.Sub>());
            Assert.That(result.WithTies.HasValue, Is.False);
            Assert.That(result.Percent.HasValue, Is.False);
        }

        [Test]
        public void Top_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Top>("TOP"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Top>("TOP (1"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Top>("TOP 1)"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Top>("TOP 1 WITH"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Top>("TOP 1.1 PERCENT WITH"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Top>("TOP 1 PERCENT WITH"));
        }
    }
}