﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class SystemFunctionTest
    {
        private readonly StartRuleDescriptor parser = TSql.SystemFunction;

        [Test]
        public void Function_System_Raw()
        {
            var result = parser.Parse<Function.System>("newid()");

            Assert.That(result.Name.Value, Is.EqualTo("newid"));
        }

        [Test]
        public void Function_System_Raw_parameters()
        {
            var result = parser.Parse<Function.System>("ABS(1)");

            Assert.That(result.Name.Value, Is.EqualTo("abs"));
            Assert.That(result.Parameters.Count(), Is.EqualTo(1));
        }

        [Test]
        public void Function_System_Parameterless()
        {
            var result = parser.Parse<Function.System>("SYSTEM_USER");

            Assert.That(result.Name.Value, Is.EqualTo("system_user"));
        }

        [Test]
        public void Function_System_Parameterless_brackets()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Function.System>("SYSTEM_USER()"));
        }

        [Test]
        public void Function_System_GlobalParameter()
        {
            var result = parser.Parse<Function.System>("@@Language");

            Assert.That(result.Name.Value, Is.EqualTo("@@language"));
        }
    }
}