﻿using System.Linq;
using System.Linq.Expressions;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class OrderByTest
    {
        private StartRuleDescriptor parser = TSql.OrderBy;

        [Test]
        public void OrderBy()
        {
            var result = parser.Parse<OrderBy>("order by a collate b desc, c desc, d asc, e");

            Assert.That(result.Items.Count(), Is.EqualTo(4));

            var firstMember = result.Items.ElementAt(0);

            Assert.That(firstMember.Expression, Is.InstanceOf<ColumnIdentifier>());
            Assert.That(firstMember.Collation.Value.Name.Value, Is.EqualTo("b"));
            Assert.That(firstMember.Sort.Value, Is.EqualTo(SortMode.Desc));

            var secondMember = result.Items.ElementAt(1);

            Assert.That(secondMember.Sort.Value, Is.EqualTo(SortMode.Desc));

            var thirdMember = result.Items.ElementAt(2);

            Assert.That(thirdMember.Sort.Value, Is.EqualTo(SortMode.Asc));

            var fourthMember = result.Items.ElementAt(3);

            Assert.That(fourthMember.Sort.HasValue, Is.False);
        }

        [Test]
        public void OrderBy_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<OrderBy>("order by"));
        }

        [Test]
        public void OrderBy_Fetch_parameter()
        {
            var result = parser.Parse<OrderBy>("order by a offset 1 row fetch first @b rows only");

            var fetch = result.Fetch.Value;

            Assert.That(fetch.Mode.Value, Is.EqualTo(FetchMode.First));
            Assert.That(fetch.RowsCount, Is.InstanceOf<Parameter>());
        }

        [Test]
        public void OrderBy_Fetch_constant()
        {
            var result = parser.Parse<OrderBy>("order by a offset 1 row fetch next 1 rows only");

            var fetch = result.Fetch.Value;

            Assert.That(fetch.Mode.Value, Is.EqualTo(FetchMode.Next));
            Assert.That(fetch.RowsCount, Is.InstanceOf<Constant.Integer>());
        }

        [Test]
        public void OrderBy_Fetch_expression()
        {
            var result = parser.Parse<OrderBy>("order by a offset 1 row fetch next 1 + @a rows only");

            var fetch = result.Fetch.Value;

            Assert.That(fetch.RowsCount, Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void OrderBy_Offset()
        {
            var result = parser.Parse<OrderBy>("order by a offset 1 rows");

            var offset = result.Offset.Value;

            Assert.That(offset.RowsCount, Is.InstanceOf<Constant.Integer>());
        }

        [Test]
        public void OrderBy_Offset_expression()
        {
            var result = parser.Parse<OrderBy>("order by a offset 1 + 1 rows");

            var offset = result.Offset.Value;

            Assert.That(offset.RowsCount, Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void OrderBy_Offset_Fetch()
        {
            var result = parser.Parse<OrderBy>("order by a offset 100 rows fetch next 15 rows only");

            Assert.That(result.Offset.HasValue);
            Assert.That(result.Fetch.HasValue);
        }

        [Test]
        public void OrderBy_expression()
        {
            var result = parser.Parse<OrderBy>("order by a + @a");

            Assert.That(result.Items.ElementAt(0).Expression, Is.InstanceOf<Expression.Binary>());
        }
    }
}