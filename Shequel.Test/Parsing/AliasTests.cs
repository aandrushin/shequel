﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class AliasTest
    {
        private SimpleRuleDescriptor parser = TSql.Alias;

        [Test]
        public void Alias()
        {
            Assert.That(parser.Parse<Alias>("as a").Name.Value, Is.EqualTo("a"));
            Assert.That(parser.Parse<Alias>("a").Name.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Alias_string()
        {
            Assert.That(parser.Parse<Alias>("as 'a'").Name.Value, Is.EqualTo("'a'"));
            Assert.That(parser.Parse<Alias>("as N'a'").Name.Value, Is.EqualTo("N'a'"));
            Assert.That(parser.Parse<Alias>("'a'").Name.Value, Is.EqualTo("'a'"));
            Assert.That(parser.Parse<Alias>("N'a'").Name.Value, Is.EqualTo("N'a'"));
        }

        [Test]
        public void Alias_brackets()
        {
            Assert.That(parser.Parse<Alias>("as [a]").Name.Value, Is.EqualTo("[a]"));
            Assert.That(parser.Parse<Alias>("[a]").Name.Value, Is.EqualTo("[a]"));
        }
    }
}