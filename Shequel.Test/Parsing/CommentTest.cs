﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class CommentTest
    {
        private SimpleRuleDescriptor parser = TSql.Select;

        [Test]
        public void Comment_multiline()
        {
            var result = parser.Parse<Select>("select a, /* abc */ 1");

            Assert.That(result.Projection.Count(), Is.EqualTo(2));
        }

        [Test]
        public void Comment_singleline()
        {
            var result = parser.Parse<Select>("select 1 -- ,abc");

            Assert.That(result.Projection.Count(), Is.EqualTo(1));
        }
    }
}
