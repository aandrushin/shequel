﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class ValuesTest
    {
        private SimpleRuleDescriptor parser = TSql.Values;

        [Test]
        public void Values()
        {
            var result = parser.Parse<Values>("values (1, 2 + 1)");

            Assert.That(result.Rows.Count(), Is.EqualTo(1));

            var row = result.Rows.First();

            Assert.That(row.Values.Count(), Is.EqualTo(2));
            Assert.That(row.Values.ElementAt(0), Is.InstanceOf<Constant.Integer>());
            Assert.That(row.Values.ElementAt(1), Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void Values_rows()
        {
            var result = parser.Parse<Values>("values (1, 2), (3, 4)");

            Assert.That(result.Rows.Count(), Is.EqualTo(2));
        }

        [Test]
        public void Values_default()
        {
            var result = parser.Parse<Values>("values (default, 1)");

            var row = result.Rows.First();

            Assert.That(row.Values.First(), Is.InstanceOf<DefaultExpression>());
        }
    }
}