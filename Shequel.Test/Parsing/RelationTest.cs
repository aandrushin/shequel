﻿using System.Collections.Generic;
using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class RelationTest
    {
        private StartRuleDescriptor parser = TSql.From_Relation;

        #region Function
        [Test]
        public void Relation_Function()
        {
            var result = parser.Parse<RelationDeclaration.FunctionRelation>("dbo.a(1, 2)");

            Assert.That(result.Alias.HasValue, Is.False);
            Assert.That(result.FunctionName.Object.Value, Is.EqualTo("a"));
            Assert.That(result.Parameters.Count(), Is.EqualTo(2));
        }

        [Test]
        public void Relation_Function_alias()
        {
            var result = parser.Parse<RelationDeclaration.FunctionRelation>("dbo.a(1, 2) as a");

            Assert.That(result.Alias.Value.Name.Value, Is.EqualTo("a"));

            Assert.That(result.FunctionName.Object.Value, Is.EqualTo("a"));
            Assert.That(result.Parameters.Count(), Is.EqualTo(2));
        }
        #endregion

        #region TempTable
        [Test]
        public void Relation_TempTable()
        {
            var result = parser.Parse<RelationDeclaration.TempTableRelation>("#a");

            Assert.That(result.Alias.HasValue, Is.False);
            Assert.That(result.Table.Name.Value, Is.EqualTo("#a"));
        }

        [Test]
        public void Relation_TempTable_alias()
        {
            var result = parser.Parse<RelationDeclaration.TempTableRelation>("#a b");

            Assert.That(result.Alias.Value.Name.Value, Is.EqualTo("b"));
            Assert.That(result.Table.Name.Value, Is.EqualTo("#a"));
        }
        #endregion
        
        #region Parameter
        [Test]
        public void Relation_Parameter()
        {
            var result = parser.Parse<RelationDeclaration.ParameterRelation>("@a");

            Assert.That(result.Alias.HasValue, Is.False);
            Assert.That(result.Parameter.Name.Value, Is.EqualTo("@a"));
        }

        [Test]
        public void Relation_Parameter_alias()
        {
            var result = parser.Parse<RelationDeclaration.ParameterRelation>("@a b");

            Assert.That(result.Alias.Value.Name.Value, Is.EqualTo("b"));
            Assert.That(result.Parameter.Name.Value, Is.EqualTo("@a"));
        }
        #endregion

        #region Joins
        [Test]
        public void Relation_Join_Inner()
        {
            var result = parser.Parse<Join.Inner>("@a a inner join @b b on a.c = b.d");

            Assert.That(result.Left, Is.InstanceOf<RelationDeclaration.ParameterRelation>());
            Assert.That(result.Right, Is.InstanceOf<RelationDeclaration.ParameterRelation>());
            Assert.That(result.On, Is.InstanceOf<Predicate.Comparison>());
            Assert.That(result.Hint.HasValue, Is.False);
        }

        [Test]
        public void Relation_Join_Inner_hint()
        {
            var hints = new Dictionary<string, JoinHint>
            {
                {"LOOP", JoinHint.Loop},
                {"HASH", JoinHint.Hash},
                {"MERGE", JoinHint.Merge},
                {"REMOTE", JoinHint.Remote},
            };

            foreach (var hint in hints)
            {
                var result = parser.Parse<Join.Inner>(
                    $"@a a inner {hint.Key} join @b b on a.c = b.d");

                Assert.That(result.Hint.Value, Is.EqualTo(hint.Value));
            }
        }

        [Test]
        public void Relation_Join_Inner_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("inner join @b b on a.id = b.id"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("@a a inner join on a.id = b.id"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("@a a inner join @b b"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("@a a inner join @b b on "));
        }

        [Test]
        public void Relation_Join_Outer()
        {
            var joins = new Dictionary<string, OuterJoinKind>
            {
                {"LEFT", OuterJoinKind.Left},
                {"RIGHT", OuterJoinKind.Right},
                {"FULL", OuterJoinKind.Full},
                {"LEFT OUTER", OuterJoinKind.Left},
                {"RIGHT OUTER", OuterJoinKind.Right},
                {"FULL OUTER", OuterJoinKind.Full},
            };

            foreach (var join in joins)
            {
                var result = parser.Parse<Join.Outer>($"@a a {join.Key} join @b b on a.c = b.d");

                Assert.That(result.Kind.Value, Is.EqualTo(join.Value));
                Assert.That(result.Left, Is.InstanceOf<RelationDeclaration.ParameterRelation>());
                Assert.That(result.Right, Is.InstanceOf<RelationDeclaration.ParameterRelation>());
                Assert.That(result.On, Is.InstanceOf<Predicate.Comparison>());
            }
        }

        [Test]
        public void Relation_Join_Outer_hints()
        {
            var hints = new Dictionary<string, JoinHint>
            {
                {"LOOP", JoinHint.Loop},
                {"HASH", JoinHint.Hash},
                {"MERGE", JoinHint.Merge},
                {"REMOTE", JoinHint.Remote},
            };

            foreach (var hint in hints)
            {
                var result = parser.Parse<Join.Outer>($"@a a left {hint.Key} join @b b on a.c = b.d");

                Assert.That(result.Hint.Value, Is.EqualTo(hint.Value));
            }
        }

        [Test]
        public void Relation_Join_Outer_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("left join @b b on a.id = b.id"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("@a a left join on a.id = b.id"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("@a a left join @b b"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("@a a left join @b b on "));
        }

        [Test]
        public void Relation_Join_Cross()
        {
            var result = parser.Parse<Join.Cross>("@a a cross join @b b");

            Assert.That(result.Left, Is.InstanceOf<RelationDeclaration.ParameterRelation>());
            Assert.That(result.Right, Is.InstanceOf<RelationDeclaration.ParameterRelation>());
        }

        [Test]
        public void Relation_Join_Cross_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("cross join @b b"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("@a a cross join"));
        }

        [Test]
        public void Relation_Apply()
        {
            var apply = new Dictionary<string, ApplyKind>
            {
                {"CROSS", ApplyKind.Cross},
                {"OUTER", ApplyKind.Outer}
            };

            foreach (var name in apply)
            {
                var result = parser.Parse<Join.Apply>(
                    $"@a a {name.Key} apply @b b");

                Assert.That(result.Kind.Value, Is.EqualTo(name.Value));
            }
        }

        [Test]
        public void Relation_Apply_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("cross apply @b b"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("@a a cross apply"));
        }

        [Test]
        public void Relation_Sub()
        {
            var result = parser.Parse<Join.Inner>(
                "@a a inner join (@b b inner join @c c on b.id = c.id) on c.id = a.id");

            Assert.That(result.Right, Is.InstanceOf<Join.Sub>());

            var subjoin = (Join.Sub)result.Right;

            Assert.That(subjoin.Inner, Is.InstanceOf<Join.Inner>());
        }

        [Test]
        public void Relation_Sub_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("@a a inner join () on c.id = a.id"));
        }

        [Test]
        public void Relation_Outer_parameter()
        {
            var result = parser.Parse<Join.Outer>("@a a right outer join c on c.id = a.id");

            Assert.That(result.Left, Is.InstanceOf<RelationDeclaration.ParameterRelation>());
        }
        #endregion

        #region Pivot / Unpivot
        [Test]
        public void Relation_Pivot()
        {
            var result = parser.Parse<RelationDeclaration.PivotRelation>(
                "tbl t PIVOT (COUNT (id) FOR id IN ([a], [b], [c], [d], [e])) AS pvt");

            Assert.That(result.Relation, Is.InstanceOf<RelationDeclaration.ObjectRelation>());

            var source = (RelationDeclaration.ObjectRelation)result.Relation;

            Assert.That(source.Alias.Value.Name.Value, Is.EqualTo("t"));
            Assert.That(result.Function.Name.Value, Is.EqualTo("count"));
            Assert.That(result.Column.Name.Value, Is.EqualTo("id"));
            Assert.That(result.PivotColumns.Count(), Is.EqualTo(5));
            Assert.That(result.Alias.Name.Value, Is.EqualTo("pvt"));
        }
        
        [Test]
        public void Relation_Pivot_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("dbo.tbl t PIVOT () AS pvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("dbo.tbl t PIVOT (COUNT () FOR id IN ([a], [b], [c], [d], [e])) AS pvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("dbo.tbl t PIVOT (COUNT (id) FOR id IN ()) AS pvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("dbo.tbl t PIVOT (COUNT (id) FOR IN ([a], [b], [c], [d], [e])) AS pvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("dbo.tbl t PIVOT (FOR id IN ([a], [b], [c], [d], [e])) AS pvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("PIVOT (COUNT (id) FOR id IN ([a], [b], [c], [d], [e])) AS pvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("dbo.tbl PIVOT (COUNT (id) FOR id IN ([a], [b], [c], [d], [e]))"));
        }

        [Test]
        public void Relation_Unpivot()
        {
            var result = parser.Parse<RelationDeclaration.UnpivotRelation>(
                @"pvt.p UNPIVOT (ValueColumn FOR pc IN (a, b, c, d, e)) AS unpvt");

            Assert.That(result.Relation, Is.InstanceOf<RelationDeclaration.ObjectRelation>());

            var source = (RelationDeclaration.ObjectRelation)result.Relation;

            Assert.That(source.ObjectName, Is.InstanceOf<ObjectIdentifier>());
            Assert.That(source.Alias.HasValue, Is.False);

            Assert.That(result.ValueColumn, Is.InstanceOf<Identifier>());
            Assert.That(result.ValueColumn.Name.Value, Is.EqualTo("ValueColumn"));
            Assert.That(result.PivotColumn, Is.InstanceOf<Identifier>());
            Assert.That(result.PivotColumn.Name.Value, Is.EqualTo("pc"));

            Assert.That(result.Columns.Count(), Is.EqualTo(5));

            Assert.That(result.Alias.Name.Value, Is.EqualTo("unpvt"));
        }

        [Test]
        public void Relation_Unpivot_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("UNPIVOT (ValueColumn FOR PivotColumn IN (a, b, c, d, e)) AS unpvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("pvt.p UNPIVOT ( FOR PivotColumn IN (a, b, c, d, e)) AS unpvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("pvt.p UNPIVOT (ValueColumn FOR  IN (a, b, c, d, e)) AS unpvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("pvt.p UNPIVOT (ValueColumn FOR PivotColumn IN ()) AS unpvt"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("pvt.p UNPIVOT ()"));
        }
        #endregion
        
        #region Object

        [Test]
        public void Relation_Object()
        {
            var result = parser.Parse<RelationDeclaration.ObjectRelation>("dbo.a");

            Assert.That(result.ObjectName.Object.Value, Is.EqualTo("a"));
            Assert.That(result.ObjectName.Schema.Value, Is.EqualTo("dbo"));
            Assert.That(result.Alias.HasValue, Is.False);
        }

        [Test]
        public void Relation_Object_alias()
        {
            var result = parser.Parse<RelationDeclaration.ObjectRelation>("dbo.a b");

            Assert.That(result.ObjectName.Object.Value, Is.EqualTo("a"));
            Assert.That(result.ObjectName.Schema.Value, Is.EqualTo("dbo"));
            Assert.That(result.Alias.Value.Name.Value, Is.EqualTo("b"));
        }
        #endregion


        #region SubQuery

        [Test]
        public void Relation_SubQuery()
        {
            var result = parser.Parse<RelationDeclaration.SubQueryRelation>(
                    "(SELECT 1) AS t(itmID)");

            Assert.That(result.Alias.Name.Value, Is.EqualTo("t"));
            Assert.That(result.Alias.Columns.Count(), Is.EqualTo(1));
        }

        [Test]
        public void Relation_SubQuery_inomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Relation>("(select 1 as id)"));
        }
        
        #endregion
    }
}