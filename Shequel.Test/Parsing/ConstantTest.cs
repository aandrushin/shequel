﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class ConstantTest
    {
        private StartRuleDescriptor parser = TSql.Constant;

        [Test]
        public void Contant_Any()
        {
            Assert.That(parser.Parse<Constant>("1"), Is.InstanceOf<Constant.Integer>());
            Assert.That(parser.Parse<Constant>("1.0"), Is.InstanceOf<Constant.Decimal>());
            Assert.That(parser.Parse<Constant>("$1.0"), Is.InstanceOf<Constant.Money>());
            Assert.That(parser.Parse<Constant>("1e5"), Is.InstanceOf<Constant.Real>());
            Assert.That(parser.Parse<Constant>("0x1"), Is.InstanceOf<Constant.Binary>());
            Assert.That(parser.Parse<Constant>("N'a'"), Is.InstanceOf<Constant.String>());
            Assert.That(parser.Parse<Constant>("NULL"), Is.InstanceOf<Constant.Null>());
        }

        [Test]
        public void Contant_Integer()
        {
            Assert.That(parser.Parse<Constant.Integer>("1").ParsedValue.Value, Is.EqualTo(1));
            Assert.That(parser.Parse<Constant.Integer>("+1").ParsedValue.Value, Is.EqualTo(1));
            Assert.That(parser.Parse<Constant.Integer>("-1").ParsedValue.Value, Is.EqualTo(-1));
        }

        [Test]
        public void Contant_Decimal()
        {
            Assert.That(parser.Parse<Constant.Decimal>("1.0").ParsedValue.Value, Is.EqualTo(1.0d));
            Assert.That(parser.Parse<Constant.Decimal>("+1.0").ParsedValue.Value, Is.EqualTo(1.0d));
            Assert.That(parser.Parse<Constant.Decimal>("-1.0").ParsedValue.Value, Is.EqualTo(-1.0d));
        }

        [Test]
        public void Contant_Money()
        {
            Assert.That(parser.Parse<Constant.Money>("$1.0").ParsedValue.Value, Is.EqualTo(1.0m));
            Assert.That(parser.Parse<Constant.Money>("+$1.0").ParsedValue.Value, Is.EqualTo(1.0m));
            Assert.That(parser.Parse<Constant.Money>("-$1.0").ParsedValue.Value, Is.EqualTo(-1.0m));
        }

        [Test]
        public void Contant_Real()
        {
            Assert.That(parser.Parse<Constant.Real>("1e5").ParsedValue.Value, Is.EqualTo(1e5));
            Assert.That(parser.Parse<Constant.Real>("+1e6").ParsedValue.Value, Is.EqualTo(1e6));
            Assert.That(parser.Parse<Constant.Real>("-1e6").ParsedValue.Value, Is.EqualTo(-1e6));
            Assert.That(parser.Parse<Constant.Real>("-1e-6").ParsedValue.Value, Is.EqualTo(-1e-6));
            Assert.That(parser.Parse<Constant.Real>("1e").ParsedValue.Value, Is.EqualTo(1e0));
            Assert.That(parser.Parse<Constant.Real>("-1e").ParsedValue.Value, Is.EqualTo(-1e0));
        }

        [Test]
        public void Contant_Binary()
        {
            Assert.That(parser.Parse<Constant.Binary>("0x1").ParsedValue.Value, Is.EqualTo("0x1"));
        }

        [Test]
        public void Contant_String_UnicodeString()
        {
            var str = parser.Parse<Constant.String>("N'a'");

            Assert.That(str.ParsedValue.Value, Is.EqualTo("N'a'"));
            Assert.That(str.Unicode.Value, Is.True);
            Assert.That(str.Body.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Contant_String_RegularString()
        {
            var str = parser.Parse<Constant.String>("'a'");

            Assert.That(str.ParsedValue.Value, Is.EqualTo("'a'"));
            Assert.That(str.Unicode.Value, Is.False);
            Assert.That(str.Body.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Contant_Null()
        {
            Assert.That(parser.Parse<Constant>("NULL"), Is.InstanceOf<Constant.Null>());
            Assert.That(parser.Parse<Constant>("null"), Is.InstanceOf<Constant.Null>());
        }
    }
}
