﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class ObjectIdentifierTest
    {
        private StartRuleDescriptor parser = TSql.ObjectIdentifier;

        [Test]
        public void ObjectIdentifier_Simple()
        {
            var identifier = parser.Parse<ObjectIdentifier>("a");

            Assert.That(identifier.Object.Value, Is.EqualTo("a"));
            Assert.That(identifier.Schema.Value, Is.EqualTo(string.Empty));
            Assert.That(identifier.Database.Value, Is.EqualTo(string.Empty));
            Assert.That(identifier.Server.Value, Is.EqualTo(string.Empty));
        }

        [Test]
        public void WildcardIdentifier_TwoPart()
        {
            var identifier = parser.Parse<ObjectIdentifier>("b.a");

            Assert.That(identifier.Object.Value, Is.EqualTo("a"));
            Assert.That(identifier.Schema.Value, Is.EqualTo("b"));
            Assert.That(identifier.Database.Value, Is.EqualTo(string.Empty));
            Assert.That(identifier.Server.Value, Is.EqualTo(string.Empty));
        }

        [Test]
        public void WildcardIdentifier_ThreePart()
        {
            var identifier = parser.Parse<ObjectIdentifier>("c.b.a");

            Assert.That(identifier.Object.Value, Is.EqualTo("a"));
            Assert.That(identifier.Schema.Value, Is.EqualTo("b"));
            Assert.That(identifier.Database.Value, Is.EqualTo("c"));
            Assert.That(identifier.Server.Value, Is.EqualTo(string.Empty));
        }

        [Test]
        public void WildcardIdentifier_FourPart()
        {
            var identifier = parser.Parse<ObjectIdentifier>("d.c.b.a");

            Assert.That(identifier.Object.Value, Is.EqualTo("a"));
            Assert.That(identifier.Schema.Value, Is.EqualTo("b"));
            Assert.That(identifier.Database.Value, Is.EqualTo("c"));
            Assert.That(identifier.Server.Value, Is.EqualTo("d"));
        }

        [Test]
        public void WildcardIdentifier_FourPart_bracket()
        {
            var identifier = parser.Parse<ObjectIdentifier>("[d].[c].[b].[a]");

            Assert.That(identifier.Object.Value, Is.EqualTo("[a]"));
            Assert.That(identifier.Schema.Value, Is.EqualTo("[b]"));
            Assert.That(identifier.Database.Value, Is.EqualTo("[c]"));
            Assert.That(identifier.Server.Value, Is.EqualTo("[d]"));
        }

        [Test]
        public void WildcardIdentifier_FourPart_too_long_identifier()
        {
            Assert.Throws<ParsingFailureException>(
                () => parser.Parse<ObjectIdentifier>("[e].[d].[c].[b].[a]"));
        }

        [Test]
        public void WildcardIdentifier_keyword()
        {
            Assert.Inconclusive("joined");
        }
    }
}