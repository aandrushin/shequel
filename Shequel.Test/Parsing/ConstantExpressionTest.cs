﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class ConstantExpressionTest
    {
        private readonly ExtensibleRuleDescriptor parser = TSql.ConstantExpression;

        [Test]
        public void ConstantExpression_constants()
        {
            var expressions = parser
                .Parse<Expression>("N'1' + '1' + 1.0 + 0x1 + $1 + 1e0 + NULL")
                .Flatten()
                .ToArray();

            Assert.That(expressions.ElementAt(0), Is.InstanceOf<Constant.Null>());
            Assert.That(expressions.ElementAt(1), Is.InstanceOf<Constant.Real>());
            Assert.That(expressions.ElementAt(2), Is.InstanceOf<Constant.Money>());
            Assert.That(expressions.ElementAt(3), Is.InstanceOf<Constant.Binary>());
            Assert.That(expressions.ElementAt(4), Is.InstanceOf<Constant.Decimal>());
            Assert.That(expressions.ElementAt(5), Is.InstanceOf<Constant.String>());
            Assert.That(expressions.ElementAt(6), Is.InstanceOf<Constant.String>());
        }

        [Test]
        public void ConstantExpression_binary_operators()
        {
            var operators = parser
                .Parse<Expression>("1 + 1 - 1 & 1 | 1 * 1 / 1 % 1")
                .FlattenOperators()
                .ToArray();

            Assert.That(operators.ElementAt(0), Is.EqualTo("%"));
            Assert.That(operators.ElementAt(1), Is.EqualTo("/"));
            Assert.That(operators.ElementAt(2), Is.EqualTo("*"));
            Assert.That(operators.ElementAt(3), Is.EqualTo("|"));
            Assert.That(operators.ElementAt(4), Is.EqualTo("&"));
            Assert.That(operators.ElementAt(5), Is.EqualTo("-"));
            Assert.That(operators.ElementAt(6), Is.EqualTo("+"));
        }

        [Test]
        public void ConstantExpression_unary_operators()
        {
            var negate1 = parser.Parse<Expression.Unary>("~1");
            var negate2 = parser.Parse<Expression.Unary>("- -1");

            Assert.That(negate1.Operator.Value, Is.EqualTo("~"));
            Assert.That(negate2.Operator.Value, Is.EqualTo("-"));
        }

        [Test]
        public void ConstantExpression_Sub()
        {
            var expression = parser.Parse<Expression.Binary>("1 + (1 + 2)");

            Assert.That(expression.Right, Is.InstanceOf<Expression.Sub>());

            var sub = (Expression.Sub)expression.Right;

            Assert.That(sub.Inner, Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void ConstantExpression_system_functions()
        {
            var expression = parser.Parse<Expression.Binary>("abs(1) + 'a'");

            Assert.That(expression.Right, Is.InstanceOf<Constant.String>());
            Assert.That(expression.Left, Is.InstanceOf<Function.System>());
        }

        [Test]
        public void ConstantExpression_global_parameters()
        {
            var expression = parser.Parse<Expression.Binary>("@@language + 'a'");

            Assert.That(expression.Right, Is.InstanceOf<Constant.String>());
            Assert.That(expression.Left, Is.InstanceOf<Function.System>());
        }
    }
}