﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class ParameterTest
    {
        private StartRuleDescriptor parser = TSql.Parameter;

        [Test]
        public void Parameter()
        {
            var result = parser.Parse<Parameter>("@a");

            Assert.That(result.Name.Value, Is.EqualTo("@a"));
        }

        [Test]
        public void Parameter_special_name()
        {
            Assert.Inconclusive();
            Assert.That(parser.Parse<Parameter>("@1").Name.Value, Is.EqualTo("@1"));
            Assert.That(parser.Parse<Parameter>("@1a").Name.Value, Is.EqualTo("@1a"));
            Assert.That(parser.Parse<Parameter>("@@1a").Name.Value, Is.EqualTo("@@1a"));
            Assert.That(parser.Parse<Parameter>("@$a").Name.Value, Is.EqualTo("@$a"));
            Assert.That(parser.Parse<Parameter>("@#a").Name.Value, Is.EqualTo("@#a"));
            Assert.That(parser.Parse<Parameter>("@_a").Name.Value, Is.EqualTo("@_a"));
            Assert.That(parser.Parse<Parameter>("@#").Name.Value, Is.EqualTo("@#"));
            Assert.That(parser.Parse<Parameter>("@@").Name.Value, Is.EqualTo("@@"));
            Assert.That(parser.Parse<Parameter>("@_").Name.Value, Is.EqualTo("@_"));
            Assert.That(parser.Parse<Parameter>("@$").Name.Value, Is.EqualTo("@$"));
            Assert.That(parser.Parse<Parameter>("@").Name.Value, Is.EqualTo("@"));
            Assert.That(parser.Parse<Parameter>("@@").Name.Value, Is.EqualTo("@@"));
            Assert.That(parser.Parse<Parameter>("@join").Name.Value, Is.EqualTo("@join"));
            Assert.That(parser.Parse<Parameter>("@@join").Name.Value, Is.EqualTo("@@join"));
        }
        
        [Test]
        public void Parameter_missing_at_symbol()
        {
            Assert.Throws<ParsingFailureException>(
                () => parser.Parse<Parameter>("abc"));
        }
    }
}