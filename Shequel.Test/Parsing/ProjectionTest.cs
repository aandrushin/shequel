﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class ProjectionTest
    {
        private StartRuleDescriptor parser = TSql.Projection;

        [Test]
        public void Projection_Simple()
        {
            var result = parser.Parse<Projection.Simple>("1");

            Assert.That(result.Alias.HasValue, Is.False);
            Assert.That(result.Expression, Is.InstanceOf<Constant>());
        }

        [Test]
        public void Projection_Simple_alias()
        {
            var result = parser.Parse<Projection.Simple>("1 a");

            Assert.That(result.Alias.Value.Name.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Projection_Simple_alias_set()
        {
            var result = parser.Parse<Projection.Simple>("a = 1");

            Assert.That(result.Alias.Value.Name.Value, Is.EqualTo("a"));
            Assert.That(result.Expression, Is.InstanceOf<Constant>());
        }

        [Test]
        public void Projection_Set()
        {
            var result = parser.Parse<Projection.Set>("@a = 1");

            Assert.That(result.Parameter.Name.Value, Is.EqualTo("@a"));
            Assert.That(result.Expression, Is.InstanceOf<Constant>());
        }

        [Test]
        public void Projection_Simple_wildcard()
        {
            var result = parser.Parse<Projection.Simple>("a.*");

            Assert.That(result.Expression, Is.InstanceOf<ColumnIdentifier>());
        }

        [Test]
        public void Projection_Simple_wildcard_1()
        {
            var result = parser.Parse<Projection.Simple>("*");

            Assert.That(result.Expression, Is.InstanceOf<ColumnIdentifier>());
        }

        [Test]
        public void Projection_Simple_wildcard_alias()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Projection>("* as a"));
        }
        
        [Test]
        public void Projection_PseudoColumn()
        {
            var result = parser.Parse<Projection.Simple>("$identity");

            var reference = (ColumnIdentifier)result.Expression;

            Assert.That(reference.Column.Value, Is.EqualTo("$identity"));
            Assert.That(reference.Database.HasValue, Is.False);
            Assert.That(reference.Schema.HasValue, Is.False);
            Assert.That(reference.Table.HasValue, Is.False);
        }

        [Test]
        public void Projection_PseudoColumn_alias()
        {
            var result = parser.Parse<Projection.Simple>("$identity as a");

            var reference = (ColumnIdentifier)result.Expression;

            Assert.That(reference.Column.Value, Is.EqualTo("$identity"));
            Assert.That(reference.Database.HasValue, Is.False);
            Assert.That(reference.Schema.HasValue, Is.False);
            Assert.That(reference.Table.HasValue, Is.False);

            Assert.That(result.Alias.HasValue);
            Assert.That(result.Alias.Value.Name.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Projection_PseudoColumn_1()
        {
            var result = parser.Parse<Projection.Simple>("$rowguid");

            var reference = (ColumnIdentifier)result.Expression;

            Assert.That(reference.Column.Value, Is.EqualTo("$rowguid"));
            Assert.That(reference.Database.HasValue, Is.False);
            Assert.That(reference.Schema.HasValue, Is.False);
            Assert.That(reference.Table.HasValue, Is.False);
        }

        [Test]
        public void Projection_PseudoColumn_alias_1()
        {
            var result = parser.Parse<Projection.Simple>("$rowguid as a");

            var reference = (ColumnIdentifier)result.Expression;

            Assert.That(reference.Column.Value, Is.EqualTo("$rowguid"));
            Assert.That(reference.Database.HasValue, Is.False);
            Assert.That(reference.Schema.HasValue, Is.False);
            Assert.That(reference.Table.HasValue, Is.False);

            Assert.That(result.Alias.HasValue);
            Assert.That(result.Alias.Value.Name.Value, Is.EqualTo("a"));
        }
    }
}