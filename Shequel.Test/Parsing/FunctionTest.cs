﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class UserDefinedFunctionTest
    {
        private readonly StartRuleDescriptor parser = TSql.UserDefinedFunction;

        [Test]
        public void Function_UserDefined_no_parameters()
        {
            var result = parser.Parse<Function.UserDefined>("a()");

            Assert.That(result.FullName.Object.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Function_UserDefined_parameters()
        {
            var result = parser.Parse<Function.UserDefined>("a(a, 1, $100)");

            Assert.That(result.FullName.Object.Value, Is.EqualTo("a"));
            Assert.That(result.Parameters.Count(), Is.EqualTo(3));
            Assert.That(result.Parameters.ElementAt(0), Is.InstanceOf<ColumnIdentifier>());
            Assert.That(result.Parameters.ElementAt(1), Is.InstanceOf<Constant.Integer>());
            Assert.That(result.Parameters.ElementAt(2), Is.InstanceOf<Constant.Money>());
        }

        [Test]
        public void Function_UserDefined_expression_parameter()
        {
            var result = parser.Parse<Function.UserDefined>("a(1 + 1)");

            Assert.That(result.FullName.Object.Value, Is.EqualTo("a"));
            Assert.That(result.Parameters.Count(), Is.EqualTo(1));
            Assert.That(result.Parameters.ElementAt(0), Is.InstanceOf<Expression.Binary>());
        }

        [Test]
        public void Function_UserDefined_full_name_1()
        {
            var result = parser.Parse<Function.UserDefined>("dbo.a(1 + 1)");

            Assert.That(result.FullName.Schema.Value, Is.EqualTo("dbo"));
            Assert.That(result.FullName.Object.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Function_UserDefined_full_name_2()
        {
            var result = parser.Parse<Function.UserDefined>("test.dbo.a(1 + 1)");

            Assert.That(result.FullName.Database.Value, Is.EqualTo("test"));
            Assert.That(result.FullName.Schema.Value, Is.EqualTo("dbo"));
            Assert.That(result.FullName.Object.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Function_UserDefined_full_name_3()
        {
            var result = parser.Parse<Function.UserDefined>("serv1.test.dbo.a(1 + 1)");

            Assert.That(result.FullName.Server.Value, Is.EqualTo("serv1"));
            Assert.That(result.FullName.Database.Value, Is.EqualTo("test"));
            Assert.That(result.FullName.Schema.Value, Is.EqualTo("dbo"));
            Assert.That(result.FullName.Object.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Function_UserDefined_no_brackets()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Function.UserDefined>("a"));
        }

        [Test]
        public void Function_UserDefined_too_long_name()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Function.UserDefined>("wtf.serv1.test.dbo.a()"));
        }
    }
}