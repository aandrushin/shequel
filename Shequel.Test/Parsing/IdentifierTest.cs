﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class IdentifierTest
    {
        private ExtensibleRuleDescriptor parser = TSql.Identifier;

        [Test]
        public void Identifier_Bracket()
        {
            Assert.That(parser.Parse<Identifier>("[a]").Name.Value, Is.EqualTo("[a]"));
        }

        [Test]
        public void Identifier_keyword()
        {
            Assert.Inconclusive("joined");
        }

        [Test]
        public void Identifier_Bracket_escaped()
        {
            Assert.That(parser.Parse<Identifier>("[a]]]").Name.Value, Is.EqualTo("[a]]]"));
        }

        [Test]
        public void Identifier_Quoted_escaped()
        {
            Assert.That(parser.Parse<Identifier>("\"a\"\"\"").Name.Value, Is.EqualTo("\"a\"\"\""));
        }

        [Test]
        public void Identifier_Simple()
        {
            Assert.That(parser.Parse<Identifier>("a").Name.Value, Is.EqualTo("a"));
        }

        [Test]
        public void Identifier_special()
        {
            Assert.Inconclusive();
            Assert.That(parser.Parse<Identifier>("_a").Name.Value, Is.EqualTo("_a"));
            Assert.That(parser.Parse<Identifier>("a@").Name.Value, Is.EqualTo("a@"));
            Assert.That(parser.Parse<Identifier>("a$").Name.Value, Is.EqualTo("a$"));
            Assert.That(parser.Parse<Identifier>("a#").Name.Value, Is.EqualTo("a#"));
            Assert.That(parser.Parse<Identifier>("a_").Name.Value, Is.EqualTo("a_"));
        }

        [Test]
        public void Identifier_empty()
        {
            Assert.That(parser.Parse<Identifier>("[ ]").Name.Value, Is.EqualTo("[ ]"));
            Assert.That(parser.Parse<Identifier>("\" \"").Name.Value, Is.EqualTo("\" \""));
        }
    }
}
