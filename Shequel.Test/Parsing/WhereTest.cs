﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class WhereTest
    {
        private StartRuleDescriptor parser = TSql.Where;

        [Test]
        public void Where()
        {
            var result = parser.Parse<Predicate>("WHERE @a > 2");

            Assert.That(result, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void Where_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<Predicate>("WHERE"));
        }
    }
}