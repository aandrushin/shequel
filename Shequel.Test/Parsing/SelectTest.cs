﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class SelectTest
    {
        private StartRuleDescriptor parser = TSql.Select;

        [Test]
        public void Select()
        {
            var result = parser.Parse<Select>("select a");

            Assert.That(result.Projection.Count(), Is.EqualTo(1));
        }

        [Test]
        public void Select_with_constant()
        {
            var result = parser.Parse<Select>("select 1");

            Assert.That(result.Projection.Count(), Is.EqualTo(1));
        }

        [Test]
        public void Select_all()
        {
            var result = parser.Parse<Select>("select all a");

            Assert.That(result.Distinct.HasValue, Is.True);
            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.All));
        }

        [Test]
        public void Select_distinct()
        {
            var result = parser.Parse<Select>("select distinct a");

            Assert.That(result.Distinct.HasValue, Is.True);
            Assert.That(result.Distinct.Value, Is.EqualTo(DistinctMode.Distinct));
        }

        [Test]
        public void Select_top()
        {
            var result = parser.Parse<Select>("select top 1 a");

            Assert.That(result.Top.HasValue);
            Assert.That(result.Top.Value.Expression, Is.InstanceOf<Constant.Integer>());
        }

        [Test]
        public void Select_from()
        {
            var result = parser.Parse<Select>("select a from b");

            Assert.That(result.From.Any);
            Assert.That(result.From.First(), Is.InstanceOf<RelationDeclaration.ObjectRelation>());
        }

        [Test]
        public void Select_wildcard()
        {
            var result = parser.Parse<Select>("select * from b");

            var projection = (Projection.Simple) result.Projection.First();

            Assert.That(projection.Expression, Is.InstanceOf<ColumnIdentifier>());
        }

        [Test]
        public void Select_where()
        {
            var result = parser.Parse<Select>("select a from b where b.a < 1");

            Assert.That(result.Where.HasValue);
            Assert.That(result.Where.Value, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void Select_group_by()
        {
            var result = parser.Parse<Select>("select a from b group by c");

            Assert.That(result.GroupBy.HasValue);
            Assert.That(result.GroupBy.Value.Items.Count(), Is.EqualTo(1));
        }

        [Test]
        public void Select_having()
        {
            var result = parser.Parse<Select>("select a from b group by c having b.a < 2");

            Assert.That(result.Having.HasValue);
            Assert.That(result.Having.Value, Is.InstanceOf<Predicate.Comparison>());
        }

        [Test]
        public void Select_order_by()
        {
            var result = parser.Parse<Select>("select a from b order by a");

            Assert.That(result.OrderBy.HasValue);
            Assert.That(result.OrderBy.Value.Items.Count(), Is.EqualTo(1));
        }

        [Test]
        public void Select_incomplete()
        {
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("select"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("select top"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("select from a"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("select b from"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("select b where"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("select b order by"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("select b group by"));
            Assert.Throws<ParsingFailureException>(() => parser.Parse<SubQuery>("select b having"));
        }
    }
}