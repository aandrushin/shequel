﻿using System.Collections.Generic;
using Nitra;
using Nitra.ProjectSystem;
using NUnit.Framework;

namespace Shequel.Test.Parsing
{
    [TestFixture]
    public class ContainsConditionTest
    {
        [Test]
        public void ContainsCondition_Word()
        {
            var result = TSql.ContainsCondition.ParseTree("abc");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty); 
        }

        [Test]
        public void ContainsCondition_Phrase()
        {
            var result = TSql.ContainsCondition.ParseTree("\"abc cba\"");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_PrefixWord()
        {
            var result = TSql.ContainsCondition.ParseTree("\"abc*\"");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_PrefixPhrase()
        {
            var result = TSql.ContainsCondition.ParseTree("\"abc cba*\"");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_FormsOf_inflectional()
        {
            var result = TSql.ContainsCondition.ParseTree("formsof (inflectional, abc, cba, def)");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_FormsOf_thesaurus()
        {
            var result = TSql.ContainsCondition.ParseTree("formsof (thesaurus, abc)");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_GenericProximity()
        {
            var result = TSql.ContainsCondition.ParseTree("abc near cba ~ cde");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_CustomProximity()
        {
            var result = TSql.ContainsCondition.ParseTree("near (abc, \"cba\")");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_CustomProximity_distance()
        {
            var result = TSql.ContainsCondition.ParseTree("near ((abc, \"cba\"), 1)");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_CustomProximity_distance_and_order()
        {
            var result = TSql.ContainsCondition.ParseTree("near ((abc, \"cba\"), 1, true)");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_Weighted()
        {
            var result = TSql.ContainsCondition.ParseTree("isabout ( abc, \"acb*\", formsof (thesaurus, abc), near (abc))");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_Weighted_weight()
        {
            var result = TSql.ContainsCondition.ParseTree("isabout ( abc weight (.0), \"acb*\" weight (1.))");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }

        [Test]
        public void ContainsCondition_Expression()
        {
            var result = TSql.ContainsCondition.ParseTree("(near (abc)) and (abc) & (\"abc*\") &! (formsof (thesaurus, abc)) or (isabout ( abc weight (.0))) | (abc near bca)");

            Assert.That(result.CompilerMessages.AsEnumerable(), Is.Empty);
        }
    }

    public static class CompilerMessagesEx
    {
        public static IEnumerable<CompilerMessage> AsEnumerable(this ICompilerMessages messages)
        {
            return (CompilerMessageList) messages;
        }
    }
}
