﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class ColumnIdentifierBindingTest
    {
        private SimpleRuleDescriptor parser = TSql.ColumnIdentifier;

        [Test]
        public void ColumnIdentifier_ObjectQualifiedName()
        {
            Assert.That(parser.Eval<ColumnIdentifier>("d.c.b.a").ObjectQualifiedName, Is.EqualTo("d.c.b"));
            Assert.That(parser.Eval<ColumnIdentifier>("c.b.a").ObjectQualifiedName, Is.EqualTo("c.b"));
            Assert.That(parser.Eval<ColumnIdentifier>("b.a").ObjectQualifiedName, Is.EqualTo("b"));
            Assert.That(parser.Eval<ColumnIdentifier>("a").ObjectQualifiedName, Is.EqualTo(""));
        }

        [Test]
        public void ColumnIdentifier_ObjectQualifiedName_no_schema()
        {
            Assert.Inconclusive("d..b.a");
        }

        [Test]
        public void ColumnIdentifier_QualifiedName()
        {
            Assert.That(parser.Eval<ColumnIdentifier>("d.c.b.a").QualifiedName, Is.EqualTo("d.c.b.a"));
            Assert.That(parser.Eval<ColumnIdentifier>("c.b.a").QualifiedName, Is.EqualTo("c.b.a"));
            Assert.That(parser.Eval<ColumnIdentifier>("b.a").QualifiedName, Is.EqualTo("b.a"));
            Assert.That(parser.Eval<ColumnIdentifier>("a").QualifiedName, Is.EqualTo("a"));
        }

        [Test]
        public void ColumnIdentifier_QualifiedName_no_schema()
        {
            Assert.Inconclusive("d..b.a");
        }

        [Test]
        public void ColumnIdentifier_RelationReference()
        {
            Assert.That(parser.Eval<ColumnIdentifier>("d.c.b.a").RelationReference.Text, Is.EqualTo("[d].[c].[b]"));
            Assert.That(parser.Eval<ColumnIdentifier>("c.b.a").RelationReference.Text, Is.EqualTo("[c].[b]"));
            Assert.That(parser.Eval<ColumnIdentifier>("b.a").RelationReference.Text, Is.EqualTo("[b]"));
            Assert.That(parser.Eval<ColumnIdentifier>("a").RelationReference.Text, Is.EqualTo(""));
        }

        [Test]
        public void ColumnIdentifier_ColumnReference()
        {
            Assert.That(parser.Eval<ColumnIdentifier>("d.c.b.a").ColumnReference.Text, Is.EqualTo("[d].[c].[b].[a]"));
            Assert.That(parser.Eval<ColumnIdentifier>("c.b.a").ColumnReference.Text, Is.EqualTo("[c].[b].[a]"));
            Assert.That(parser.Eval<ColumnIdentifier>("b.a").ColumnReference.Text, Is.EqualTo("[b].[a]"));
            Assert.That(parser.Eval<ColumnIdentifier>("a").ColumnReference.Text, Is.EqualTo("[a]"));
        }

        [Test]
        public void ColumnIdentifier_ColumnReference_no_schema()
        {
            Assert.Inconclusive("d..b.a");
        }
    }
}