﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class ObjectIdentifierBindingTest
    {
        private SimpleRuleDescriptor parser = TSql.ObjectIdentifier;

        [Test]
        public void ObjectIdentifier_Binding()
        {
            var result = parser.Eval<ObjectIdentifier>("d.c.b.a");

            Assert.That(result.Bindings, Is.EquivalentTo(new[]
            {
                "[a]", "[b].[a]", "[c].[b].[a]", "[d].[c].[b].[a]"
            }));
        }

        [Test]
        public void ObjectIdentifier_QualifiedName()
        {
            var result = parser.Eval<ObjectIdentifier>("d.c.b.a");

            Assert.That(result.QualifiedName, Is.EqualTo("d.c.b.a"));
        }

        [Test]
        public void ObjectIdentifier_Reference()
        {
            Assert.That(parser.Eval<ObjectIdentifier>("d.c.b.a").Reference.Text, Is.EqualTo("[d].[c].[b].[a]"));
            Assert.That(parser.Eval<ObjectIdentifier>("c.b.a").Reference.Text, Is.EqualTo("[c].[b].[a]"));
            Assert.That(parser.Eval<ObjectIdentifier>("b.a").Reference.Text, Is.EqualTo("[b].[a]"));
            Assert.That(parser.Eval<ObjectIdentifier>("a").Reference.Text, Is.EqualTo("[a]"));
        }
    }
}
