﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class ValuesRelationBindingTest
    {
        private ExtensibleRuleDescriptor parser = TSql.From_Relation;

        [Test]
        public void ValuesRelationSymbol_Bindings()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.ValuesRelation>("(values (1)) a(id)").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[a]"
            }));
        }

        [Test]
        public void ValuesRelationSymbol_Columns_alias()
        {
            var result = parser.EvalSymbolColumns<RelationDeclaration.ValuesRelation>("(values (1, 2, 3)) a(id1, id2, id3)");
            
            Assert.That(result.SelectMany(c => c.Bindings), Is.EquivalentTo(new[]
            {
                "[id1]", "[id2]", "[id3]", "[a].[id1]", "[a].[id2]", "[a].[id3]"
            }));
        }
    }
}