﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class UnpivotRelationBindingTest
    {
        private ExtensibleRuleDescriptor parser = TSql.From_Relation;
        
        [Test]
        public void PivotRelationSymbol_Bindings_alias()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.UnpivotRelation>("(select 1) ids UNPIVOT (vc FOR pc IN (a)) AS unpvt").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[unpvt]"
            }));
        }

        [Test]
        public void PivotRelationSymbol_Column()
        {
            var result = parser.EvalSymbolColumns<RelationDeclaration.UnpivotRelation>("(select 1 id, 2 id2) a UNPIVOT (vc FOR pc IN (id)) AS unpvt").ToArray();

            Assert.That(result.SelectMany(c => c.Bindings), Is.EquivalentTo(new[]
            {
                "[pc]", "[vc]", "[id2]", "[unpvt].[pc]", "[unpvt].[vc]", "[unpvt].[id2]"
            }));

            Assert.That(result.Select(c => c.Name), Is.EquivalentTo(new[]
            {
                "[pc]", "[vc]", "[id2]"
            }));
        }
    }
}