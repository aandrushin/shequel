﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class TempTableRelationBindingTest
    {
        private ExtensibleRuleDescriptor parser = TSql.From_Relation;

        [Test]
        public void TempTableRelationSymbol_Bindings()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.TempTableRelation>("#t").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "#t"
            }));
        }

        [Test]
        public void TempTableRelationSymbol_Bindings_alias()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.TempTableRelation>("#t a").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[a]"
            }));
        }
    }
}