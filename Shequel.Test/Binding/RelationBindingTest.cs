﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nemerle.Collections;
using Nitra;
using Nitra.Declarations;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class RelationBindingTest
    {
        private readonly StartRuleDescriptor parser = TSql.From_Relation;

        [Test]
        public void Relation_Join_Columns()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .AddColumns(new[] {"a1", "a2", "a3"})
                .Define(scope);

            Build
                .ExternalTable()
                .WithName("b", "dbo", "c", "d")
                .AddColumns(new[] {"b1", "b2", "b3"})
                .Define(scope);

            var result = parser.Parse<Join.Inner>("a inner join b on a.a1 = b.b1");
            result.Scope = scope;
            result.ContainingTable = scope;

            var host = new ColumnEvalHost(result, scope, new ParseOptions());
            var context = new DependentPropertyEvalContext();
            host.EvalProperties(context, stage: 0);
            host.EvalProperties(context, stage: 1);
            host.EvalProperties(context, stage: 2);

            var columns = new LightList<ColumnDeclarationSymbol>();
            scope.FindMany(s => true, ref columns);

            Assert.That(columns.ToArray().Select(c => c.Name), Is.EquivalentTo(new []
            {
                "[a1]", "[a2]", "[a3]", "[b1]", "[b2]", "[b3]"
            }));
        }
    }
}
