﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class RelationRefTest : RefTest
    {
        private SimpleRuleDescriptor parser = TSql.Select;

        [Test]
        public void RelationRef_projection_expression()
        {
            var result = parser.Eval<Select>("select a, b.a, b.a + 1, - b.a, (b.a), case when b.a > 1 then b.a else b.a end, case b.a when b.a then b.a else b.a end from (values (1)) b(a)");

            var column1 = Cast<ColumnIdentifier>(result.Projection.ElementAt(0));
            var column2 = Cast<ColumnIdentifier>(result.Projection.ElementAt(1));
            var binary = Cast<Expression.Binary>(result.Projection.ElementAt(2));
            var unary = Cast<Expression.Unary>(result.Projection.ElementAt(3));
            var sub = Cast<Expression.Sub>(result.Projection.ElementAt(4));
            var searchCase = Cast<Expression.SearchCase>(result.Projection.ElementAt(5));
            var simpleCase = Cast<Expression.SimpleCase>(result.Projection.ElementAt(6));
            
            Assert.That(column1.RelationRef.IsUnresolved);
            Assert.That(column2.RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(binary.Left).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(unary.Expression).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(sub.Inner).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(searchCase.Else.Value).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(searchCase.Items.First().When).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(searchCase.Items.First().Then).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(simpleCase.Else.Value).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(simpleCase.Expression).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(simpleCase.Items.First().When).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(simpleCase.Items.First().Then).RelationRef.IsUnresolved, Is.False);
        }

        [Test]
        public void RelationRef_select()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .AddColumn("id")
                .Define(scope);

            var result = parser.Eval<Select>("select id from a inner join a b on a.id = b.id where a.id > 0 group by a.id having a.id > 1 order by a.id", scope);

            var join = Cast<Join.Inner>(result.From.First());
            var where = result.Where.Value;
            var having = result.Having.Value;
            var groupByItem = result.GroupBy.Value.Items.First();
            var orderByItem = result.OrderBy.Value.Items.First();

            Assert.That(Cast<ColumnIdentifier>(join.On).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(where).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(having).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(groupByItem).RelationRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(orderByItem.Expression).RelationRef.IsUnresolved, Is.False);
        }
        
        [Test]
        public void RelationRef_from()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .Define(scope);

            Build
                .ExternalTableFunction()
                .WithName("b", "dbo", "c", "d")
                .Define(scope);

            Build
                .Declare()
                .WithName("@e")
                .WithType(Build.SimpleType().WithName("int"))
                .Define(scope);
            
            var result = parser.Eval<Select>("select a.id, b.id, c.id, d.id, e.id, f.id from a, b(), (values (1)) c(id), (select 1 id) d, @e e, #f f", scope);

            var table = Cast<ColumnIdentifier>(result.Projection.ElementAt(0));
            var function = Cast<ColumnIdentifier>(result.Projection.ElementAt(1));
            var values = Cast<ColumnIdentifier>(result.Projection.ElementAt(2));
            var subquery = Cast<ColumnIdentifier>(result.Projection.ElementAt(3));
            var parameter = Cast<ColumnIdentifier>(result.Projection.ElementAt(4));
            var temptable = Cast<ColumnIdentifier>(result.Projection.ElementAt(5));

            Assert.That(table.RelationRef.IsUnresolved, Is.False);
            Assert.That(function.RelationRef.IsUnresolved, Is.False);
            Assert.That(values.RelationRef.IsUnresolved, Is.False);
            Assert.That(subquery.RelationRef.IsUnresolved, Is.False);
            Assert.That(parameter.RelationRef.IsUnresolved, Is.False);
            Assert.That(temptable.RelationRef.IsUnresolved, Is.False);
        }
    }

    public abstract class RefTest
    {
        protected T Cast<T>(Projection projection)
        {
            return (T)((Projection.Simple)projection).Expression;
        }

        protected T Cast<T>(Predicate predicate)
        {
            return (T)((Predicate.Comparison)predicate).Left;
        }

        protected T Cast<T>(GroupByItem item)
        {
            return (T)((GroupByItem.Simple)item).Expression;
        }

        protected T Cast<T>(Expression expression)
        {
            return (T)expression;
        }

        protected T Cast<T>(Relation relation)
        {
            return (T)relation;
        }
    }
}
