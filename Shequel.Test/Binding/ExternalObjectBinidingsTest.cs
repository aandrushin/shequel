﻿using System.Collections.Immutable;
using Nitra;
using Nitra.Declarations;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class ExternalObjectBinidingsTest
    {
        [Test]
        public void ExternalObject_Bindings()
        {
            var declaration = Build
                .ExternalTable()
                .WithName("a", "b", "c", "d")
                .Create();

            var symbol = declaration.EvalExternalSymbol<ExternalObject.TableSymbol>();
            
            Assert.That(symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[b].[a]", "[c].[b].[a]", "[d].[c].[b].[a]"
            }));
        }

        [Test]
        public void ExternalObject_Bindings_default_schema()
        {
            var declaration = Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .Create();

            var symbol = declaration.EvalExternalSymbol<ExternalObject.TableSymbol>();
            
            Assert.That(symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[a]", "[dbo].[a]", "[c].[dbo].[a]", "[d].[c].[dbo].[a]", "[c]..[a]", "[d].[c]..[a]", 
            }));
        }
    }
}