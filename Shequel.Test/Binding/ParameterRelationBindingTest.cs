﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class ParameterRelationBindingTest
    {
        private ExtensibleRuleDescriptor parser = TSql.From_Relation;

        [Test]
        public void ParameterRelationSymbol_Bindings()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.ParameterRelation>("@a").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "@a"
            }));
        }

        [Test]
        public void ParameterRelationSymbol_Bindings_alias()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.ParameterRelation>("@a b").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[b]"
            }));
        }

        [Test]
        public void ParameterRelationSymbol_Columns()
        {
            var scope = new BindingScope();
            var columnType = Build
                .SimpleType()
                .WithName("int");
            var declareType = Build
                .TableType()
                .AddColumns(new [] { "id1", "id2" }, columnType);
            Build
                .Declare()
                .WithName("@a")
                .WithType(declareType)
                .Define(scope);

            var results = parser.EvalSymbolColumns<RelationDeclaration.ParameterRelation>("@a b", scope).ToArray();

            Assert.That(results.SelectMany(c => c.Bindings), Is.SupersetOf(new[]
           {
                "[id1]", "[id2]", "[b].[id1]", "[b].[id2]"
            }));

            Assert.That(results.Select(c => c.Name), Is.EquivalentTo(new[]
            {
                "[id1]", "[id2]"
            }));
        }
    }
}