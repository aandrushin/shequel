﻿using System.Linq;
using Nemerle.Collections;
using Nitra;
using Nitra.Declarations;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class PivotRelationBindingTest
    {
        private ExtensibleRuleDescriptor parser = TSql.From_Relation;
        
        [Test]
        public void PivotRelationSymbol_Bindings_alias()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.PivotRelation>("dbo.tbl PIVOT (COUNT (id) FOR id IN ([a])) b").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[b]"
            }));
        }

        [Test]
        public void PivotRelationSymbol_Columns()
        {
            var results = parser.EvalSymbolColumns<RelationDeclaration.PivotRelation>("(select 1 id, 2 id2) a PIVOT (COUNT (id) FOR id IN ([a], [b], [c])) b").ToArray();

            Assert.That(results.SelectMany(c => c.Bindings), Is.SupersetOf(new[]
            {
                "[a]", "[b]", "[c]", "[id2]", "[b].[a]", "[b].[b]", "[b].[c]", "[b].[id2]"
            }));
            
            Assert.That(results.Select(c => c.Name), Is.EquivalentTo(new[]
            {
                "[a]", "[b]", "[c]", "[id2]"
            }));
        }

        [Test]
        public void PivotRelationSymbol_Columns_join()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .AddColumns(new[] {"a1", "a2"})
                .Define(scope);

            Build
                .ExternalTable()
                .WithName("b", "dbo", "c", "d")
                .AddColumns(new[] {"b1", "b2"})
                .Define(scope);
            
            var result = parser.Parse<RelationDeclaration.PivotRelation>("(a inner join b on a.a1 = b.b1) PIVOT (COUNT (a1) FOR a1 IN ([a], [b], [c])) b");
            result.Scope = scope;
            result.ContainingTable = scope;

            var host = new ColumnEvalHost(result, scope, new ParseOptions());
            var context = new DependentPropertyEvalContext();
            host.EvalProperties(context, stage: 0);
            host.EvalProperties(context, stage: 1);
            host.EvalProperties(context, stage: 2);

            var columns = new LightList<ColumnDeclarationSymbol>();
            scope.FindMany(s => true, ref columns);

            Assert.That(columns.ToArray().SelectMany(c => c.Bindings), Is.EquivalentTo(new[]
            {
                 "[a]", "[b]", "[c]", "[a2]", "[b1]", "[b2]", "[b].[a]", "[b].[b]", "[b].[c]", "[b].[a2]", "[b].[b1]", "[b].[b2]"
            }));

            Assert.That(columns.ToArray().Select(c => c.Name), Is.EquivalentTo(new[]
            {
                 "[a]", "[b]", "[c]", "[a2]", "[b1]", "[b2]"
            }));
        }
    }
}