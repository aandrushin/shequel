﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class ColumnRefTest : RefTest
    {
        private SimpleRuleDescriptor parser = TSql.Select;

        [Test]
        public void ColumnRef_projection()
        {
            var result = parser.Eval<Select>("select a, b.a, b.a + 1, - b.a, (b.a), case when b.a > 1 then b.a else b.a end, case b.a when b.a then b.a else b.a end from (values (1)) b(a)");

            var column1 = Cast<ColumnIdentifier>(result.Projection.ElementAt(0));
            var column2 = Cast<ColumnIdentifier>(result.Projection.ElementAt(1));
            var binary = Cast<Expression.Binary>(result.Projection.ElementAt(2));
            var unary = Cast<Expression.Unary>(result.Projection.ElementAt(3));
            var sub = Cast<Expression.Sub>(result.Projection.ElementAt(4));
            var searchCase = Cast<Expression.SearchCase>(result.Projection.ElementAt(5));
            var simpleCase = Cast<Expression.SimpleCase>(result.Projection.ElementAt(6));

            Assert.That(column1.ColumnRef.IsUnresolved, Is.False);
            Assert.That(column2.ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(binary.Left).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(unary.Expression).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(sub.Inner).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(searchCase.Else.Value).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(searchCase.Items.First().When).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(searchCase.Items.First().Then).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(simpleCase.Else.Value).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(simpleCase.Expression).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(simpleCase.Items.First().When).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(simpleCase.Items.First().Then).ColumnRef.IsUnresolved, Is.False);
        }

        [Test]
        public void ColumnRef_select()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .AddColumn("id")
                .Define(scope);

            var result = parser.Eval<Select>("select id from a inner join a b on a.id = b.id where a.id > 0 group by a.id having a.id > 1 order by a.id", scope);

            var join = Cast<Join.Inner>(result.From.First());
            var where = result.Where.Value;
            var having = result.Having.Value;
            var groupByItem = result.GroupBy.Value.Items.First();
            var orderByItem = result.OrderBy.Value.Items.First();

            Assert.That(Cast<ColumnIdentifier>(join.On).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(where).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(having).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(groupByItem).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(orderByItem.Expression).ColumnRef.IsUnresolved, Is.False);
        }

        [Test]
        public void ColumnRef_from()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .AddColumn("id")
                .Define(scope);

            Build
                .ExternalTableFunction()
                .WithName("b", "dbo", "c", "d")
                .AddColumn("id")
                .Define(scope);

            Build
                .Declare()
                .WithName("@e")
                .WithType(Build.TableType().AddColumn("id", Build.SimpleType().WithName("int")))
                .Define(scope);

            var result = parser.Eval<Select>("select a.id, b.id, c.id, d.id, e.id, f.id from a, b(), (values (1)) c(id), (select 1 id) d, @e e, #f f", scope);

            var table = Cast<ColumnIdentifier>(result.Projection.ElementAt(0));
            var function = Cast<ColumnIdentifier>(result.Projection.ElementAt(1));
            var values = Cast<ColumnIdentifier>(result.Projection.ElementAt(2));
            var subquery = Cast<ColumnIdentifier>(result.Projection.ElementAt(3));
            var parameter = Cast<ColumnIdentifier>(result.Projection.ElementAt(4));
            // var temptable = Cast<ColumnIdentifier>(result.Projection.ElementAt(5));

            Assert.That(table.ColumnRef.IsUnresolved, Is.False);
            Assert.That(function.ColumnRef.IsUnresolved, Is.False);
            Assert.That(values.ColumnRef.IsUnresolved, Is.False);
            Assert.That(subquery.ColumnRef.IsUnresolved, Is.False);
            Assert.That(parameter.ColumnRef.IsUnresolved, Is.False);
            // Assert.That(temptable.ColumnRef.IsUnresolved, Is.False);
        }

        [Test]
        public void ColumnRef_pivot()
        {
            var result = parser.Eval<Select>("select b.a, b.b, b.c, b.id2 from (select 1 id, 2 id2) a PIVOT (COUNT (id) FOR id IN ([a], [b], [c])) b");

            Assert.That(Cast<ColumnIdentifier>(result.Projection.ElementAt(0)).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(result.Projection.ElementAt(1)).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(result.Projection.ElementAt(2)).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(result.Projection.ElementAt(3)).ColumnRef.IsUnresolved, Is.False);
        }

        [Test]
        public void ColumnRef_unpivot()
        {
            var result = parser.Eval<Select>("select unpvt.vc, unpvt.pc, unpvt.id2 from (select 1 id, 2 id2) a UNPIVOT (vc FOR pc IN (id)) AS unpvt");

            Assert.That(Cast<ColumnIdentifier>(result.Projection.ElementAt(0)).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(result.Projection.ElementAt(1)).ColumnRef.IsUnresolved, Is.False);
            Assert.That(Cast<ColumnIdentifier>(result.Projection.ElementAt(2)).ColumnRef.IsUnresolved, Is.False);
        }
    }
}