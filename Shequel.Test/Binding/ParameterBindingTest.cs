﻿using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class ParameterBindingTest
    {
        private SimpleRuleDescriptor parser = TSql.Parameter;

        [Test]
        public void Parameter_DeclareReference()
        {
            Assert.That(parser.Eval<Parameter>("@a").DeclareReference.Text, Is.EqualTo("@a"));
        }
    }
}