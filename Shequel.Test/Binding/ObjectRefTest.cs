﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class ObjectRefTest : RefTest
    {
        private SimpleRuleDescriptor parser = TSql.Select;

        [Test]
        public void ObjectRef()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .Define(scope);

            Build
                .ExternalTableFunction()
                .WithName("b", "dbo", "c", "d")
                .Define(scope);

            var result = parser.Eval<Select>("select id from a, b()", scope);

            var table = Cast<RelationDeclaration.ObjectRelation>(result.From.ElementAt(0));
            var function = Cast<RelationDeclaration.FunctionRelation>(result.From.ElementAt(1));

            Assert.That(table.ObjectName.ObjectRef.IsUnresolved, Is.False);
            Assert.That(function.FunctionName.ObjectRef.IsUnresolved, Is.False);
        }
    }
}