﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class FunctionRelationBindingTest
    {
        private ExtensibleRuleDescriptor parser = TSql.From_Relation;

        [Test]
        public void FunctionRelationSymbol_Bindings()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.FunctionRelation>("d.c.b.a()").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[a]", "[b].[a]", "[c].[b].[a]", "[d].[c].[b].[a]"
            }));
        }

        [Test]
        public void FunctionRelationSymbol_Bindings_alias()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.FunctionRelation>("d.c.b.a() b").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[b]"
            }));
        }

        [Test]
        public void FunctionRelationSymbol_Bindings_default_schema()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.FunctionRelation>("d.c..a()").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[a]", "[c]..[a]", "[d].[c]..[a]"
            }));
        }

        [Test]
        public void FunctionRelationSymbol_ExternalTableFunction_Columns()
        {
            var scope = new BindingScope();
            Build.ExternalTableFunction()
                .WithName("a", "b", "c", "d")
                .AddColumns(new[] { "id1", "id2" })
                .Define(scope);

            var results = parser.EvalSymbolColumns<RelationDeclaration.FunctionRelation>("d.c.b.a()", scope).ToArray();

            Assert.That(results.SelectMany(c => c.Bindings), Is.SupersetOf(new[]
            {
                "[id1]", "[id2]", "[a].[id1]", "[a].[id2]", "[b].[a].[id1]", "[b].[a].[id2]", "[c].[b].[a].[id1]", "[c].[b].[a].[id2]"
            }));

            Assert.That(results.Select(c => c.Name), Is.EquivalentTo(new[]
            {
                "[id1]", "[id2]"
            }));
        }
    }
}