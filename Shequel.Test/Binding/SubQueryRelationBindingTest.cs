﻿using System.Linq;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Binding
{
    [TestFixture]
    public class SubQueryRelationBindingTest
    {
        private ExtensibleRuleDescriptor parser = TSql.From_Relation;

        [Test]
        public void SubQueryRelationSymbol_Bindings()
        {
            Assert.That(parser.EvalSymbol<RelationDeclaration.SubQueryRelation>("(select 1) a").Symbol.Bindings, Is.EquivalentTo(new[]
            {
                "[a]"
            }));
        }

        [Test]
        public void SubQuery_Columns()
        {
            var result = parser.EvalSymbolColumns<RelationDeclaration.SubQueryRelation>("(select 1 + 1 a, 1, a.id) a").ToArray();

            Assert.That(result.SelectMany(c => c.Bindings), Is.EquivalentTo(new []
            {
                "[a]", "[id]", "[a].[a]", "[a].[id]"
            }));

            Assert.That(result.Select(c => c.Name), Is.EquivalentTo(new []
            {
                "[a]", "[id]"
            }));
        }

        [Test]
        public void SubQuery_Columns_alias()
        {
            var result = parser.EvalSymbolColumns<RelationDeclaration.SubQueryRelation>("(select 1 + 1 a, 1, a.id) a(id1, id2, id3)").ToArray();

            Assert.That(result.SelectMany(c => c.Bindings), Is.EquivalentTo(new[]
            {
                "[id1]", "[id2]", "[id3]", "[a].[id1]", "[a].[id2]", "[a].[id3]"
            }));

            Assert.That(result.Select(c => c.Name), Is.EquivalentTo(new []
            {
                "[id1]", "[id2]", "[id3]"
            }));
        }
    }
}