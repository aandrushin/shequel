﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nitra.Declarations;
using Nitra.ProjectSystem;
using NUnit.Framework;
using Shequel.Internal;

namespace Shequel.Test.Messages
{
    [TestFixture]
    public class SignatureValidatorTest
    {
        [Test]
        public void UserdDefinedFunction_Signature_validating()
        {
            var scope = new BindingScope();
            Build.ExternalScalarFunction()
                .WithName("a", "dbo", "c", "d")
                .AddParameters(new[] {"id1", "id2"})
                .Define(scope);

            var ast = TSql.UserDefinedFunction.Eval<Function.UserDefined>("a()", scope);
            var messages = new CompilerMessageList(); 
            var visitor = new ValidateFunctionSignatureVisitor(messages);
            visitor.Visit(ast);

            Assert.That(messages.Select(m => m.Text), Is.EquivalentTo(new[]
            {
                "The a(id1, id2) function requires 2 argument(s)."
            }));
        }

        [Test]
        public void SystemFunction_Signature_validating()
        {
            var ast = TSql.SystemFunction.Eval<Function.System>("newid(1)");
            var messages = new CompilerMessageList(); 
            var visitor = new ValidateFunctionSignatureVisitor(messages);
            visitor.Visit(ast);

            Assert.That(messages.Select(m => m.Text), Is.EquivalentTo(new[]
            {
                "The newid() function requires 0 argument(s)."
            }));
        }
    }
}
