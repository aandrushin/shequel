﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nitra;
using NUnit.Framework;

namespace Shequel.Test.Messages
{
    [TestFixture]
    public class ColumnIdentifierVisitorTest
    {
        private SimpleRuleDescriptor parser = TSql.Select;

        [Test]
        public void ColumnIdentifierVisitor_unresolved_column()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .Define(scope);

            var result = parser.EvalMessages("select a.id from a", scope);

            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "Unresolved column name 'id'."
            }));
        }

        [Test]
        public void ColumnIdentifierVisitor_ambiguous_column()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .AddColumn("id")
                .Define(scope);

            Build
                .ExternalTable()
                .WithName("b", "dbo", "c", "d")
                .AddColumn("id")
                .Define(scope);

            var result = parser.EvalMessages("select a.id from a, b", scope);

            Assert.That(result.Select(r => r.Text), Is.EquivalentTo(new []
            {
                "Ambiguous column name 'id'."
            }));
        }

        [Test]
        public void ColumnIdentifierVisitor_unresolved_object()
        {
            var result = parser.EvalMessages("select a.id from a");

            Assert.That(result.Select(r => r.Text), Is.SupersetOf(new []
            {
                "Invalid object name 'a'."
            }));
        }

        [Test]
        public void ColumnIdentifierVisitor_ambiguous_object()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .AddColumn("id1")
                .Define(scope);

            Build
                .ExternalView()
                .WithName("a", "b2", "c", "d")
                .AddColumn("id2")
                .Define(scope);

            var result = parser.EvalMessages("select a.id from a, b2.a", scope);

            Assert.That(result.Select(r => r.Text), Is.SupersetOf(new []
            {
                "Ambiguous reference 'a'."
            }));
        }

        [Test]
        public void ColumnIdentifierVisitor_ambiguous_external_object()
        {
            var scope = new BindingScope();

            Build
                .ExternalTable()
                .WithName("a", "dbo", "c", "d")
                .AddColumn("id1")
                .Define(scope);

            Build
                .ExternalView()
                .WithName("a", "dbo", "c", "d")
                .AddColumn("id2")
                .Define(scope);

            var result = parser.EvalMessages("select a.id from a", scope);

            Assert.That(result.Select(r => r.Text), Is.SupersetOf(new []
            {
                "Ambiguous reference 'a'."
            }));
        }

        [Test]
        public void ColumnIdentifierVisitor_pivot()
        {
            var result = parser.EvalMessages("select [2] from (select 1 [id]) a pivot (COUNT ([id]) for [id] in ([id], [1], [2])) a");

            Assert.That(result.Select(r => r.Text), Is.SupersetOf(new []
            {
                "The column name '[id]' specified in the PIVOT operator conflicts with the existing column name in the PIVOT argument."
            }));
        }

        [Test]
        public void ColumnIdentifierVisitor_unpivot_1()
        {
            var result = parser.EvalMessages("select a.c, a.d from a unpivot (c for c1 in (c1, c2, c3)) a");

            Assert.That(result.Select(r => r.Text), Is.SupersetOf(new []
            {
                "The column name 'c1' specified in the UNPIVOT operator conflicts with the existing column name in the UNPIVOT argument."
            }));
        }

        [Test]
        public void ColumnIdentifierVisitor_unpivot_2()
        {
            var result = parser.EvalMessages("select a.c, a.d from a unpivot (c1 for d in (c1, c2, c3)) a");

            Assert.That(result.Select(r => r.Text), Is.SupersetOf(new []
            {
                "The column name 'c1' specified in the UNPIVOT operator conflicts with the existing column name in the UNPIVOT argument."
            }));
        }
    }
}
