﻿using Nemerle;
using Nemerle.Assertions;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.IO;

namespace Shequel
{
	using Utils;
	using AstUtils;

    public class BindingScope : TableScope
	{
		public this ()
		{
			base ("binding scope", null);
			
			DefaultSchema = "dbo";
			CaseComparer = StringComparer.OrdinalIgnoreCase;
		}
	
		public DefaultSchema : string { get; set; }
		public CaseComparer : IEqualityComparer[string] { get; set; }
		
		public Define[TSymbol]([NotNull] externalObject : ExternalObject) : TSymbol
			where TSymbol : ExternalObjectSymbol
		{
			def symbol = Define.[TSymbol](externalObject, null);
			externalObject.Symbol = symbol;
			
			symbol.Bindings = 
				if (string.IsNullOrEmpty(externalObject.Schema))
					CreateDefaultBindings(externalObject)
				else if (CaseComparer.Equals(Escape(externalObject.Schema), Escape(DefaultSchema)))
					CreateDefaultBindings(externalObject)
				else
					CreateBindings(externalObject);
			symbol;
		}
		
		public Reset() : void
		{
			Undefine(_ => true);
		}
		
		internal EvalProperties(context : DependentPropertyEvalContext) : void
		{
			foreach (symbol in GetSymbols())
			{
				foreach (declaration in symbol.Declarations)
					unless (declaration.IsAllPropertiesEvaluated)
						declaration.EvalProperties(context);
				
				unless (symbol.IsAllPropertiesEvaluated)
					symbol.EvalProperties(context);
			}
		}
		
		public override BindMany[TSymbol](reference : Reference, results : ref LightList[TSymbol]) : void
        {
			def bindings = GetBindings.[TSymbol](reference.Text);
			foreach (binding in bindings)
				base.BindMany.[TSymbol](Reference(Location.Default, binding), ref results);
        }
		
		public override MakeCompletionList(prefix : string) : Seq[DeclarationSymbol]
		{
			def filterSymbol(symbol : DeclarationSymbol, prefix : string)
			{
				match (symbol)
				{
					| s is ExternalObjectSymbol => s.Bindings.Any(b => b.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
					| s is RelationDeclarationSymbol => s.Bindings.Any(b => b.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
					| s is ColumnDeclarationSymbol => s.Bindings.Any(b => b.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
					| _ => symbol.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase)
				}
			}
			
			GetSymbols().Where(s => filterSymbol(s, prefix));
		}
		
		private GetBindings[TSymbol](binding : string) : Seq[string]
			where TSymbol : DeclarationSymbol
		{
			def symbols = GetSymbols();
			def filterSymbol(symbol : DeclarationSymbol, binding : string)
			{
				match (symbol)
				{
					| s is ExternalObjectSymbol => HashSet(s.Bindings, CaseComparer).Contains(binding)
					| s is RelationDeclarationSymbol => HashSet(s.Bindings, CaseComparer).Contains(binding)
					| s is ColumnDeclarationSymbol => HashSet(s.Bindings, CaseComparer).Contains(binding)
					| _ => CaseComparer.Equals(symbol.Name, binding);
				}
			}
			
			symbols.OfType.[TSymbol]().Where(s => filterSymbol(s, binding)).Select(s => s.Name);
		}
		
		private GetSymbols() : Seq[DeclarationSymbol]
		{
			Symbols.SelectMany(s => s);
		}
		
		private CreateBindings(externalObject : ExternalObject) : ImmutableArray[string]
		{
			ImmutableArray.Create(
				CreateBindings(externalObject.Object, externalObject.Schema, externalObject.Database, externalObject.Server).ToArray());
		}
		
		private CreateBindings(name : string, schema : string, database : string, server : string) : list[string]
		{
			| (	_, "", "",  "") => []
			| (	n, 	s, "",  "") => [ CreateObjectBinding(n, s, "",  "") ]
			| (	n, 	s, 	d,  "") => 	 CreateObjectBinding(n, s,  d,  "") :: CreateBindings(n, s, "", "")
			| (	n, 	s, 	d, srv) => 	 CreateObjectBinding(n, s,  d, srv) :: CreateBindings(n, s,  d, "")
		}
		
		private CreateDefaultBindings(externalObject : ExternalObject) : ImmutableArray[string]
		{
			ImmutableArray.Create(
				CreateDefaultBindings(externalObject.Object, externalObject.Schema, externalObject.Database, externalObject.Server).ToArray());
		}
		
		private CreateDefaultBindings(name : string, schema : string, database : string, server : string) : list[string]
		{
			| ("", "", "", 	"") => []
			| (	n, "", "", 	"") => [ CreateObjectBinding(n, "", "",  "") ]
			| (	n, "", 	d, 	"") => 	 CreateObjectBinding(n, "",  d,  "") :: CreateDefaultBindings(n, "", "", "")
			| (	n, "", 	d, srv) => 	 CreateObjectBinding(n, "",  d, srv) :: CreateDefaultBindings(n, "",  d, "")
			| (	n,  s, "", 	"") => 	 CreateObjectBinding(n,  s, "",  "") :: CreateDefaultBindings(n, "", "", "")
			| (	n,  s, 	d, 	"") => 	 CreateObjectBinding(n,  s,  d,  "") :: CreateObjectBinding(n, "",  d,  "") :: CreateDefaultBindings(n, s, "", "")
			| (	n,  s, 	d, srv) =>   CreateObjectBinding(n,  s,  d, srv) :: CreateObjectBinding(n, "",  d, srv) :: CreateDefaultBindings(n, s,  d, "")
		}
	}
	
	[Record]
	[StructuralEquality]
    public class CompletionInfo
	{
		public Text	: string { get; }
		public Kind	: CompletionObjectKind { get; }
	}    
	
	public enum CompletionObjectKind
    {
        | Column
        | Table
        | View
        | TableFunction
        | ScalarFunction
        | Parameter
        | Relation
		| Procedure
		| UserDefinedType
		| XmlSchema
    }
	
	internal module ScopeFilters
	{
		public static Relation(symbol : DeclarationSymbol) : bool
		{
			| ExternalObject.TableSymbol
			| ExternalObject.ViewSymbol
			| ExternalObject.TableFunctionSymbol
			| DeclareSymbol(
				FirstDeclarationOrDefault=Declare(
					DataType = DataType.Table))
			| RelationDeclaration.ParameterRelationSymbol(
				FirstDeclarationOrDefault=RelationDeclaration.ParameterRelation(
					Parameter=(IsDeclareRefEvaluated = true, 
						DeclareRef=(IsSymbolEvaluated = true, 
							Symbol=DeclareSymbol(
								FirstDeclarationOrDefault=Declare(
									DataType = DataType.Table)))))) => true
			| _ => false
		}
		
		public static Projection(symbol : DeclarationSymbol) : bool
		{
			| ExternalObject.ProcedureSymbol
			| ExternalObject.UserDefinedTypeSymbol
			| ExternalObject.TableFunctionSymbol => false
			| RelationDeclaration.ParameterRelationSymbol(
				FirstDeclarationOrDefault=RelationDeclaration.ParameterRelation(
					Alias = a,
					Parameter=(IsDeclareRefEvaluated = true, 
						DeclareRef=(IsSymbolEvaluated = true, 
							Symbol=DeclareSymbol(
								FirstDeclarationOrDefault=Declare(
									DataType = t)))))) => !(t is DataType.Table) || a.HasValue
			| DeclareSymbol(
				FirstDeclarationOrDefault=Declare(
					DataType = t)) => !(t is DataType.Table)
			| _ => true
		}
	}
}