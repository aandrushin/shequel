﻿using Nemerle;
using Nemerle.Assertions;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;
using Nitra.Runtime.Reflection;
using Nitra.Serialization2;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;

namespace Shequel
{
	using Internal;
	using AstUtils;

	public class ShequelQueryProject : Project
	{
		private files : List[ShequelQueryFile];
	
		public this()
		{
			this.files = List();
		}
	
		public override Files : Seq[File]
		{
		  get { this.files }
		}
		
		public Add([NotNull] file : ShequelQueryFile) : void
		{
			file.Project = this;
			files.Add(file);
		}
		
		public EvalProperties() : void
		{
			foreach (file in files)
				file.EvalProperties();
		}
	}
	
	public class ShequelQueryFile : File
	{
		private mutable ast 		: IAst;
		private mutable parseTree 	: ParseTree;
		private mutable parseResult : IParseResult;
		private mutable evalMessages: Seq[CompilerMessage];
		private parser      		: IncrementalParser;
	
		public this (name : string)
		{
			this (name, string.Empty);
		}
	
		public this ([NotNull] name : string, [NotNull] text : string)
		{
			base (GetFileStatistics());
				
			FullName 		= name;
			RootScope 		= BindingScope();
			Text 			= text;
			Language 		= ShequelLang.Instance;
			Options			= ParseOptions();
			parser 			= IncrementalParser();
			evalMessages 	= Enumerable.Empty();
		}
	
		public Text 				: string { get; set; }
		public RootScope			: BindingScope { get; }
		public Options				: ParseOptions { get; }
        public override Project 	: Project 	{ get; set; }
        public override Length 		: int 		{ get { Text.Length; } }
        public override FullName 	: string 	{ get; private set; }
        public override Language  	: Language 	{ get; private set; }
        
		public EvalMessages : IReadOnlyCollection[CompilerMessage] 
		{ 
			get { evalMessages.ToList(); } 
		}
		
		public override ParseResult : IParseResult 
		{ 
			get { when (parseResult == null) Parse(); parseResult; } 
		}
        
		public override Ast : IAst 		
		{ 
			get { when (ast == null) UpdateAst(); ast; } 
		}
		
		public Query : Query 
		{
			get { Ast :> Query; } 
		}
		
        public override HasAst : bool 		
		{ 
			get { ast != null; }
		}
	
		public override GetParseTree() : ParseTree
        {
            when (parseTree == null) UpdateParseTree(); parseTree;
        }

		public Parse() : void
		{
			try
			{
				_typingMessages.Clear();
				_astMessages.Clear();
				_parseMessages.Clear();
				
				Statistics.Parse.Restart();
				
				parseResult = parser.DetectChangesAndParse(GetParseSession(), GetSource());
			}
			finally
			{
				Statistics.Parse.Stop();
			}
		}
		
		public UpdateParseTree() : void
		{
			when (parseResult == null)
				Parse();
			
			try
			{
				Statistics.ParseTree.Restart();
				
				parseTree = parseResult.CreateParseTree();
			}
			finally
			{
				Statistics.ParseTree.Stop();
			}
		}
		
		public UpdateAst() : void
		{
			when (parseTree == null)
				UpdateParseTree();
			
			try
			{
				_astMessages.Clear();
				_typingMessages.Clear();
				
				Statistics.Ast.Restart();
			
				ast = AstContext.GetAstUntyped(parseTree, _astMessages);
			}
			finally 
			{
				Statistics.Ast.Stop();
			}
		}
		
		public EvalProperties() : void
		{
			def context  	= DependentPropertyEvalContext();
			def evalData	= GetEvalPropertiesData();
			context.Files 	= ImmutableArray.Create(evalData);
			
			def evalHost 	= ShequelEvalHost(Ast, RootScope, Options);
			def task 		= Statistics.Typing.Tasks.Single();
			
			try
			{
				when (task is StatisticsTask.Single as t)
					t.Start();
				
				RootScope.CaseComparer = Options.CaseComparer;
				RootScope.DefaultSchema = Options.DefaultSchema;
				RootScope.EvalProperties(context);
				
				evalHost.EvalProperties(context, "Eval properties", 0);
				evalHost.EvalProperties(context, "Resolve object references", 1);
				evalHost.EvalProperties(context, "Resolve column and relation references", 2);
				
				def messages = evalData.GetCompilerMessage();
				def visitor = ReferenceVisitor(messages, Options.CaseComparer);
				visitor.Visit(Ast);
				
				def functionSignatureVisitor = ValidateFunctionSignatureVisitor(messages);
				functionSignatureVisitor.Visit(Ast);
				
				when (Options.QuotedIdentifier)
				{
					def quotedVisitor = QuotedIdentifierVisitor(messages);
					quotedVisitor.Visit(Ast);
				}
				
				evalMessages = messages;
			}
			finally
			{
				when (task is StatisticsTask.Single as t)
					t.Start();
			}
		}
		
        public override DeepResetProperties() : void
        {
			_typingMessages.Clear();
            when (ast != null)
				IAstExtensions.DeepResetProperties(ast);
        }

        public override GetSource() : SourceSnapshot
        {
            SourceSnapshot(Text, this);
        }
		
		public override CompleteWord(pos : int, replacementSpan : out NSpan) : Seq[object]
		{
			replacementSpan 	= NSpan(pos, pos);
		
			def curretSpan     	= NSpan(pos, pos);
			def spans          	= HashSet();
			def spasesWalker   	= VoidRuleWalker(curretSpan);

			spasesWalker.Walk(ParseResult, spans);

			foreach (spanInfo when spanInfo.Span.Contains(curretSpan) && spanInfo.SpanClass != Language.DefaultSpanClass in spans)
				return [];
		
			mutable spacesStart = pos;
			mutable spacesEnd = pos;
			when (spans.Count != 0)
			{
			    spacesStart = spans.Min(s => s.Span.StartPos);
			    spacesEnd = spans.Max(s => s.Span.EndPos);
			}
				
			def caretPosition = NSpan(spacesStart, spacesEnd);
			def visitor = FindNodeAstVisitor(caretPosition);
			Ast.Accept(visitor);
			
			unless (Ast.IsAllPropertiesEvaluated)
				EvalProperties();			
			
			while (visitor.Stack.Count > 0)
			{
				def objs = match (visitor.Stack.Pop()) 
				{
					| id is ObjectIdentifier => {
						def prefix = id.Reference.Text.TrimEnd(']');
						id.Scope.MakeCompletionList(prefix);
					}
					| id is ColumnIdentifier => {
						def prefix = id.ColumnReference.Text.TrimEnd(']');
						id.Scope.MakeCompletionList(prefix);
					}
					| p is Parameter => {
						def prefix = p.Name.Value;
						p.Scope.MakeCompletionList(prefix);
					}
					| p is Projection.AstList => {
					    def prefix = string.Empty;
						FilteringScope(p.Scope, ScopeFilters.Projection).MakeCompletionList(prefix);
					}
					| r is Relation.AstList => {
						def prefix = string.Empty;
						def defaultSchemaPredicate (symbol : DeclarationSymbol) : bool {
							| s is ExternalObjectSymbol =>
								Options.CaseComparer.Equals(Escape(s.Schema), Escape(Options.DefaultSchema))
							| _ => true
						}
						FilteringScope(r.Scope, s => ScopeFilters.Relation(s) && defaultSchemaPredicate(s))
							.MakeCompletionList(prefix);
					}
					| p is Predicate => {
					    def prefix = string.Empty;
						FilteringScope(p.Scope, ScopeFilters.Projection).MakeCompletionList(prefix);
					}
					| a is BindableAst => {
						def prefix = string.Empty;
						a.Scope.MakeCompletionList(prefix);
					}
					| _ => Enumerable.Empty();
				}
				
				def arr = objs.ToArray();
				
				when (arr.Length > 0) {
					def projectSymbolToCompletionInfo(s : DeclarationSymbol) : CompletionInfo
					{
						| d is DeclareSymbol => 
							CompletionInfo(UnEscape(d.Name), CompletionObjectKind.Parameter)
						| e is ExternalObjectSymbol => 
							CompletionInfo(UnEscape(e.Object), e.CompletionKind)
						| r is RelationDeclarationSymbol =>
							CompletionInfo(UnEscape(r.Name), r.CompletionKind)
						| c is ColumnDeclarationSymbol =>
							CompletionInfo(UnEscape(c.Name), CompletionObjectKind.Column)
						| _ => 
							CompletionInfo(UnEscape(s.Name), CompletionObjectKind.Table)
					}
				
					return ImmutableArray.Create(
						arr.Select(projectSymbolToCompletionInfo).Distinct().ToArray());
				}
			}
			Enumerable.Empty.[CompletionInfo]();
		}
		
		protected virtual GetParseSession() : ParseSession
		{
			ParseSession(
				Language.StartRule, 
				Language.CompositeGrammar, 
				compilerMessages=_parseMessages, 
				onRecovery=ParseSession.SmartRecovery);
		}
		
		private static GetFileStatistics() : FileStatistics
		{
			def parseStatistics = StatisticsTask.Single("Parse");
			def parseTreeStatistics = StatisticsTask.Single("Parse tree");
			def astStatistics	= StatisticsTask.Single("Ast");
			def evalStatistics	= StatisticsTask.Single("Eval properties");
			def container 		= StatisticsTask.Container("Eval properties");
			container.Tasks.Add(evalStatistics);
			
			FileStatistics(
				parseStatistics,
				parseTreeStatistics,
				astStatistics,
				container);
		}
	}
}