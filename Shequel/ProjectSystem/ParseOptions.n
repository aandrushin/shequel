﻿using Nemerle;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Extensions;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;

namespace Shequel
{
	public class ParseOptions
	{
		public this ()
		{
			DefaultSchema = "dbo";
			QuotedIdentifier = true;
			CaseComparer = StringComparer.OrdinalIgnoreCase;
		}
	
		public DefaultSchema 	: string { get; set; }
		public QuotedIdentifier : bool { get; set; }
		public CaseComparer		: IEqualityComparer[string] { get; set; }
	}
}