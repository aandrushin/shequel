﻿using Nemerle;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.IO;

namespace Shequel
{
	using BuildImplementation;

    public static class Build
    {
        public static Declare() : ParameterDeclarationBuilder
        {
			ParameterDeclarationBuilder();
        }
		
		public static Parameter() : ParameterBuilder
        {
			ParameterBuilder();
        }
		
        public static SimpleType() : SimpleTypeBuilder
        {
			SimpleTypeBuilder();
        }
        
		public static TableType() : TableDataTypeBuilder
        {
			TableDataTypeBuilder();
        }
		
		public static ExternalTable() : ExternalTableBuilder
		{
			ExternalTableBuilder();
		}
		
		public static ExternalView() : ExternalViewBuilder
		{
			ExternalViewBuilder();
		}
		
		public static ExternalTableFunction() : ExternalTableFunctionBuilder
		{
			ExternalTableFunctionBuilder();
		}
		
		public static ExternalScalarFunction() : ExternalScalarFunctionBuilder
		{
			ExternalScalarFunctionBuilder();
		}
		
		public static ExternalUserDefinedType() : ExternalUserDefinedTypeBuilder
		{
			ExternalUserDefinedTypeBuilder();
		}
		
		public static ExternalProcedure() : ExternalProcedureBuilder
		{
			ExternalProcedureBuilder();
		}
    }
	
	public class BuildException 
		: Exception
	{
		public this (reason : string)
		{
			base (reason);
		}
	}
	
    namespace BuildImplementation
    {
		using BuilderUtil;
		using Internal;
		
        public class ExternalObjectBuilder
        {
            protected mutable obj : string;
		    protected mutable schema : string;
		    protected mutable database : string;
		    protected mutable server : string;
	
		    protected SetName(obj : string, schema : string, database : string, server : string) : void
		    {
			    this.obj = obj;
			    this.schema = schema;
			    this.database = database;
			    this.server = server;
		    }
		
		    protected SetObjectName(externalObject : ExternalObject) : void
		    {
			    externalObject.Object = obj;
			    externalObject.Schema = schema;
			    externalObject.Database = database;
			    externalObject.Server = server;
		    }
			
			protected GetColumns(columns : Seq[string]) : ImmutableArray[string]
			{
				ImmutableArray.Create(columns.ToArray());
			}
			
			protected GetSignature(name : string, parameters : Seq[string]) : SignatureInfo
			{
				def signatureParameters = parameters
					.Select(p => SignatureParameter(p, false))
					.ToImmutableArray();
				SignatureInfo(name, signatureParameters);
			}
        }
        
        public class ExternalTableBuilder : ExternalObjectBuilder
        {
			private columns : List[string] = List();
		
            public this ()
			{
				base.SetName(string.Empty, string.Empty, string.Empty, string.Empty);
			}
		
			public WithName(obj : string, schema : string, database : string, server : string) : ExternalTableBuilder
			{
				base.SetName(obj, schema, database, server);
				this;
			}
			
			public AddColumn(column : string) : ExternalTableBuilder
			{
				this.columns.Add(column);
				this;
			}
		
			public AddColumns(columns : Seq[string]) : ExternalTableBuilder
			{
				this.columns.AddRange(columns);
				this;
			}
			
			public Create() : ExternalObject.Table
			{
				def name = AstUtils.CreateQualifiedName(obj, schema, database, server);
				def table = ExternalObject.Table(Loc(), Name(Loc(), name));
                base.SetObjectName(table);
				table.Columns = GetColumns(columns);
				table;
			}
			
			public Define(scope : BindingScope) : ExternalObject.TableSymbol
			{
				scope.Define.[ExternalObject.TableSymbol](Create());
			}
        }
        
        public class ExternalViewBuilder : ExternalObjectBuilder
		{
			private columns : List[string] = List();
		
			public this ()  
			{
				base.SetName(string.Empty, string.Empty, string.Empty, string.Empty);
			}
		
			public WithName(obj : string, schema : string, database : string, server : string) : ExternalViewBuilder
			{
				base.SetName(obj, schema, database, server);
				this;
			}
			
			public AddColumn(column : string) : ExternalViewBuilder
			{
				this.columns.Add(column);
				this;
			}
		
			public AddColumns(columns : Seq[string]) : ExternalViewBuilder
			{
				this.columns.AddRange(columns);
				this;
			}
			
			public Create() : ExternalObject.View
			{
				def name = AstUtils.CreateQualifiedName(obj, schema, database, server);
				def table = ExternalObject.View(Loc(), Name(Loc(), name));
                base.SetObjectName(table);
				table.Columns = GetColumns(columns);
				table;
			}
			
			public Define(scope : BindingScope) : ExternalObject.ViewSymbol
			{
				scope.Define.[ExternalObject.ViewSymbol](Create());
			}
		}
		
		public class ExternalUserDefinedTypeBuilder : ExternalObjectBuilder
		{
			public this ()  
			{
				base.SetName(string.Empty, string.Empty, string.Empty, string.Empty);
			}
		
			public WithName(obj : string, schema : string) : ExternalUserDefinedTypeBuilder
			{
				base.SetName(obj, schema, string.Empty, string.Empty);
				this;
			}
			
			public Create() : ExternalObject.UserDefinedType
			{
				def name = AstUtils.CreateQualifiedName(obj, schema, string.Empty, string.Empty);
				def type = ExternalObject.UserDefinedType(Loc(), Name(Loc(), name));
                base.SetObjectName(type);
				type;
			}
			
			public Define(scope : BindingScope) : ExternalObject.UserDefinedTypeSymbol
			{
				scope.Define.[ExternalObject.UserDefinedTypeSymbol](Create());
			}
		}
		
		public class ExternalScalarFunctionBuilder : ExternalObjectBuilder
		{
			private parameters : List[string] = List();
		
			public this ()  
			{
				base.SetName(string.Empty, string.Empty, string.Empty, string.Empty);
			}
		
			public WithName(obj : string, schema : string, database : string, server : string) : ExternalScalarFunctionBuilder
			{
				base.SetName(obj, schema, database, server);
				this;
			}
			
			public AddParameters(parameters : Seq[string]) : ExternalScalarFunctionBuilder
			{
				this.parameters.AddRange(parameters);
				this;
			}
			
			public Create() : ExternalObject.ScalarFunction
			{
				def name = AstUtils.CreateQualifiedName(obj, schema, database, server);
				def function = ExternalObject.ScalarFunction(Loc(), Name(Loc(), name));
                base.SetObjectName(function);
				function.Signature = GetSignature(obj, parameters);
				function;
			}
			
			public Define(scope : BindingScope) : ExternalObject.ScalarFunctionSymbol
			{
				scope.Define.[ExternalObject.ScalarFunctionSymbol](Create());
			}
		}
		
		public class ExternalProcedureBuilder : ExternalObjectBuilder
		{
			public this ()
			{
				base.SetName(string.Empty, string.Empty, string.Empty, string.Empty);
			}
		
			public WithName(obj : string, schema : string, database : string, server : string) : ExternalProcedureBuilder
			{
				base.SetName(obj, schema, database, server);
				this;
			}
			
			public Create() : ExternalObject.Procedure
			{
				def name = AstUtils.CreateQualifiedName(obj, schema, database, server);
				def procedure = ExternalObject.Procedure(Loc(), Name(Loc(), name));
                base.SetObjectName(procedure);
				procedure;
			}
			
			public Define(scope : BindingScope) : ExternalObject.ProcedureSymbol
			{
				scope.Define.[ExternalObject.ProcedureSymbol](Create());
			}
		}
		
		public class ExternalTableFunctionBuilder : ExternalObjectBuilder
		{
			private columns 	: List[string] = List();
			private parameters 	: List[string] = List();
			
			public this ()  
			{
				base.SetName(string.Empty, string.Empty, string.Empty, string.Empty);
			}
		
			public WithName(obj : string, schema : string, database : string, server : string) : ExternalTableFunctionBuilder
			{
				base.SetName(obj, schema, database, server);
				this;
			}
			
			public AddColumn(column : string) : ExternalTableFunctionBuilder
			{
				this.columns.Add(column);
				this;
			}
		
			public AddColumns(columns : Seq[string]) : ExternalTableFunctionBuilder
			{
				this.columns.AddRange(columns);
				this;
			}
			
			public AddParameters(parameters : Seq[string]) : ExternalTableFunctionBuilder
			{
				this.parameters.AddRange(parameters);
				this;
			}
			
			public Create() : ExternalObject.TableFunction
			{
				def name = AstUtils.CreateQualifiedName(obj, schema, database, server);
				def function = ExternalObject.TableFunction(Loc(), Name(Loc(), name));
                base.SetObjectName(function);
				function.Columns = GetColumns(columns);
				function.Signature = GetSignature(obj, parameters);
				function;
			}
			
			public Define(scope : BindingScope) : ExternalObject.TableFunctionSymbol
			{
				scope.Define.[ExternalObject.TableFunctionSymbol](Create());
			}
		}
		
		public abstract class DataTypeBuilder
		{
			public abstract Create() : DataType;
		}
		
		public class TableDataTypeBuilder : DataTypeBuilder
	    {
	        private columnTypes : Hashtable[string, DataTypeBuilder] = 
				Hashtable();
	    
			public this () {}
	
	        public AddColumn(column : string, typeBuilder : SimpleTypeBuilder) : TableDataTypeBuilder
	        {
	            columnTypes.Add(column, typeBuilder);
	            this;
	        }
	    
	        public AddColumns(columns : Seq[string], typeBuilder : SimpleTypeBuilder) : TableDataTypeBuilder
	        {
	            foreach (column in columns)
	                columnTypes.Add(column, typeBuilder);
	            this;
	        }
	        
	        public override Create() : DataType
	        {
	            def items = List(columnTypes.Count);
			    foreach (columnWithType in columnTypes)
			    {
				    def collation = Collation.AstOption(Loc());
				    def defaultValue = DefaultValue.AstOption(Loc());
				    def constraints = ColumnConstraint.AstList(Loc());
				    def rowGuidColumn = ParsedValue(0);
					def type = columnWithType.Value.Create();
				    def item = TableDataTypeColumnItem(Loc(), ToString(columnWithType.Key), type, collation, defaultValue, constraints, rowGuidColumn);
				    items.Add(item);
			    }
			    DataType.Table(Loc(), TableDataTypeItem.AstList(Loc(), items.ToArray()));
	        }
	    }
		
		public class SimpleTypeBuilder : DataTypeBuilder
	    {
	        private mutable type 	: string;
		    private mutable length 	: int;
		    private mutable scale 	: int;
		    private mutable max 	: bool;
		
			public this ()
			{
				type 	= string.Empty;
				length 	= 0;
				scale 	= 0;
				max 	= false;
			}
		
		    public WithName(name : string) : SimpleTypeBuilder
		    {
			    this.type = name;
			    this;
		    }
	
		    public WithLength(length : int) : SimpleTypeBuilder
		    {
			    this.length = length;
			    this.scale = 0;
			    this.max = false;
			    this;
		    }
	
		    public WithLength(length : int, scale : int) : SimpleTypeBuilder
		    {
			    this.length = length;
			    this.scale = scale;
			    this.max = false;
			    this;
		    }
	
		    public WithMaxLength() : SimpleTypeBuilder
		    {
			    this.length = 0;
			    this.scale = 0;
			    this.max = true;
			    this;
		    }
		
		    public override Create() : DataType
		    {
			    match (this.type) {
				    | v when string.IsNullOrEmpty(v) => 
					    throw BuildException("Type name not set")
				    | v => {
					    def typeLength = match ((max, length, scale)) {
						    | (true, _, _) => 
							    DataTypeLength.Max(Loc())
						    | (_   , 0, 0) => 
							    DataTypeLength.Value(Loc(), ParsedValue(0), ParsedValue(0))
						    | (_   , l, 0) => 
							    DataTypeLength.Value(Loc(), ParsedValue(NSpan(), l), ParsedValue(0))
						    | (_   , l, s) => 
							    DataTypeLength.Value(Loc(), ParsedValue(NSpan(), l), ParsedValue(NSpan(), s))
					    };
					    DataType.Simple(Loc(), ToString(v), DataTypeLength.AstOption(Loc(), typeLength :> DataTypeLength))
				    } 
			    }
		    }
	    }
		
		public class ParameterBuilder
		{
			private mutable name : string;
			
			public WithName(name : string) : ParameterBuilder
			{
				this.name = name;
				this;
			}
			
			public Create() : Parameter
			{
				match (this.name) {
					| v when string.IsNullOrEmpty(v) => 
						throw BuildException("Parameter name not set")
					| v => 
						Parameter(Loc(), ToString(v))
				}
			}
		}
		
		public class ParameterDeclarationBuilder
		{
			private mutable name : string;
			private mutable type : DataTypeBuilder;
			
			public WithName(name : string) : ParameterDeclarationBuilder
			{
				this.name = name;
				this;
			}
			
			public WithType(typeBuilder : DataTypeBuilder) : ParameterDeclarationBuilder
			{
				this.type = typeBuilder;
				this;
			}
			
			public Create() : Declare
			{
				match (this.name) {
					| v when string.IsNullOrEmpty(v) => 
						throw BuildException("Parameter name not set")
					| v => 
						Declare(Loc(), Name(Loc(), v), type.Create())
				}
			}
			
			public Define(scope : BindingScope, context : DependentPropertyEvalContext = null) : DeclareSymbol
			{
				def declare = Create();
				declare.ContainingTable = scope;
				def symbol = scope.Define.[DeclareSymbol](declare, context ?? DependentPropertyEvalContext());
				declare.Symbol = symbol;
				symbol.AddDeclaration(declare);
				symbol;
			}
		}
		
		internal module BuilderUtil
		{
			public Loc() : Location 
			{
				Location.Default;
			}
			
			public ToString(value : string) : ParsedValue[string]
			{
				ParsedValue(NSpan(0, value.Length), value);
			}
		}
    }
}