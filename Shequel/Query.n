﻿using Nemerle.Internal;
using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Collections.Immutable;
using Nitra.ProjectSystem;

namespace Shequel
{
  public partial class Query : AstBase, IProjectSupport
  {
    public RefreshProject(cancellationToken : CancellationToken, files : ImmutableArray[FileEvalPropertiesData], data : object) : void
    {
        _ = cancellationToken; _ = files; _ = data;
    }
    
    public RefreshReferences(cancellationToken : CancellationToken, project : Project) : object
    {
        _ = cancellationToken; _ = project;
        object();
    }
  }
}