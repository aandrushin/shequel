﻿using Nemerle;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.IO;

namespace Shequel
{
	[Record]
	public class Binding
	{
		public Location : Location { get; }
		public Name 	: string { get; }
		
		public CreateName() : Name
		{
			Name(Location, Name);
		}
	}

    public module Utils
    {
        public static ToInt(span : NSpan, data : string) : ParsedValue[int]
        {
            mutable value; 
			if (int.TryParse(data, out value))
				ParsedValue(span, value);
			else
				ParsedValue(span.StartPos);
        }
        
        public static ToString(span : NSpan, value : string) : ParsedValue[string]
		{
			if (span.IsEmpty)
				ParsedValue(span, string.Empty);
			else
				ParsedValue(span, value);
		}
		
		public static ToDouble(span : NSpan, data : string) : ParsedValue[double]
		{
			ToDouble(span, data, NumberStyles.Any);
		}
		
		public static ToDouble(span : NSpan, data : string, style : NumberStyles) : ParsedValue[double]
		{
			mutable dataValue = data;
			when (style == NumberStyles.Float)
				when (data.EndsWith("e", StringComparison.OrdinalIgnoreCase))
					dataValue = string.Concat(data, "0");
		
			mutable value;
			if (double.TryParse(dataValue, style, CultureInfo.InvariantCulture, out value))
				ParsedValue(span, value);
			else
				ParsedValue(span.StartPos);
		}
		
		public static ToDecimal(span : NSpan, data : string) : ParsedValue[decimal]
		{
			mutable value;
			if (decimal.TryParse(data, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
				ParsedValue(span, value);
			else
				ParsedValue(span.StartPos);
		}
		
		public static ToBool(span : NSpan) : ParsedValue[bool]
		{
			if (span.IsEmpty)
				ParsedValue(span.StartPos);
			else
				ParsedValue(span, true);
		}
		
		public static NameOf(value : string) : string
		{
			value.Replace(" ", string.Empty).ToLower();
		}
		
		public static ToParsedValue[T](span : NSpan) : ParsedValue[T]
		{
			ParsedValue(span.StartPos);
		}
		
		public static ToParsedValue[T](span : NSpan, value : T) : ParsedValue[T]
		{
			ParsedValue(span, value);
		}
    }
	
	internal module AstUtils
	{	
		public static Escape(value : string) : string
		{
			match((value ?? string.Empty).Trim())
			{
				| "" => string.Empty
				| v when v.StartsWith("[") && v.EndsWith("]") 	=> v
				| v when v.StartsWith("\"") && v.EndsWith("\"") =>
					Escape(UnEscape(value))
				| _ => string.Concat("[", value, "]")
			}
		}
		
		public static UnEscape(value : string) : string
		{
			match((value ?? string.Empty).Trim())
			{
				| "" => string.Empty
				| v when v.StartsWith("[") && v.EndsWith("]") 	=> 
					value.Substring(1, value.Length - 2).Replace("]]", "]")
				| v when v.StartsWith("\"") && v.EndsWith("\"") =>
					value.Substring(1, value.Length - 2).Replace("\"\"", "\"")
				| _ => value
			}
		}
	
		public static CreateQualifiedName(column : string, table : string, schema : string, database : string) : string
		{			
			| ( c, "", "", "")	=> c
			| ( c, 	t, "", "") 	=> $"$t.$c"
			| ( c, 	t,  s, "") 	=> $"$s.$t.$c"
			| ( c, 	t, 	s, 	d) 	=> $"$d.$s.$t.$c"		
		}
	
		public static CreateColumnBinding(column : string, table : string, schema : string, database : string) : string
		{
			def createInternal(column : string, table : string, schema : string, database : string) : string
			{
				| ( c, "", "", "")	=> c
				| ( c, 	t, "", "") 	=> $"$t.$c"
				| ( c, 	t, "", 	d) 	=> $"$d..$t.$c"
				| ( c, 	t,  s, "") 	=> $"$s.$t.$c"
				| ( c, 	t, 	s, 	d) 	=> $"$d.$s.$t.$c"
			}
			createInternal(Escape(column), Escape(table), Escape(schema), Escape(database));
		}
	
		public static CreateObjectBinding(name : string, schema : string, database : string, server : string) : string
		{
			def createInternal (name : string, schema : string, database : string, server : string) : string
			{
				| ( n, "", "", 	"")		=> n
				| ( n, 	s, 	"", "")  	=> $"$(s).$(n)"
				| ( n, "", 	d, 	"")     => string.Concat(d, "..", n)
				| ( n, "", 	d, 	srv)    => string.Concat(srv, ".", d, "..", n)
				| ( n, 	s, 	d, 	"") 	=> $"$(d).$(s).$(n)"
				| ( n, 	s, 	d, 	srv) 	=> $"$(srv).$(d).$(s).$(n)"
			}
			createInternal(Escape(name), Escape(schema), Escape(database), Escape(server));
		}
		
		public static CreateObjectBindings(name : string, schema : string, database : string, server : string) : ImmutableArray[string]
		{
			def createBindings(name : string, schema : string, database : string, server : string) : list[string]
			{
				| ("", "", "", 	"") => []
				| (	n, "", "", 	"") => [ CreateObjectBinding(n, "", "",  "") ]
				| (	n, "", 	d, 	"") => 	 CreateObjectBinding(n, "",  d,  "") :: createBindings(n, "", "", "")
				| (	n, "", 	d, srv) => 	 CreateObjectBinding(n, "",  d, srv) :: createBindings(n, "",  d, "")
				| (	n,  s, "", 	"") => 	 CreateObjectBinding(n,  s, "",  "") :: createBindings(n, "", "", "")
				| (	n,  s, 	d, 	"") => 	 CreateObjectBinding(n,  s,  d,  "") :: createBindings(n, s, "", "")
				| (	n,  s, 	d, srv) =>   CreateObjectBinding(n,  s,  d, srv) :: createBindings(n, s,  d, "")
			}
			
			ImmutableArray.Create(createBindings(Escape(name), Escape(schema), Escape(database), Escape(server)).ToArray());
		}
		
		public static CreateRelationBindings(options : ParseOptions, identifier : ObjectIdentifier) : ImmutableArray[string]
		{
			def list = List();
			list.AddRange(identifier.Bindings);
			match (identifier) {
				| ObjectIdentifier(IsObjectRefEvaluated = true, 
					ObjectRef=(IsSymbolEvaluated = true, Symbol = s is ExternalObjectSymbol)) =>
						list.AddRange(s.Bindings)
				| _ => ()
			}
			ImmutableArray.Create(list.Distinct(options.CaseComparer).ToArray());
		}
	}
}