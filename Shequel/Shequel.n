﻿using Nemerle;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.IO;

namespace Shequel
{
	using Utils;

    public enum TempTableKind
	{
		| Local
		| Global
	}
	
	public enum	FetchMode
	{
		| First 
		| Next
	}
	
	public enum SortMode
	{
		| Asc
		| Desc
	}
	
	public enum OverWindowMode
	{
		| Rows 
		| Range
	}
	
	public enum WindowFrameDirection
	{
		| Following 
		| Preceding
	}
	
	public enum DistinctMode
	{
		| All
		| Distinct
	}
	
	public enum QueryComparisonMode
	{
		| All
		| Some
		| Any
	}
	
	public enum XmlContentKind
	{
		| Document
		| Content
	}
	
	public enum JoinHint 
	{ 
		| Loop 	
		| Hash 	
		| Merge 
		| Remote
	}
	
	public enum OuterJoinKind
	{
		| Left 	
		| Right 	
		| Full
	}
	
	public enum ApplyKind
	{
		| Cross
		| Outer
	}
	
	public enum GroupByKind
	{
		| Cube
		| Rollup
	}
	
	public enum DatePart
	{
		| Year
		| Quarter
		| Month
		| DayOfYear
		| Day
		| Week
		| Weekday
		| Hour
		| Minute
		| Second
		| Millisecond
		| Microsecond
		| Nanosecond
	}
	
	internal module EnumParsers
	{
		public static ToDatePart(span : NSpan, value : string) : ParsedValue[DatePart]
		{
			match(NameOf(value)) {
				| "yy" 	| "yyyy"| "year" 	=> ToParsedValue(span, DatePart.Year)
				| "q"  	| "qq" 	| "quarter"	=> ToParsedValue(span, DatePart.Quarter)
				| "m"  	| "mm" 	| "month"  	=> ToParsedValue(span, DatePart.Month)
				| "y"  	| "dy" 	| "dayofyear" => ToParsedValue(span, DatePart.DayOfYear)
				| "d"	| "dd"	| "day" 	=> ToParsedValue(span, DatePart.Day)
				| "ww"	| "wk"	| "week" 	=> ToParsedValue(span, DatePart.Week)
				| "w"	| "dw"	| "weekday" => ToParsedValue(span, DatePart.Weekday)
				| "hh"	| "hour" 			=> ToParsedValue(span, DatePart.Hour)
				| "n"	| "mi"	| "minute" 	=> ToParsedValue(span, DatePart.Minute)
				| "s"	| "ss"	| "second"	=> ToParsedValue(span, DatePart.Second)
				| "ms"	| "millisecond"	=> ToParsedValue(span, DatePart.Millisecond)
				| "mcs"	| "microsecond"	=> ToParsedValue(span, DatePart.Microsecond)
				| "ns"	| "nanosecond"	=> ToParsedValue(span, DatePart.Nanosecond)
				| _ => ToParsedValue(span)
			};
		}
	
		public static ToFetchMode(span : NSpan, value : string) : ParsedValue[FetchMode]
		{
			match(NameOf(value)) {
				| "first" => ToParsedValue(span, FetchMode.First)
				| "next"  => ToParsedValue(span, FetchMode.Next)
				| _ => ToParsedValue(span)
			};
		}
		
		public static ToSortMode(span : NSpan, value : string) : ParsedValue[SortMode]
		{
			match(NameOf(value))
			{
				| ""
				| "asc" => ToParsedValue(span, SortMode.Asc)
				| "desc" => ToParsedValue(span, SortMode.Desc)
				| _ => ToParsedValue(span)
			}
		}
		
		public static ToOverWindowMode(span : NSpan, value : string) : ParsedValue[OverWindowMode]
		{
			match(NameOf(value)) {
				| "rows" => ToParsedValue(span, OverWindowMode.Rows);
				| "range" => ToParsedValue(span, OverWindowMode.Range);
				| _ => ToParsedValue(span);
			};
		}
		
		public static ToWindowFrameDirection(span : NSpan, value : string) : ParsedValue[WindowFrameDirection]
		{
			match(NameOf(value)) {
				| "following" => ToParsedValue(span, WindowFrameDirection.Following);
				| "preceding" => ToParsedValue(span, WindowFrameDirection.Preceding);
				| _ => ToParsedValue(span);
			};
		}
		
		public static ToDistinctMode(span : NSpan, value : string) : ParsedValue[DistinctMode]
		{
			match(NameOf(value)) {
				| ""
				| "all" => ToParsedValue(span, DistinctMode.All);
				| "distinct" => ToParsedValue(span, DistinctMode.Distinct);
				| _ => ToParsedValue(span);
			};
		}
		
		public static ToQueryComparisonMode(span : NSpan, value : string) : ParsedValue[QueryComparisonMode]
		{
			match(NameOf(value)) {
				| "all" => ToParsedValue(span, QueryComparisonMode.All);
				| "some" => ToParsedValue(span, QueryComparisonMode.Some);
				| "any" => ToParsedValue(span, QueryComparisonMode.Any);
				| _ => ToParsedValue(span);
			}
		}
		
		public static ToXmlContentKind(span : NSpan, value : string) : ParsedValue[XmlContentKind]
		{
			match(NameOf(value))
			{	
				| "content" => ToParsedValue(span, XmlContentKind.Content);
				| "document" => ToParsedValue(span, XmlContentKind.Document);
				| _ => ToParsedValue(span);
			};
		}
		
		public static ToJoinHint(span : NSpan, value : string) : ParsedValue[JoinHint]
		{
			match(NameOf(value))
			{	
				| "loop" => ToParsedValue(span, JoinHint.Loop);
				| "hash" => ToParsedValue(span, JoinHint.Hash);
				| "merge" => ToParsedValue(span, JoinHint.Merge);
				| "remote" => ToParsedValue(span, JoinHint.Remote);
				| _ => ToParsedValue(span);
			};
		}
		
		public static ToOuterJoinKind(span : NSpan, value : string) : ParsedValue[OuterJoinKind]
		{
			match(NameOf(value))
			{	
				| "left" => ToParsedValue(span, OuterJoinKind.Left);
				| "right" => ToParsedValue(span, OuterJoinKind.Right);
				| "full" => ToParsedValue(span, OuterJoinKind.Full);
				| _ => ToParsedValue(span);
			};
		}
		
		public static ToApplyKind(span : NSpan, value : string) : ParsedValue[ApplyKind]
		{
			match(NameOf(value))
			{	
				| "outer" => ToParsedValue(span, ApplyKind.Outer);
				| "cross" => ToParsedValue(span, ApplyKind.Cross);
				| _ => ToParsedValue(span);
			};
		}
		
		public static ToGroupByKind(span : NSpan, value : string) : ParsedValue[GroupByKind]
		{
			match(Utils.NameOf(value)) {
				| "cube" => ToParsedValue(span, GroupByKind.Cube);
				| "rollup" => ToParsedValue(span, GroupByKind.Rollup);
				| _ => ToParsedValue(span);
			}
		}
	}
}