﻿using Nemerle;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Extensions;
using Nemerle.Assertions;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;
using Nitra.Runtime.Reflection;
using Nitra.Serialization2;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.IO;

namespace Shequel.Internal
{
	using Utils;
	using AstUtils;
	
    public class ReferenceVisitor : IAstVisitor
	{
		private messages : CompilerMessageList;
		private caseComparer : IEqualityComparer[string];
	
		public this([NotNull] messages : CompilerMessageList, [NotNull] caseComparer : IEqualityComparer[string])
		{
			this.messages = messages;
			this.caseComparer = caseComparer;
		}
	
		public Messages : CompilerMessageList { get { messages; } }
	
		public Visit(parseTree : IAst) : void
		{
			match (parseTree)
		    {
		        | id is ColumnIdentifier => {
					when (id.Table.HasValue)
					{		
						def location = Location(id.Source, id.Database.Span + id.Schema.Span + id.Table.Span);
						def qualifiedName = CreateQualifiedName(id.Table.Value, id.Schema.Value, id.Database.Value, string.Empty);
						when (id.IsRelationRefEvaluated)
						{
							when (id.RelationRef.IsUnresolved)
								messages.Error(location, $"Unresolved reference '$(qualifiedName)'.");
							
							when (id.RelationRef.IsAmbiguous)
								messages.Error(location, $"Ambiguous reference '$(qualifiedName)'.");
						}
					}
					
					unless (string.IsNullOrEmpty(id.Column.Value) || string.Equals(id.Column.Value, "*"))
					{
						def columnLocation = Location(id.Source, id.Column.Span);
						when (id.IsColumnRefEvaluated)
						{
							when (id.ColumnRef.IsUnresolved)
								messages.Error(columnLocation, $"Unresolved column name '$(id.Column.Value)'.");	
						
							when (id.ColumnRef.IsAmbiguous)
								messages.Error(columnLocation, $"Ambiguous column name '$(id.Column.Value)'.");	
						}						
					}						
				}
			    | RelationDeclaration.PivotRelation(PivotColumns = cols, Column = c) => {
					foreach (column in cols)
						unless (column.IsMissing)
							when (caseComparer.Equals(c.Name.Value, column.Name.Text))
								messages.Error(column, $"The column name '$(column.Name.Text)' specified in the PIVOT operator conflicts with the existing column name in the PIVOT argument.");
			
				}
				| relation is RelationDeclaration.UnpivotRelation => {
					foreach (column in relation.Columns)
					{	
						unless (column.IsMissing)
						{
							def pivotColumn = relation.PivotColumn;		
							unless (pivotColumn.IsMissing)
								when (caseComparer.Equals(Escape(pivotColumn.Name.Value), Escape(column.Column.Value)))
									messages.Error(column, $"The column name '$(column.Column.Value)' specified in the UNPIVOT operator conflicts with the existing column name in the UNPIVOT argument.");
									
							def valueColumn = relation.ValueColumn;
							unless (valueColumn.IsMissing)
								when (caseComparer.Equals(Escape(valueColumn.Name.Value), Escape(column.Column.Value))) // TODO: change to bind
									messages.Error(column, $"The column name '$(column.Column.Value)' specified in the UNPIVOT operator conflicts with the existing column name in the UNPIVOT argument.");
						}
					}
				}
				| identifier is ObjectIdentifier => {
					when (identifier.ObjectRef.IsUnresolved)
						messages.Error(identifier, $"Invalid object name '$(identifier.QualifiedName)'.");
					
					when (identifier.ObjectRef.IsAmbiguous)
						messages.Error(identifier, $"Ambiguous reference '$(identifier.QualifiedName)'.");
				}
				| _ => ()
		    }
			
			parseTree.Accept(this);
		}
		
		public Visit(reference : Reference) : void implements IAstVisitor.Visit
		{
			_ = reference;
		}

		public Visit(name : Name) : void implements IAstVisitor.Visit
		{
			_ = name;
		}

		public Visit(r : IRef) : void
		{
			_ = r;
		}
	}
}