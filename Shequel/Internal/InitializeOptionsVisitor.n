﻿using Nemerle;
using Nemerle.Assertions;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Extensions;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.IO;

namespace Shequel.Internal
{
	public class InitializeOptionsVisitor : IAstVisitor
	{
		private options : ParseOptions;
		
		public this ([NotNull] options : ParseOptions)
		{
			this.options = options;
		}
	
		public Visit(parseTree : IAst) : void
		{
			when (parseTree is WithOptions as withOptions)
		        withOptions.Options = options;
		        
			parseTree.Accept(this);
		}
		
		public Visit(reference : Reference) : void implements IAstVisitor.Visit
		{
			_ = reference;
		}

		public Visit(name : Name) : void implements IAstVisitor.Visit
		{
			_ = name;
		}

		public Visit(r : IRef) : void
		{
			_ = r;
		}
	}
}