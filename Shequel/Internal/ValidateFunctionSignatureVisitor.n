﻿using Nemerle;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Assertions;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.Reflection;
using System.IO;

namespace Shequel.Internal
{
	public class ValidateFunctionSignatureVisitor : IAstVisitor
	{
		private static resourceName : string = "Shequel.Data.SystemFunction.txt";
		private static signatures   : Dictionary[string, SignatureInfo];
		private messages : CompilerMessageList;

		static this ()
		{
			def assembly = Assembly.GetExecutingAssembly();
			using (def stream = assembly.GetManifestResourceStream(resourceName))
			using (def reader = StreamReader(stream))
			{
				signatures = Hashtable(StringComparer.OrdinalIgnoreCase);
				while (!reader.EndOfStream)
				{
					def line = reader.ReadLine();
					def index = line.IndexOfAny(array[' ', '\t']);
					if (index > 0)
					{
						def name = line.Substring(0, index).Trim();
						def parameters = line
							.Substring(index, line.Length - index)
							.Trim()
							.Split(',')
							.Select(GetSignatureParameter)
							.ToArray();
							
						unless (signatures.ContainsKey(name))
							signatures.Add(name, SignatureInfo(name, ImmutableArray.Create(parameters)));
					}
					else
					{
						def name = line.Trim();
						
						when (string.IsNullOrEmpty(name))
						    continue;
						    
						unless (signatures.ContainsKey(name))
						    signatures.Add(name, SignatureInfo(name, ImmutableArray.Create()));
					}
				}
			}
		}
		
		public this ([NotNull] messages : CompilerMessageList)
		{
			this.messages = messages;
		}
	
		public Visit(parseTree : IAst) : void
		{
			def error = 
				match (parseTree) {
					| f is Function.System => {
						mutable info : SignatureInfo;
						if (f.Name.HasValue && signatures.TryGetValue(f.Name.Value, out info))
							GetError(info, f.Parameters)
						else 
							string.Empty;
					}
					| Function.UserDefined(
						Parameters = parameters,
						FullName=(
							ObjectRef=(IsUnresolved = false, IsSymbolEvaluated = true, Symbol = s is ExternalObjectSymbol))) => {
								def signature = match (s) {
									| f is ExternalObject.ScalarFunctionSymbol => f.Signature
									| f is ExternalObject.TableFunctionSymbol => f.Signature
									| _ => null
								};
								GetError(signature, parameters);
							}
					| _ => string.Empty;
				};
			
			unless (string.IsNullOrEmpty(error))
		        messages.Error(parseTree, error);
		        
			parseTree.Accept(this);
		}
		
		public Visit(reference : Reference) : void implements IAstVisitor.Visit
		{
			_ = reference;
		}

		public Visit(name : Name) : void implements IAstVisitor.Visit
		{
			_ = name;
		}

		public Visit(r : IRef) : void
		{
			_ = r;
		}
		
		private GetError(info : SignatureInfo, [NotNull] parameters : Expression.IAstList) : string
		{
			when (info == null) return string.Empty;
			
			def parameterCount = parameters.Count;
			def minParameterCount = info.Parameters.Length - info.OptionalParameters.Length;
			def maxParameterCount = info.Parameters.Length;
			when (parameterCount < minParameterCount || parameterCount > maxParameterCount)
			{
				return if (minParameterCount == maxParameterCount)
					$"The $(info.SignaturePopup) function requires $(minParameterCount) argument(s)."
				else
					$"The $(info.SignaturePopup) function requires $(minParameterCount) to $(maxParameterCount) argument(s).";
			}
			string.Empty;
		}
		
		private static GetSignatureParameter(str : string) : SignatureParameter
		{
			def parts = str.Split('=');
			if (parts.Length > 1)
				SignatureParameter(parts[0], parts[1].Trim().EndsWith("?"))
			else
				SignatureParameter(parts[0], parts[0].Trim().EndsWith("?"))
		}
	}
	
	[Record]
	public class SignatureInfo
	{
		public Name : string { get; }
		public Parameters : ImmutableArray[SignatureParameter] { get; }
		
		public OptionalParameters : ImmutableArray[SignatureParameter]
		{
			[Memoize] get { Parameters.Where(p => p.Optional).ToImmutableArray(); }
		}
		
		public SignaturePopup : string
		{
			[Memoize] get { $<#$(Name)(..$(Parameters; ", "; _.ParameterPopUp))#>; }
		}
	}
	
	[Record]
	public class SignatureParameter
	{
		public Name : string { get; }
		public Optional : bool { get; }
		
		public ParameterPopUp : string
		{
			[Memoize] get { if (Optional) $"[$Name]" else $"$Name"; }
		}
	}
}