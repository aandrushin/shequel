﻿using Nemerle;
using Nemerle.Assertions;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Extensions;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.IO;

namespace Shequel.Internal
{
	using Utils;
	using AstUtils;
	
    public class DefineColumnVisitor : IAstVisitor
	{
		private context : DependentPropertyEvalContext;
		private options : ParseOptions;
		
		public this ([NotNull] context : DependentPropertyEvalContext, [NotNull] options : ParseOptions)
		{
			this.context = context;
			this.options = options;
		}
	
		public Visit(parseTree : IAst) : void
		{
			match (parseTree) {
				| r is RelationDeclaration =>
					foreach (column in GetDeclarationColumns(r))
						DefineColumn(r.ContainingTable, context, column)
				| _ => ()
			}
			
			parseTree.Accept(this);
		}
		
		public Visit(reference : Reference) : void implements IAstVisitor.Visit
		{
			_ = reference;
		}

		public Visit(name : Name) : void implements IAstVisitor.Visit
		{
			_ = name;
		}

		public Visit(r : IRef) : void
		{
			_ = r;
		}
		
		private GetDeclarationColumns(relation : RelationDeclaration) : Seq[ColumnInfo]
		{
			| RelationDeclaration.ObjectRelation(
				IsSymbolEvaluated = true,
				Symbol = rs,
				ObjectName=(IsObjectRefEvaluated = true, 
					ObjectRef=(IsSymbolEvaluated = true, Symbol = s is ExternalObject.TableSymbol))) => {
				s.Columns.Select(c => ColumnInfo(c, GetBindings(c, ConcatBindings(rs.Bindings, s.Bindings))));
			}
			| RelationDeclaration.ObjectRelation(
				IsSymbolEvaluated = true,
				Symbol = rs,
				ObjectName = (IsObjectRefEvaluated = true, 
					ObjectRef=(IsSymbolEvaluated = true, Symbol = s is ExternalObject.ViewSymbol))) => {
				s.Columns.Select(c => ColumnInfo(c, GetBindings(c, ConcatBindings(rs.Bindings, s.Bindings))));
			}
			| RelationDeclaration.FunctionRelation(
				IsSymbolEvaluated = true,
				Symbol = rs,
				FunctionName = (IsObjectRefEvaluated = true, 
					ObjectRef=(IsSymbolEvaluated = true, Symbol = s is ExternalObject.TableFunctionSymbol))) => {
				s.Columns.Select(c => ColumnInfo(c, GetBindings(c, ConcatBindings(rs.Bindings, s.Bindings))));
			}
			| RelationDeclaration.ParameterRelation(
				IsSymbolEvaluated = true,
				Symbol = rs,
				Parameter=(IsDeclareRefEvaluated = true, 
					DeclareRef=(IsSymbolEvaluated = true, 
						Symbol=DeclareSymbol(
							FirstDeclarationOrDefault=Declare(
								DataType = t is DataType.Table))))) => {
				t.Items
					.OfType.[TableDataTypeColumnItem]()
					.Where(i => i.Column.HasValue)
					.Select(i => ColumnInfo(i.Column.Value, GetBindings(i.Column.Value, rs.Bindings)));
			}
			| RelationDeclaration.SubQueryRelation(IsSymbolEvaluated = true, Symbol = rs, SubQuery = s, Alias = a) => {
				if (a.Columns.Any())
				{
					a.Columns.Select(c => ColumnInfo(c.Name.Text, GetBindings(c.Name.Text, rs.Bindings)));
				}
				else
				{
					def list = List();
					foreach (projection in s.Projection)
					{
						| Projection.Simple(Alias=(HasValue = true, Value = a)) 
							when a.Name.HasValue =>
								list.Add(ColumnInfo(a.Name.Value, GetBindings(a.Name.Value, rs.Bindings))) 
						| Projection.Simple(Expression = c is ColumnIdentifier) 
							when c.Column.HasValue =>
								list.Add(ColumnInfo(c.Column.Value, GetBindings(c.Column.Value, rs.Bindings)))
						| _ => ()
					}
					list;
				}
			}
			| RelationDeclaration.ValuesRelation(IsSymbolEvaluated = true, Symbol = rs, Alias = a) => {
				a.Columns.Select(c => ColumnInfo(c.Name.Text, GetBindings(c.Name.Text, rs.Bindings)));
			}
			| RelationDeclaration.PivotRelation(IsSymbolEvaluated = true, PivotColumns = cols, Column = pc, Relation = r, Name = n) => {
				def columns = cols.Select(c => ColumnInfo(c.Name.Text, GetBindings(c.Name.Text, n.Text))).ToList();
				when (!pc.IsMissing && !r.IsMissing)
					columns.AddRange(GetRelationColumns(r).Select(c => ColumnInfo(c.Column, GetBindings(c.Column, n.Text))).Except([ ColumnInfo(pc.Name.Value, GetBindings(pc.Name.Value, n.Text)) ]));
				columns;
			}
			| RelationDeclaration.UnpivotRelation(IsSymbolEvaluated = true, PivotColumn = pc, ValueColumn = vc, Relation = r, Columns = cols, Name = n) => {
				def list = List();
				when (!pc.IsMissing && pc.Name.HasValue)
					list.Add(ColumnInfo(pc.Name.Value, GetBindings(pc.Name.Value, n.Text)));
				
				when (!vc.IsMissing && pc.Name.HasValue)
					list.Add(ColumnInfo(vc.Name.Value, GetBindings(vc.Name.Value, n.Text)));
				
				when (!r.IsMissing)
				{	
					def columns = cols
						.Where(c => !c.IsMissing && c.Column.HasValue)
						.Select(c => ColumnInfo(c.Column.Value, GetBindings(c.Column.Value, n.Text)));
					
					list.AddRange(GetRelationColumns(r).Select(c => ColumnInfo(c.Column, GetBindings(c.Column, n.Text))).Except(columns));
				}
				list;
			}
			| _ => Enumerable.Empty();
		}
		
		private GetRelationColumns(relation : Relation) : Seq[ColumnInfo]
		{
			| r is RelationDeclaration =>
				GetDeclarationColumns(r)
			| Join.Inner(Left = l, Right = r) =>  {
				ConcatRelationColumns(l, r);
			}	
			| Join.Sub(Inner = i) => {
				GetRelationColumns(i);
			} 
			| Join.Outer(Left = l, Right = r) => {
				ConcatRelationColumns(l, r);
			}
			| Join.Cross(Left = l, Right = r) =>  {
				ConcatRelationColumns(l, r)
			}
			| Join.Apply(Left = l, Right = r) => {
				ConcatRelationColumns(l, r)
			}
			| _ => Enumerable.Empty();
		}
		
		private ConcatRelationColumns(relation1 : Relation, relation2 : Relation) : Seq[ColumnInfo]
		{
			GetRelationColumns(relation1)
				.Union(GetRelationColumns(relation2));
		}
		
		private DefineColumn(scope : TableScope, context : DependentPropertyEvalContext, column : ColumnInfo) : void
		{
			def name  = Name(Location.Default, Escape(column.Column));
			def value = ToString(NSpan(), Escape(column.Column));
			def declaration = ColumnDeclaration(Location.Default, name, value);
			def symbol = scope.Define.[ColumnDeclarationSymbol](declaration, context, name);
			symbol.Bindings = ImmutableArray.Create(column.Bindings.ToArray());
			symbol.AddDeclaration(declaration);
			declaration.Symbol = symbol;
		}
		
		private GetBindings(column : string, relationBindings : Seq[string]) : Seq[string]
		{
			def escape = Escape(column);
			[ escape ].Union(relationBindings.Select(b => $"$(b).$(escape)"));
		}
		
		private ConcatBindings(bindings1 : Seq[string], bindings2 : Seq[string]) : Seq[string]
		{
			HashSet(bindings1.Concat(bindings2), options.CaseComparer);
		}
		
		private GetBindings(column : string, binding : string) : Seq[string]
		{
			def escape = Escape(column);
			[ escape, $"$(binding).$(escape)" ];
		}
		
		[Record]
		[StructuralEquality]
		internal class ColumnInfo
		{
			public Column : string { get; }
			[EqualsIgnore] public Bindings : Seq[string] { get; }
		}
	}
}