﻿using Nemerle;
using Nemerle.Assertions;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Extensions;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.IO;

namespace Shequel.Internal
{
    public class ShequelEvalHost : EvalPropertiesHost
	{
		private ast : IAst;
		private scope : BindingScope;
		private options : ParseOptions;
	
		public this ([NotNull] ast : IAst, [NotNull] scope : BindingScope, [NotNull] options : ParseOptions)
		{
			this.ast = ast;
			this.scope = scope;
			this.options = options;
		}
		
		protected override ExecutePass(context : DependentPropertyEvalContext, passName : string) : void
		{
			when (context.CancellationToken.IsCancellationRequested)
				return;
				
			def statistics = StatisticsTask.Single(passName);
			EvalProperties(context, ast, statistics);
		}
		
		protected override BeforeStage(context : DependentPropertyEvalContext, _ : string) : void
		{
			match (context.Stage)
			{
				| 0 => {
					scope.EvalProperties(context); // eval properties of external objects
					
					foreach (file in context.Files)
					{
						def optionsVisitor = InitializeOptionsVisitor(options);
						optionsVisitor.Visit(file.Ast);
						
						when (file.Ast is Query as query)
							query.Scope = scope;
					}
				}
				| 2 => {
					foreach (file in context.Files)
						when (file.Ast is Query as query) {
							def visitor = DefineColumnVisitor(context, options);
							visitor.Visit(query);
						}
				}
				| _ => ()
			}
		}
	}
}