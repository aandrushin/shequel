﻿using Nemerle;
using Nemerle.Assertions;
using Nemerle.Imperative;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Extensions;

using Nitra;
using Nitra.Declarations;
using Nitra.Internal;
using Nitra.ProjectSystem;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections.Immutable;
using System.Threading;
using System.IO;

namespace Shequel.Internal
{
	public class QuotedIdentifierVisitor : IAstVisitor
	{
		private messages : CompilerMessageList;
		
		public this ([NotNull] messages : CompilerMessageList)
		{
			this.messages = messages;
		}
	
		public Messages : CompilerMessageList { get { messages; } }
	
		public Visit(parseTree : IAst) : void
		{
			def ids = match (parseTree) {
				| r is ColumnIdentifier when !r.IsMissing => 
					[ r.Database, r.Schema, r.Table, r.Column ].Where(Quoted)
				| r is ObjectIdentifier when !r.IsMissing => 
					[ r.Server, r.Database, r.Schema, r.Object ].Where(Quoted)
				| _ => []
			};
			
			foreach (id in ids)
				messages.Error(Location(parseTree.Source, id.Span), $"Incorrect syntax near $(id.Value)");
			
			parseTree.Accept(this);
		}
		
		public Visit(reference : Reference) : void implements IAstVisitor.Visit
		{
			_ = reference;
		}

		public Visit(name : Name) : void implements IAstVisitor.Visit
		{
			_ = name;
		}

		public Visit(r : IRef) : void
		{
			_ = r;
		}
		
		private Quoted(id : ParsedValue[string]) : bool
		{
			id.Value.StartsWith("\"") && id.Value.EndsWith("\"");
		}
	}
}