# README #

### Summary ###

Simple [Nitra](https://github.com/rsdn/nitra) based TSQL parser.

### License ###

[BSD 3-Clause License](https://opensource.org/licenses/BSD-3-Clause)